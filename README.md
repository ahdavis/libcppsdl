# libcppsdl

## Overview

libcppsdl is a shared library for Simple DirectMedia Layer (SDL) graphics 
development written in pure C++. It acts as a wrapper around the 
C SDL API and allows developers to work in an object-oriented context, 
vastly simplifying the development process. For instance, there is no need 
for manual memory management with libcppsdl, as the classes handle that 
aspect of development automatically. There are many more reasons to use 
this library, as I'm sure you will discover through its use and this 
document. Read on!

## Legal Stuff

This library is licensed under the GNU LGPL license (version 3). For more
information, see [here](https://www.gnu.org/licenses/lgpl-3.0.en.html).
In a nutshell, this means that I have to provide the source code to you,
and that if you redistribute this library (either the compiled binary or
its source code), you have to provide the source code, along with the
LICENSE file located in the root directory of the source repository. If
you notice a violation of this license, please notify me at 
[ahdavis13@gmail.com](mailto:ahdavis13@gmail.com).

##  Compilation and Installation Guide

First, install the `build-essentials` package using your preferred package
manager if it is not installed already. To compile this library without 
the test program, type `make library` at the terminal prompt while in the 
root directory of the library. To compile the test program after the
library has been compiled, type `make test` at the prompt. To compile both 
the library and the test program at once, simply type `make` at the prompt.
After compiling the library and/or the test program, type 
`sudo make install` at the prompt. You must have root access, or this 
command will not execute properly. Finally, type `make clean` to delete 
the compiled library and/or the test program.

##  Repository Structure

- Root
	- `src`: Contains the source code for the library and test program
		- `lib`: Library source code
			- `cppsdl.h`: Master include file for the library
			- `color`: Contains files related to color
			- `element`: Contains text and image classes
			- `event`: Contains event-related files
			- `except`: Contains exception classes
			- `file`: Contains files related to file I/O
			- `gfx`: Contains files related to GPU graphics
			- `gui`: Contains files related to GUI programming
			- `input`: Contains files relating to user input
			- `sound`: Contains files relating to sound output
			- `thread`: Contains files relating to multithreading
			- `time`: Contains files relating to timing
			- `util`: Contains utility files
			- `win`: Contains files relating to windows
		- `test`: Test program code and assets
			- `main.cpp`: Main code file for the test program
			- `assets`: Resources for the test program
	- `.gitignore`: Defines file types to not be included in the repo
	- `LICENSE`: Copy of the GNU LGPL license (see "Legal Stuff" above)
	- `Makefile`: Build/install script for the library
	- `README.md`: This file

## Interface Description

All classes, constants and functions reside in the `CPPSDL` namespace.
To check the usage and purpose of a method or function, look it up in its
corresponding `.h` file.

## Getting Started

First, create a `Window` object. To do this, you will need the caption for
the window, the window's width and height, whether it should show on
creation, and (optionally) its coordinates on the computer screen.
Next, create a `Renderer` object, using your window object as the argument 
to the `Renderer` class constructor. Then, create an `EventHandler` object.
In your main loop, follow these steps:

	1. Handle events
	2. Clear the window
	3. Render your textures
	4. Update the state of your program

Any source file that uses code from this library must contain the line
`#include <cppsdl/cppsdl.h>`. Furthermore, any source file that uses code
from this library must be linked with the `-lcppsdl` flag.

## Closing Remarks

I really enjoyed writing this library. I focused on making it easy to use,
while not exposing any SDL functions or objects to the user. I hope that
you have fun using my library, and find it useful and complete. As always,
if you think of a feature that this library needs, feel free to submit a
pull request. Happy development!
