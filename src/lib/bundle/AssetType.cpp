/*
 * AssetType.cpp
 * Implements helper functions for the AssetType enum
 * Created by Andrew Davis
 * Created on 6/22/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "AssetType.h"

//typeFromName function - converts a string to an asset type
CPPSDL::AssetType CPPSDL::typeFromName(const std::string& name) {
	//declare the return type
	//defaults to a data file
	CPPSDL::AssetType ret = CPPSDL::AssetType::DAT;
	
	//handle different names
	if(name == "IMG") { //image
		ret = CPPSDL::AssetType::IMG;
	} else if(name == "FNT") { //font
		ret = CPPSDL::AssetType::FNT;
	} else if(name == "SND") { //sound
		ret = CPPSDL::AssetType::SND;
	} else if(name == "MAP") { //tilemap
		ret = CPPSDL::AssetType::MAP;
	} else if(name == "SAV") { //save data
		ret = CPPSDL::AssetType::SAV;
	} else if(name == "CHK") { //chunk data
		ret = CPPSDL::AssetType::CHK;
	}

	//return the return value
	return ret;
}

//nameFromType function - converts an asset type to a string
std::string CPPSDL::nameFromType(CPPSDL::AssetType type) {
	std::string name; //the return value
	switch(type) {
		case CPPSDL::AssetType::IMG: //image asset
			{
				name = "IMG";
				break;
			}
		case CPPSDL::AssetType::FNT: //font asset
			{
				name = "FNT";
				break;
			}
		case CPPSDL::AssetType::SND: //sound asset
			{
				name = "SND";
				break;
			}
		case CPPSDL::AssetType::MAP: //tilemap asset
			{
				name = "MAP";
				break;
			}
		case CPPSDL::AssetType::SAV: //save data
			{
				name = "SAV";
				break;
			}
		case CPPSDL::AssetType::DAT: //data file
			{
				name = "DAT";
				break;
			}	
		case CPPSDL::AssetType::CHK: //chunk data
			{
				name = "CHK";
				break;
			}
	}

	//return the name
	return name;

}

//end of implementation
