/*
 * Asset.cpp
 * Implements a class that represents an asset
 * Created by Andrew Davis
 * Created on 6/22/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "Asset.h"

//includes
#include <fstream>
#include <sstream>
#include "../util/functions.h"

//static field initialization
int CPPSDL::Asset::curID = 1;

//default constructor
CPPSDL::Asset::Asset()
	: CPPSDL::Asset(CPPSDL::AssetType::DAT, "", "")
{
	//no code needed
}

//main constructor
CPPSDL::Asset::Asset(CPPSDL::AssetType newType, const std::string& newName,
			const std::string& newRootPath)
	: type(newType), name(newName), pathFromRoot(newRootPath), 
		id(curID++)
{
}

//destructor
CPPSDL::Asset::~Asset() {
	//no code needed
}

//copy constructor
CPPSDL::Asset::Asset(const CPPSDL::Asset& a)
	: type(a.type), name(a.name), pathFromRoot(a.pathFromRoot), 
		id(curID++)
{
	//no code needed
}

//assignment operator
CPPSDL::Asset& CPPSDL::Asset::operator=(const CPPSDL::Asset& src) {
	this->type = src.type; //assign the type field
	this->name = src.name; //assign the name field
	this->pathFromRoot = src.pathFromRoot; //assign the root path field
	//do not assign the ID
	return *this; //and return the instance
}

//getType method - returns the type of the Asset
CPPSDL::AssetType CPPSDL::Asset::getType() const {
	return this->type; //return the type field
}

//getName method - returns the name of the Asset
const std::string& CPPSDL::Asset::getName() const {
	return this->name; //return the name field
}

//getPath method - returns the absolute path to the Asset
std::string CPPSDL::Asset::getPath() const {
	//init the path field
	std::stringstream ss; //used to assemble the path
	ss << CPPSDL::GetBasePath(); //add the base path to the stream
	ss << "../"; //append the "up directory" symbol
	ss << this->pathFromRoot << "/"; //append the sub-directory
	ss << this->name; //append the filename
	return ss.str(); //and return the assembled string
}

//getID method - returns the ID of the Asset
int CPPSDL::Asset::getID() const {
	return this->id; //return the ID field
}

namespace CPPSDL {
	//serialization operator
	std::ostream& operator<<(std::ostream& os, 
					const Asset& a) {
		//get the asset type as a string
		std::string atype = nameFromType(a.type);
	
		//write the asset to the file stream
		os << atype << ' ' << a.name << ' ' << a.pathFromRoot; 
		os << '\n';
	
		//and return the stream
		return os;
	}

	//deserialization operator
	std::istream& operator>>(std::istream& is, Asset& a) {
		//read the type
		std::string tname;
		is >> tname;
		a.type = typeFromName(tname);

		//read in the other fields
		is >> a.name;
		is >> a.pathFromRoot;
	
		//and return the stream
		return is;
	}
}

//end of implementation
