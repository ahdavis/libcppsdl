/*
 * Bundle.h
 * Declares a class that represents an asset bundle
 * Created by Andrew Davis
 * Created on 6/22/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include "../util/Serializable.h"
#include "Asset.h"
#include <map>
#include <vector>
#include <string>
#include <iostream>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Bundle final : public Serializable 
	{
		//public fields and methods
		public:
			//default constructor
			Bundle();

			//vector constructor
			Bundle(const std::vector<Asset>& newAssets);

			//destructor
			~Bundle();

			//copy constructor
			Bundle(const Bundle& b);

			//assignment operator
			Bundle& operator=(const Bundle& src);

			//getter methods
			
			//returns the Assets in the bundle
			std::vector<Asset> getAssets() const;

			//returns the Asset corresponding 
			//to a string name
			const Asset& assetForName(const 
						std::string& name) const;

			//returns the total number of Assets in the Bundle
			int getCount() const;

			//returns whether the Bundle is empty
			bool isEmpty() const;

			//returns whether the Bundle contains a named Asset
			bool hasAssetWithName(const std::string& 
						name) const;

			//other methods
			
			//adds an Asset to the Bundle
			void addAsset(const Asset& a);

			//removes a named asset from the Bundle
			//returns whether the asset was actually
			//removed
			bool rmAssetWithName(const std::string& name);

			//clears the Bundle
			void clear();

			//serializes the Bundle
			bool serialize(const std::string& path) override;

			//deserializes the Bundle
			bool deserialize(const std::string& path) override;
			
			//serialization operators
			
			//serialization operator
			friend std::ostream& operator<<(std::ostream& os,
							const Bundle& b);

			//deserialization operator
			friend std::istream& operator>>(std::istream& is,
							Bundle& b);

		//private fields and methods
		private:
			//fields
			std::map<std::string, Asset> db; //the database
			int count; //the number of Assets in the database
	};
}

//end of header
