/*
 * Asset.h
 * Declares a class that represents an asset
 * Created by Andrew Davis
 * Created on 6/22/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <iostream>
#include <string>
#include "AssetType.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Asset {
		//public fields and methods
		public:
			//default constructor
			Asset();

			//main constructor
			Asset(AssetType newType, 
				const std::string& newName,
					const std::string& newRootPath);

			//destructor
			virtual ~Asset();

			//copy constructor
			Asset(const Asset& a);

			//assignment operator
			Asset& operator=(const Asset& src);

			//getter methods
			
			//returns the type of the Asset
			AssetType getType() const;

			//returns the name of the Asset
			const std::string& getName() const;

			//returns the absolute path to the Asset
			std::string getPath() const;

			//returns the ID for the Asset
			int getID() const;

			//serialization operators
			
			//serialization operator
			friend std::ostream& operator<<(std::ostream& os,
							const Asset& a);

			//deserialization operator
			friend std::istream& operator>>(std::istream& is,
							Asset& a);

		//protected fields and methods
		protected:
			//fields
			static int curID; //the current ID to assign
			AssetType type; //the type of the Asset
			std::string name; //the name of the Asset
			std::string pathFromRoot; //the path to the Asset
			int id; //the ID for the Asset
	};
}

//end of header
