/*
 * Bundle.cpp
 * Implements a class that represents an asset bundle
 * Created by Andrew Davis
 * Created on 6/22/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "Bundle.h"

//includes
#include <fstream>
#include "../util/functions.h"
#include "AssetType.h"
#include "../except/AssetException.h"
#include "../except/BundleException.h"

//default constructor
CPPSDL::Bundle::Bundle()
	: db(), count(0) //init the fields
{
	//no code needed
}

//vector constructor
CPPSDL::Bundle::Bundle(const std::vector<CPPSDL::Asset>& newAssets)
	: db(), count(0)
{
	//loop and add each asset to the database
	for(const auto& asset : newAssets) {
		this->addAsset(asset);
	}
}

//destructor
CPPSDL::Bundle::~Bundle() {
	//no code needed
}

//copy constructor
CPPSDL::Bundle::Bundle(const CPPSDL::Bundle& b)
	: db(b.db), count(b.count)
{
	//no code needed
}

//assignment operator
CPPSDL::Bundle& CPPSDL::Bundle::operator=(const CPPSDL::Bundle& src) {
	this->db = src.db; //assign the database field
	this->count = src.count; //assign the count field
	return *this; //and return the instance
}

//getAssets method - returns the Assets in the Bundle
std::vector<CPPSDL::Asset> CPPSDL::Bundle::getAssets() const {
	std::vector<CPPSDL::Asset> ret; //the return value

	//loop and put the values of the database into the return vector
	for(auto it = this->db.begin(); it != this->db.end(); ++it) {
		ret.push_back(it->second);
	}

	//and return the return vector
	return ret;
}

//assetForName method - returns a named Asset from the Bundle
const CPPSDL::Asset& CPPSDL::Bundle::assetForName(const 
					std::string& name) const {
	//determine whether the database contains the name
	if(this->db.count(name) == 0) { //if the name is not in the db
		//then throw an exception
		throw CPPSDL::AssetException(name);
	} else { //if the name is in the db
		return this->db.at(name); //then return its value
	}
}

//getCount method - returns the number of Assets in the Bundle
int CPPSDL::Bundle::getCount() const {
	return this->count; //return the count field
}

//isEmpty method - returns whether the Bundle is empty
bool CPPSDL::Bundle::isEmpty() const {
	return this->count == 0; //return whether the count is 0
}

//hasAssetWithName method - returns whether the Bundle 
//contains a named Asset
bool CPPSDL::Bundle::hasAssetWithName(const std::string& name) const {
	return (this->db.count(name) == 1); 
}

//addAsset method - adds an Asset to the Bundle
void CPPSDL::Bundle::addAsset(const CPPSDL::Asset& a) {
	this->db[a.getName()] = a; //put the Asset in the database
	this->count++; //and increment the asset count
}

//rmAssetWithName method - removes a named Asset from the Bundle
bool CPPSDL::Bundle::rmAssetWithName(const std::string& name) {
	//make sure that the Bundle actually contains the Asset
	if(!this->hasAssetWithName(name)) { //Asset not in Bundle
		return false; //so return false
	} else { //Asset in Bundle
		this->db.erase(name); //remove it
		this->count--; //decrement the count
		return true; //and return true
	}
}

//clear method - clears the Bundle
void CPPSDL::Bundle::clear() {
	this->db.clear(); //clear the database
	this->count = 0; //and zero the count
}

//overridden serialize method - serializes the Bundle
bool CPPSDL::Bundle::serialize(const std::string& path) {
	std::ofstream ofs; //used to write the Bundle data to the file
	ofs.open(path); //open the file
	ofs << *this; //write the Bundle to the file
	ofs.close(); //close the file
	return true; //and return a success
}

//overridden deserialize method - deserializes the Bundle
bool CPPSDL::Bundle::deserialize(const std::string& path) {
	//make sure that the file exists
	if(!CPPSDL::FileExists(path)) { //if the file does not exist
		//then throw an exception
		throw CPPSDL::BundleException(path);
		
		//and return a failure
		return false;
	} else { //if the file does exist
		//then load the Bundle from the file
		std::ifstream ifs;
		ifs.open(path);
		ifs >> *this;
		ifs.close();

		//and return a success
		return true;
	}
}

namespace CPPSDL {
	//serialization operator
	std::ostream& operator<<(std::ostream& os, 
					const Bundle& b) {
		//serialize the count field
		os << b.count << '\n';
	
		//get the Assets as a vector
		std::vector<Asset> assets = b.getAssets();
	
		//loop and serialize each asset
		for(const auto& a : assets) {
			os << a;
		}

		//and return the stream
		return os;
	}	

	//deserialization operator
	std::istream& operator>>(std::istream& is, Bundle& b) {
		//deserialize the count field	
		is >> b.count;

		//loop and deserialize each asset
		for(int i = 0; i < b.count; i++) {
			Asset tmp;
			is >> tmp;
			b.db[tmp.getName()] = tmp;
		}

		//and return the stream
		return is;
	}
}

//end of implementation
