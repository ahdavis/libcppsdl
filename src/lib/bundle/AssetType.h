/*
 * AssetType.h
 * Enumerates types of assets for libcppsdl
 * Created by Andrew Davis
 * Created on 6/22/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include <string>

//namespace declaration
namespace CPPSDL {
	//enum definition
	enum class AssetType {
		IMG, //image
		FNT, //font
		SND, //sound
		MAP, //tilemap
		DAT, //data file
		SAV, //save file
		CHK, //chunk data file
	};

	//converts a string name to an AssetType value
	AssetType typeFromName(const std::string& name);

	//converts an AssetType value to a string name
	std::string nameFromType(AssetType type);
}

//end of enum
