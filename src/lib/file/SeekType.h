/*
 * SeekType.h
 * Declares constants that represent file seek types
 * Created by Andrew Davis
 * Created on 6/6/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include <SDL2/SDL.h>

//outer namespace declaration
namespace CPPSDL {
	//inner namespace declaration
	namespace SeekType {
		//constant declarations
		extern const int Begin; //seek from the start
		extern const int Current; //seek from current position
		extern const int End; //seek from end
	}
}

//end of header
