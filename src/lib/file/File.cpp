/*
 * File.cpp
 * Implements a class that represents a file
 * Created by Andrew Davis
 * Created on 6/6/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "File.h"

//default constructor
CPPSDL::File::File()
	: CPPSDL::File(CPPSDL::FileMode::R) //init with read mode
{
	//no code needed
}

//first main constructor
CPPSDL::File::File(CPPSDL::FileMode newMode)
	: data(nullptr), path(), mode(newMode), openFlag(false)
{
	//no code needed
}

//second main constructor
CPPSDL::File::File(const std::string& newPath, FileMode newMode)
	: data(nullptr), path(newPath), mode(newMode), openFlag(false)
{
	//open the file
	this->open(this->path, this->mode);
}

//destructor
CPPSDL::File::~File() {
	this->close(); //close the file
}

//copy constructor
CPPSDL::File::File(const CPPSDL::File& f)
	: data(nullptr), path(f.path), mode(f.mode), openFlag(f.openFlag)
{
	//open the file
	if(f.isOpen()) { //if the other file is open
		this->open(this->path, this->mode); //then open this file
	}
}

//assignment operator
CPPSDL::File& CPPSDL::File::operator=(const CPPSDL::File& src) {
	//assign the data pointer
	this->close(); //close the current file
	if(src.isOpen()) { //if the other file is open
		this->open(src.path, src.mode); //then open this one
	} else { //if it isn't
		this->path = src.path; //assign the path
		this->mode = src.mode; //assign the mode
		this->openFlag = src.openFlag; //assign the open flag
	}

	return *this; //return the instance
}

//getPath method - returns the path to the file
const std::string& CPPSDL::File::getPath() const {
	return this->path; //return the path field
}

//getMode method - returns the file's open mode
CPPSDL::FileMode CPPSDL::File::getMode() const {
	return this->mode; //return the mode field
}

//isOpen method - returns whether the file is open
bool CPPSDL::File::isOpen() const {
	return this->openFlag; //return the open flag
}

//tell method - returns the current read/write position
long CPPSDL::File::tell() const {
	return SDL_RWtell(this->data); //return the read/write position
}

//first open method - opens the file
void CPPSDL::File::open(const std::string& newPath, FileMode newMode) {
	this->close(); //close the current file
	this->path = newPath; //update the path string
	this->mode = newMode; //update the file mode

	//attempt to open the file
	this->data = SDL_RWFromFile(this->path.c_str(), CPPSDL::modeStr(
						this->mode).c_str());

	//verify that the file was opened successfully
	if(this->data == nullptr) { //if the file failed to open
		//then throw an exception
		throw CPPSDL::FileException(this->path, this->mode);
	}

	//and update the open flag
	this->openFlag = true;
}

//second open method - opens the file
void CPPSDL::File::open(const std::string& newPath) {
	this->open(newPath, this->mode); //call the other open method
}

//close method - closes the file
void CPPSDL::File::close() {
	if(this->openFlag) { //if the file is open
		SDL_RWclose(this->data); //then close it
		this->data = nullptr; //zero it out
		this->openFlag = false; //and update the open flag
	}
}

//seek method - seeks to a specific point in the file
long CPPSDL::File::seek(long offset, int seekType) {
	//seek to the offset
	return SDL_RWseek(this->data, offset, seekType);
}

//writeBytes method - writes a sequence of bytes to the file
void CPPSDL::File::writeBytes(const std::vector<std::uint8_t>& bytes) {
	for(const std::uint8_t& byte : bytes) { //loop through the bytes
		this->writeByte(byte); //and write each byte to the file
	}
}

//writeByte method - writes a single byte to the file
void CPPSDL::File::writeByte(std::uint8_t byte) {
	//write the byte to the file
	SDL_RWwrite(this->data, &byte, sizeof(std::uint8_t), 1);
}

//writeInt method - writes an integer to the file
void CPPSDL::File::writeInt(int value) {
	//write the integer to the file
	SDL_RWwrite(this->data, &value, sizeof(int), 1);
}

//writeString method - writes a string to the file
void CPPSDL::File::writeString(const std::string& str) {
	//loop and write the string to the file
	for(const char& c : str) {
		this->writeByte(static_cast<std::uint8_t>(c));
	}
}

//writeDouble method - writes a double to the file
void CPPSDL::File::writeDouble(double value) {
	//write the double to the file
	SDL_RWwrite(this->data, &value, sizeof(double), 1);
}

//first readBytes method - reads a sequence of bytes from the file
//up to and including the terminator argument
std::vector<std::uint8_t> CPPSDL::File::readBytes(std::uint8_t term) {
	std::vector<std::uint8_t> ret; //the return value

	//loop and read bytes
	while(true) {
		std::uint8_t byte; //the read byte

		//read the byte from the file
		std::size_t numRead = SDL_RWread(this->data, &byte,
						sizeof(std::uint8_t), 1);

		//determine whether to exit the loop
		if(numRead == 0) { //error or EOF
			break;
		}

		//add the byte to the return vector
		ret.push_back(byte);

		//determine whether the terminator was read
		if(byte == term) { //if the terminator was read
			break; //then exit the loop
		}
		
	}

	//return the vector
	return ret;
}

//second readBytes method - reads a specific number of bytes from the file
std::vector<std::uint8_t> CPPSDL::File::readBytes(int num) {
	std::vector<std::uint8_t> ret; //the return value

	//loop and read in bytes
	for(int i = 0; i < num; i++) {
		std::uint8_t byte; //the read byte

		//read in a byte
		std::size_t numRead = SDL_RWread(this->data, &byte, 
					sizeof(std::uint8_t), 1);

		//determine whether to exit the loop
		if(numRead == 0) { //error or EOF
			break;
		}

		//add the read byte to the vector
		ret.push_back(byte);
	}

	//return the byte vector
	return ret;
}

//readByte method - reads a byte from the file
std::uint8_t CPPSDL::File::readByte() {
	std::uint8_t ret; //the byte to be read and returned

	//read in the byte
	SDL_RWread(this->data, &ret, sizeof(std::uint8_t), 1);

	//and return the byte
	return ret;
}

//readInt method - reads an int from the file
int CPPSDL::File::readInt() {
	int ret; //the int to be read and returned

	//read in the int
	SDL_RWread(this->data, &ret, sizeof(int), 1);

	//and return it
	return ret;
}

//readString method - reads a string from the file
std::string CPPSDL::File::readString() {
	//get a null-terminated vector of bytes
	std::vector<std::uint8_t> bytes = this->readBytes('\0');

	//get a stringstream
	std::stringstream ss;

	//loop and assemble the string
	for(const std::uint8_t& byte : bytes) {
		ss << static_cast<unsigned char>(byte);
	}

	//and return the string
	return ss.str();
}

//readDouble method - reads a double from the file
double CPPSDL::File::readDouble() {
	double ret; //the return value

	//read the value from the file
	SDL_RWread(this->data, &ret, sizeof(double), 1);

	//and return it
	return ret;
}

//end of implementation
