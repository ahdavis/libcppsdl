/*
 * FileMode.h
 * Enumerates file opening modes
 * Created by Andrew Davis
 * Created on 6/6/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include <string>

//namespace declaration
namespace CPPSDL {
	//enum definition
	enum class FileMode {
		R, //Open for reading (file must exist)
		W, //Open for writing
		A, //Append
		RP, //Open for reading and writing (file must exist)
		WP, //Open for reading and writing
		AP, //Open for reading and appending
		RB, //Like R, but with a binary file
		WB, //Like W, but with a binary file
		AB, //Like A, but with a binary file
		RPB, //Like RP, but with a binary file
		WPB, //Like WP, but with a binary file
		APB //Like AP, but with a binary file
	};

	//function prototype
	
	//converts a FileMode to a string
	std::string modeStr(FileMode mode); 
}

//end of header
