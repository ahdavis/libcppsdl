/*
 * SeekType.cpp
 * Defines constants that represent file seek types
 * Created by Andrew Davis
 * Created on 6/6/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "SeekType.h"

//constant definitions
const int CPPSDL::SeekType::Begin = RW_SEEK_SET;
const int CPPSDL::SeekType::Current = RW_SEEK_CUR;
const int CPPSDL::SeekType::End = RW_SEEK_END;

//end of definitions
