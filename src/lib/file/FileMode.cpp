/*
 * FileMode.cpp
 * Implements a function that converts a FileMode to a string
 * Created by Andrew Davis
 * Created on 6/6/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "FileMode.h"

//modeStr function - converts a FileMode to a string
std::string CPPSDL::modeStr(CPPSDL::FileMode mode) {
	//declare the return value
	std::string ret;

	//switch on the file mode and generate the mode string
	switch(mode) {
		case CPPSDL::FileMode::R:
			{
				ret = "r";
				break;
			}
		case CPPSDL::FileMode::W:
			{
				ret = "w";
				break;
			}
		case CPPSDL::FileMode::A:
			{
				ret = "a";
				break;
			}
		case CPPSDL::FileMode::RP:
			{
				ret = "r+";
				break;
			}
		case CPPSDL::FileMode::WP:
			{
				ret = "w+";
				break;
			}
		case CPPSDL::FileMode::AP:
			{
				ret = "a+";
				break;
			}
		case CPPSDL::FileMode::RB:
			{
				ret = "rb";
				break;
			}
		case CPPSDL::FileMode::WB:
			{
				ret = "wb";
				break;
			}
		case CPPSDL::FileMode::AB:
			{
				ret = "ab";
				break;
			}
		case CPPSDL::FileMode::RPB:
			{
				ret = "r+b";
				break;
			}
		case CPPSDL::FileMode::WPB:
			{
				ret = "w+b";
				break;
			}
		case CPPSDL::FileMode::APB:
			{
				ret = "a+b";
				break;
			}
		default:
			{
				ret = "r"; //defaults to reading
				break;
			}
	}

	//return the string, which now contains the mode string
	return ret;
}

//end of implementation
