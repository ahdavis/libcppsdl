/*
 * File.h
 * Declares a class that represents a file
 * Created by Andrew Davis
 * Created on 6/6/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <string>
#include <cstdlib>
#include <cstdint>
#include <vector>
#include <sstream>
#include "../except/FileException.h"
#include "FileMode.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class File final {
		//public fields and methods
		public:
			//default constructor
			File();

			//first main constructor
			//specifies only a mode
			explicit File(FileMode newMode);

			//second main constructor
			//specifies a path and a mode
			File(const std::string& newPath,
					FileMode newMode);

			//destructor
			~File();
			
			//copy constructor
			File(const File& f);

			//assignment operator
			File& operator=(const File& src);

			//getter methods
			
			//returns the path to the file
			const std::string& getPath() const;

			//returns the mode used to open the file
			FileMode getMode() const; 

			//returns whether the file is open
			bool isOpen() const;

			//returns the current read/write position
			long tell() const;

			//other methods
			
			//opens the file
			void open(const std::string& newPath,
					FileMode newMode);

			//opens the file if a mode has already
			//been selected
			void open(const std::string& newPath);

			//closes the file
			void close();

			//moves to a specific point in the file
			//returns the final offset, or -1
			//if the seek is unsuccessful
			long seek(long offset, int seekType);

			//writes a sequence of bytes to the file
			void writeBytes(const std::vector<std::uint8_t>&
						bytes);

			//writes a single byte to the file
			void writeByte(std::uint8_t byte);

			//writes an integer to the file
			void writeInt(int value);

			//writes a string to the file
			void writeString(const std::string& str);

			//writes a double to the file
			void writeDouble(double value);

			//reads a sequence of bytes from the file
			//the sequence is terminated by the argument
			std::vector<std::uint8_t> 
				readBytes(std::uint8_t term);

			//reads a specific number of bytes from the file
			std::vector<std::uint8_t> readBytes(int num);

			//reads a single byte from the file
			std::uint8_t readByte();

			//reads an integer from the file
			int readInt();

			//reads a string from the file
			std::string readString();

			//reads a double from the file
			double readDouble();

		//private fields and methods
		private:
			//fields
			SDL_RWops* data; //the file itself
			std::string path; //the path to the file
			FileMode mode; //the open mode of the file
			bool openFlag; //whether the file is open
	};
}

//end of header
