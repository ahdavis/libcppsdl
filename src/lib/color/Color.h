/*
 * Color.h
 * Declares a class that represents a color
 * Created by Andrew Davis
 * Created on 3/17/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <cstdint>
#include <SDL2/SDL.h>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Color {
		//public fields and methods
		public:
			//default constructor
			Color();

			//first main constructor - specifies an alpha value
			Color(std::uint8_t newRed, std::uint8_t newGreen,
					std::uint8_t newBlue, 
					std::uint8_t newAlpha);

			//second main constructor - does not specify an
			//alpha value
			Color(std::uint8_t newRed, 
					std::uint8_t newGreen,
					std::uint8_t newBlue);

			//destructor
			virtual ~Color();

			//copy constructor
			Color(const Color& c);

			//assignment operator
			Color& operator=(const Color& src);

			//getter methods
			
			//returns the red component of the color
			std::uint8_t getRed() const;

			//returns the green component of the color
			std::uint8_t getGreen() const;

			//returns the blue component of the color
			std::uint8_t getBlue() const;

			//returns the alpha value of the color
			std::uint8_t getAlpha() const;

			//setter methods
			
			//sets the red component of the color
			void setRed(std::uint8_t newRed);

			//sets the green component of the color
			void setGreen(std::uint8_t newGreen);

			//sets the blue component of the color
			void setBlue(std::uint8_t newBlue);

			//sets the alpha value of the color
			void setAlpha(std::uint8_t newAlpha);

		//private fields and methods
		private:
			//friendship declarations
			friend class Surface;
			friend class Renderer;

			//field
			SDL_Color data; //the SDL color instance
	};
}

//end of header
