/*
 * Color.cpp
 * Implements a class that represents a color
 * Created by Andrew Davis
 * Created on 3/17/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Color.h"

//default constructor
CPPSDL::Color::Color()
	: Color(0xFF, 0xFF, 0xFF, 0xFF) //initialize the color to white
{
	//no code needed
}

//first main constructor - specifies an alpha value
CPPSDL::Color::Color(std::uint8_t newRed, std::uint8_t newGreen,
			std::uint8_t newBlue, std::uint8_t newAlpha)
	: data() //init the field
{
	//create the color
	this->data.r = newRed; //init the red component
	this->data.g = newGreen; //init the green component
	this->data.b = newBlue; //init the blue component
	this->data.a = newAlpha; //init the alpha component
}

//second main constructor - does not specify an alpha value
CPPSDL::Color::Color(std::uint8_t newRed, std::uint8_t newGreen,
			std::uint8_t newBlue)
	: Color(newRed, newGreen, newBlue, 0xFF) //call the other ctor
{
	//no code needed
}

//destructor
CPPSDL::Color::~Color() {
	//no code needed
}

//copy constructor
CPPSDL::Color::Color(const CPPSDL::Color& c)
	: data(c.data) //copy the field
{
	//no code needed
}

//assignment operator
CPPSDL::Color& CPPSDL::Color::operator=(const CPPSDL::Color& src) {
	this->data = src.data; //assign the color field
	return *this; //and return the instance
}

//getRed method - returns the red component of the color
std::uint8_t CPPSDL::Color::getRed() const {
	return this->data.r; //return the red component
}

//getGreen method - returns the green component of the color
std::uint8_t CPPSDL::Color::getGreen() const {
	return this->data.g; //return the green component
}

//getBlue method - returns the blue component of the color
std::uint8_t CPPSDL::Color::getBlue() const {
	return this->data.b; //return the blue component
}

//getAlpha method - returns the alpha value of the color
std::uint8_t CPPSDL::Color::getAlpha() const {
	return this->data.a; //return the alpha value
}

//setRed method - sets the red component of the color
void CPPSDL::Color::setRed(std::uint8_t newRed) {
	this->data.r = newRed; //assign the new red component
}

//setGreen method - sets the green component of the color
void CPPSDL::Color::setGreen(std::uint8_t newGreen) {
	this->data.g = newGreen; //assign the new green component
}

//setBlue method - sets the blue component of the color
void CPPSDL::Color::setBlue(std::uint8_t newBlue) {
	this->data.b = newBlue; //assign the new blue component
}

//setAlpha method - sets the alpha value of the color
void CPPSDL::Color::setAlpha(std::uint8_t newAlpha) {
	this->data.a = newAlpha; //assign the new alpha value
}

//end of implementation
