/*
 * Music.cpp
 * Implements a singleton class that represents music
 * Created by Andrew Davis
 * Created on 5/21/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Music.h"

//private constructor
CPPSDL::Music::Music()
	: path(), data(NULL) //init the fields
{
	//no code needed
}

//destructor
CPPSDL::Music::~Music() {
	//no code needed
}

//static getInstance method - returns an instance of the music player
CPPSDL::Music& CPPSDL::Music::getInstance() {
	static CPPSDL::Music instance;
	return instance;
}

//getPath method - returns the path to the source audio
const std::string& CPPSDL::Music::getPath() const {
	return this->path; //return the path field
}

//isPlaying method - returns whether music is currently playing
bool CPPSDL::Music::isPlaying() const {
	return Mix_PlayingMusic() != 0; //return whether music is playing
}

//isPaused method - returns whether music is paused
bool CPPSDL::Music::isPaused() const {
	return Mix_PausedMusic() == 1; //return whether music is paused
}

//hasTrack method - returns whether a track is initialized
bool CPPSDL::Music::hasTrack() const {
	return this->data == NULL; //return whether the data is null
}

//setTrack method - sets the track
void CPPSDL::Music::setTrack(const std::string& trackPath) {
	//load the music
	this->data = std::shared_ptr<Mix_Music>(
			Mix_LoadMUS(trackPath.c_str()), 
			[=](Mix_Music* music) {
				Mix_FreeMusic(music);
				music = NULL;
			});

	//make sure that the music was loaded successfully
	if(this->data == NULL) { //if the music failed to load
		//then throw an exception
		throw CPPSDL::SoundException(trackPath);
	}

	//and update the path field
	this->path = trackPath;
}

//play method - starts music playing or resumes from a pause
void CPPSDL::Music::play() {
	//make sure that the music player is initialized
	if(!this->hasTrack()) { //if no track is set
		return; //then exit the method
	}

	if(!this->isPlaying()) { //if music is not playing
		//then play it
		Mix_PlayMusic(this->data.get(), -1);
	} else { //if it is playing
		if(this->isPaused()) { //if it is paused
			Mix_ResumeMusic(); //then resume it
		}
	}
}

//pause method - pauses music
void CPPSDL::Music::pause() {
	if(this->isPlaying()) { //if music is playing
		Mix_PauseMusic(); //then pause it
	}
}

//stop method - stops the music
void CPPSDL::Music::stop() {
	//stop the music
	Mix_HaltMusic();
}

//end of implementation
