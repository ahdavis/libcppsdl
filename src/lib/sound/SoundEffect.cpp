/*
 * SoundEffect.cpp
 * Implements a class that represents a sound effect
 * Created by Andrew Davis
 * Created on 5/21/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "SoundEffect.h"

//constructor
CPPSDL::SoundEffect::SoundEffect(const std::string& newPath)
	: path(newPath), data(NULL) //init the fields
{
	//load the sound effect
	this->data = std::shared_ptr<Mix_Chunk>(
			Mix_LoadWAV(newPath.c_str()), 
				[=](Mix_Chunk* chunk) {
					Mix_FreeChunk(chunk);
					chunk = NULL;
				});

	//and verify that the sound effect was loaded successfully
	if(this->data == NULL) { //if the effect failed to load
		//then throw an exception
		throw CPPSDL::SoundException(newPath);
	}
}

//destructor
CPPSDL::SoundEffect::~SoundEffect() {
	//no code needed
}

//copy constructor
CPPSDL::SoundEffect::SoundEffect(const CPPSDL::SoundEffect& se) 
	: path(se.path), data(se.data) //copy the fields
{
	//no code needed
}

//assignment operator
CPPSDL::SoundEffect& CPPSDL::SoundEffect::operator=(const 
						CPPSDL::SoundEffect& src) {
	this->path = src.path; //assign the path field
	this->data = src.data; //assign the data pointer
	return *this; //and return the instance
}

//getPath method - returns the path to the source audio
const std::string& CPPSDL::SoundEffect::getPath() const {
	return this->path; //return the path field
}

//play method - plays the sound effect once
void CPPSDL::SoundEffect::play() {
	this->playRepeat(1); //call the playRepeat method
}

//playRepeat method - plays the sound effect multiple times
void CPPSDL::SoundEffect::playRepeat(int times) {
	Mix_PlayChannel(-1, this->data.get(), times - 1);
}

//end of implementation
