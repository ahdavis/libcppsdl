/*
 * Music.h
 * Declares a class that represents music
 * Created by Andrew Davis
 * Created on 5/21/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL_mixer.h>
#include <string>
#include <cstdlib>
#include <memory>
#include "../except/SoundException.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Music final {
		//public fields and methods
		public:
			//constructor is private

			//destructor
			~Music();

			//delete the copy constructor
			Music(const Music& m) = delete;

			//delete the assignment operator
			Music& operator=(const Music& src) = delete;

			//getter methods

			//returns an instance of the music player
			static Music& getInstance();
			
			//returns the path to the source audio
			const std::string& getPath() const;

			//returns whether music is currently playing
			bool isPlaying() const;

			//returns whether music is currently paused
			bool isPaused() const;

			//returns whether a track is set
			bool hasTrack() const;

			//setter method
			
			//changes the track
			void setTrack(const std::string& trackPath);
			
			//other methods

			//starts the music playing or resumes from a pause
			void play();

			//pauses the music
			void pause();

			//stops the music
			void stop();

		//private fields and methods
		private:
			//private constructor
			Music();

			//fields
			std::string path; //the path to the source audio

			//the raw music
			std::shared_ptr<Mix_Music> data;
	};
}

//end of header
