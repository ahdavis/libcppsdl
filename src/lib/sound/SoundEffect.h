/*
 * SoundEffect.h
 * Declares a class that represents a sound effect
 * Created by Andrew Davis
 * Created on 5/21/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL_mixer.h>
#include <string>
#include <memory>
#include <cstdlib>
#include "../except/SoundException.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class SoundEffect final {
		//public fields and methods
		public:
			//constructor
			explicit SoundEffect(const std::string& newPath);

			//destructor
			~SoundEffect();

			//copy constructor
			SoundEffect(const SoundEffect& se);

			//assignment operator
			SoundEffect& operator=(const SoundEffect& src);

			//getter method
			
			//returns the path to the sound effect's source
			const std::string& getPath() const;

			//other methods
			
			//plays the sound effect once
			void play();

			//plays the sound effect multiple times
			//DO NOT call this with a times value < 1!
			void playRepeat(int times);

		//private fields and methods
		private:
			//fields
			std::string path; //the path to the source audio

			//the raw sound effect
			std::shared_ptr<Mix_Chunk> data;
	};
}

//end of header
