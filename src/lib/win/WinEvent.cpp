/*
 * WinEvent.cpp
 * Defines constants that describe window events
 * Created by Andrew Davis
 * Created on 6/11/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "WinEvent.h"

//constant definitions
const std::uint8_t CPPSDL::WinEvent::Shown = SDL_WINDOWEVENT_SHOWN;
const std::uint8_t CPPSDL::WinEvent::Hidden = SDL_WINDOWEVENT_HIDDEN;
const std::uint8_t CPPSDL::WinEvent::Exposed = SDL_WINDOWEVENT_EXPOSED;
const std::uint8_t CPPSDL::WinEvent::Moved = SDL_WINDOWEVENT_MOVED;
const std::uint8_t CPPSDL::WinEvent::Resized = SDL_WINDOWEVENT_RESIZED;
const std::uint8_t CPPSDL::WinEvent::SizeChanged = 
			SDL_WINDOWEVENT_SIZE_CHANGED;
const std::uint8_t CPPSDL::WinEvent::Minimized = SDL_WINDOWEVENT_MINIMIZED;
const std::uint8_t CPPSDL::WinEvent::Maximized = SDL_WINDOWEVENT_MAXIMIZED;
const std::uint8_t CPPSDL::WinEvent::Restored = SDL_WINDOWEVENT_RESTORED;
const std::uint8_t CPPSDL::WinEvent::MouseEnter = SDL_WINDOWEVENT_ENTER;
const std::uint8_t CPPSDL::WinEvent::MouseLeave = SDL_WINDOWEVENT_LEAVE;
const std::uint8_t CPPSDL::WinEvent::KeyGained = 
			SDL_WINDOWEVENT_FOCUS_GAINED;
const std::uint8_t CPPSDL::WinEvent::KeyLost =
			SDL_WINDOWEVENT_FOCUS_LOST;
const std::uint8_t CPPSDL::WinEvent::Closed = SDL_WINDOWEVENT_CLOSE;
const std::uint8_t CPPSDL::WinEvent::Focused = SDL_WINDOWEVENT_TAKE_FOCUS;
const std::uint8_t CPPSDL::WinEvent::HitTest = SDL_WINDOWEVENT_HIT_TEST;

//end of definitions
