/*
 * Surface.cpp
 * Implements a class that represents an SDL surface
 * Created by Andrew Davis
 * Created on 3/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Surface.h"

//include the img_element header
#include "../element/ImgElement.h"

//window constructor
CPPSDL::Surface::Surface(const CPPSDL::Window& w)
	: data(NULL), isScreen(true), width(0), height(0) //init the fields
{
	//init the data field
	this->data = std::shared_ptr<SDL_Surface>(
			SDL_GetWindowSurface(w.data.get()),
			[=](SDL_Surface* surface) {
			//do nothing 
			});

	//and init the width and height fields
	this->width = this->data->w;
	this->height = this->data->h;
}

//image constructor
CPPSDL::Surface::Surface(const std::string& path)
	: data(NULL), isScreen(false), width(0), height(0) //init the fields
{
	std::string ext; //holds the file extension

	//get the file extension
	if(path.find_last_of(".") != std::string::npos) {
		ext = path.substr(path.find_last_of(".") + 1);
	} else { //no file extension
		throw CPPSDL::ExtException(path); //so throw an exception
	}

	bool isIMG = false; //was the image loaded with SDL_image?

	//init the Surface pointer
	if(ext != "bmp") { //image that requires SDL_image to load
		//load the PNG image
		this->data = 
		std::shared_ptr<SDL_Surface>(IMG_Load(path.c_str()),
				[=](SDL_Surface* surface) {
				SDL_FreeSurface(surface);
				surface = NULL; });

		isIMG = true; //and set the image flag
	} else { //BMP image
		//load the BMP image
		this->data =
		std::shared_ptr<SDL_Surface>(SDL_LoadBMP(path.c_str()),
				[=](SDL_Surface* surface) {
				SDL_FreeSurface(surface);
				surface = NULL; });
	}

	//handle whether the image failed to load
	if(this->data == NULL) { //if the image failed to load
		if(isIMG) { //if the image was loaded with SDL_image
			//then use IMG_GetError()
			throw CPPSDL::ImgException(path, IMG_GetError());
		} else { //if the image was loaded with core SDL
			//then use SDL_GetError()
			throw CPPSDL::ImgException(path, SDL_GetError());
		}
	}

	//and init the width and height fields
	this->width = this->data->w;
	this->height = this->data->h;
}

//text constructor
CPPSDL::Surface::Surface(const CPPSDL::Font& f, const std::string& text,
				const CPPSDL::Color& textColor)
	: data(NULL), isScreen(false), width(0), height(0)
{

	//create the Surface data from the font and the text
	this->data = std::shared_ptr<SDL_Surface>(
			TTF_RenderText_Solid(f.data.get(), text.c_str(), 
						textColor.data),
			[=](SDL_Surface* surface) {
				SDL_FreeSurface(surface);
				surface = NULL; });

	//make sure that the Surface data is valid
	if(this->data == NULL) { //if the Surface failed to load
		//then throw an exception
		throw CPPSDL::TextException(text);
	}

	//and init the width and height fields
	this->width = this->data->w;
	this->height = this->data->h;
}


//destructor
CPPSDL::Surface::~Surface() {
	//no code needed
}

//copy constructor
CPPSDL::Surface::Surface(const CPPSDL::Surface& s)
	: data(s.data), isScreen(s.isScreen), width(s.width), 
		height(s.height) //copy the fields
{
	//no code needed
}

//assignment operator
CPPSDL::Surface& CPPSDL::Surface::operator=(const CPPSDL::Surface& src) {
	this->data = src.data; //assign the Surface pointer
	this->isScreen = src.isScreen; //assign the screen flag
	this->width = src.width; //assign the width field
	this->height = src.height; //assign the height field
	return *this; //and return the instance
}

//getWidth method - returns the width of the Surface
int CPPSDL::Surface::getWidth() const {
	return this->width; //return the width field
}

//getHeight method - returns the height of the Surface
int CPPSDL::Surface::getHeight() const {
	return this->height; //return the height field
}

//isMain method - returns whether the Surface is the main screen
bool CPPSDL::Surface::isMain() const {
	return this->isScreen; //return whether the surf is the main screen
}

//fill method - fills the Surface with a color determined by an RGB set
void CPPSDL::Surface::fill(const CPPSDL::Color& col) {
	//fill the Surface
	SDL_FillRect(this->data.get(), NULL, SDL_MapRGB(
						this->data->format,
						col.getRed(), 
						col.getGreen(), 
						col.getBlue()));
}

//first blit method - overlays another Surface over this Surface
void CPPSDL::Surface::blit(const CPPSDL::Surface& other) {

	//blit the two Surfaces
	SDL_BlitSurface(other.data.get(), NULL, this->data.get(), NULL);
}

//second blit method - overlays an image element over this Surface
void CPPSDL::Surface::blit(CPPSDL::ImgElement& other) {
	//optimize the image element
	this->optimizeImg(other);

	//and call the other blit method
	this->blit(other.getSurface());
}

//third blit method - overlays another Surface over this Surface
//and scales it according to a rectangle
void CPPSDL::Surface::blit(const CPPSDL::Surface& other,
				CPPSDL::Rect scale) {
	//attempt to scale the other Surface onto the current Surface
	int res = SDL_BlitScaled(other.data.get(), NULL, 
					this->data.get(), &scale.data);

	//and handle errors
	if(res < 0) { //if the blit failed
		//then throw an exception
		throw CPPSDL::SclException();
	}
}

//fourth blit method - overlays an image element over this Surface
//and scales it according to a rectangle
void CPPSDL::Surface::blit(CPPSDL::ImgElement& other,
				CPPSDL::Rect scale) {
	this->optimizeImg(other); //optimize the image element
	this->blit(other.getSurface(), scale); //call the other blit method
}	


//private optimizeImg method - optimizes an image element against
//this Surface
void CPPSDL::Surface::optimizeImg(CPPSDL::ImgElement& other) {
	std::shared_ptr<SDL_Surface> optimizedSurface = NULL;

	//optimize the Surface
	optimizedSurface = std::shared_ptr<SDL_Surface>(
			SDL_ConvertSurface(other.surf.data.get(),
						this->data->format, 0),
			[=](SDL_Surface* surface) {
			SDL_FreeSurface(surface);
			surface = NULL; });

	//make sure that the Surface was optimized successfully
	if(optimizedSurface == NULL) { //if the optimization failed
		//then throw an exception
		throw CPPSDL::OptException(other.getPath());
	}

	//and make the image element's Surface the optimized Surface
	other.surf.data = optimizedSurface;
}

//end of implementation
