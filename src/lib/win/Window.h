/*
 * Window.h
 * Declares a class that represents an SDL window
 * Created by Andrew Davis
 * Created on 3/7/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <cstdlib>
#include <cstdint>
#include <memory>
#include <string>
#include "../except/WindowException.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Window {
		//public fields and methods
		public:
			//first constructor
			Window(const std::string& newCaption,
					int newWidth,
					int newHeight,
					bool shouldShowOnCreate);

			//second constructor
			Window(const std::string& newCaption,
					int newXPos,
					int newYPos,
					int newWidth,
					int newHeight,
					bool shouldShowOnCreate);

			//destructor
			virtual ~Window();

			//copy constructor
			Window(const Window& w);

			//assignment operator
			Window& operator=(const Window& src);

			//getter methods
			
			//returns the caption of the Window
			const std::string& getCaption() const;

			//returns the x-position of the Window
			int getXPos() const;

			//returns the y-position of the Window
			int getYPos() const;

			//returns the width of the Window
			int getWidth() const;

			//returns the height of the Window
			int getHeight() const;

			//returns whether the Window should show on
			//creation
			bool shouldShowOnCreate() const;

			//returns whether the Window is minimized
			bool isMinimized() const;

			//returns whether the Window is maximized
			bool isMaximized() const;

			//returns whether the Window is currently
			//fullscreen
			bool isFullScreen() const;

			//returns the ID of the Window
			std::uint32_t getID() const;

			//returns whether the Window has mouse focus
			bool hasMouseFocus() const;

			//returns whether the Window has keyboard focus
			bool hasKeyFocus() const;

			//returns whether the Window is showing
			bool isShown() const;

			//setter methods
			
			//sets the Window's caption
			void setCaption(const std::string& newCaption);

			//sets the Window's width
			void setWidth(int newWidth);

			//sets the Window's height
			void setHeight(int newHeight);

			//other methods

			//updates the Window
			void update();

			//minimizes the Window
			void minimize();

			//maximizes the Window
			void maximize();

			//restores the Window from a minimized state
			void restore();

			//makes the Window fullscreen
			void enterFullScreen();

			//makes the Window not fullscreen
			void exitFullScreen();

			//gives the Window mouse focus
			void giveMouseFocus();
			
			//removes mouse focus from the Window
			void rmMouseFocus();

			//gives the Window keyboard focus
			void giveKeyFocus();

			//removes keyboard focus from the Window
			void rmKeyFocus();

			//shows the Window
			void show();

			//unshows the Window
			void unshow();

			//hides the Window
			void hide();

			//focuses the Window
			void focus();

		//private fields and methods
		private:
			//friendship declarations
			friend class Surface;
			friend class Renderer;
			friend class SimpleMsgBox;
			friend class MsgBox;

			//method
			void initWindowPtr(); //initializes the Window ptr

			//fields
			std::shared_ptr<SDL_Window> data; //the Window ptr
			std::string caption; //the caption of the Window
			int xPos; //the x-position of the Window
			int yPos; //the y-position of the Window
			int width; //the width of the Window
			int height; //the height of the Window
			bool showOnCreate; //should the Window show?
			bool minimized; //is the Window minimized?
			bool maximized; //is the Window maximized?
			bool fullScreen; //is the Window fullscreen?
			bool mouseFocus; //window has mouse focus
			bool keyFocus; //window has keyboard focus
			bool shown; //is the window shown?
	};
}

//end of header
