/*
 * Window.cpp
 * Implements a class that represents an SDL window
 * Created by Andrew Davis
 * Created on 3/7/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Window.h"

//first constructor
CPPSDL::Window::Window(const std::string& newCaption,
				int newWidth,
				int newHeight,
				bool shouldShowOnCreate)
	: CPPSDL::Window(newCaption,
				SDL_WINDOWPOS_UNDEFINED,
				SDL_WINDOWPOS_UNDEFINED,
				newWidth,
				newHeight,
				shouldShowOnCreate) //call the other ctor
{
	//no code needed
}

//second constructor
CPPSDL::Window::Window(const std::string& newCaption,
				int newXPos,
				int newYPos,
				int newWidth,
				int newHeight,
				bool shouldShowOnCreate)
	: data(NULL), caption(newCaption), 
		xPos(newXPos), yPos(newYPos), width(newWidth),
		height(newHeight), showOnCreate(shouldShowOnCreate),
		minimized(false), maximized(false), fullScreen(false),
		mouseFocus(false), keyFocus(false), shown(true)
{
	//init the Window pointer
	this->initWindowPtr();
}

//destructor
CPPSDL::Window::~Window() {
	//no code needed
}

//copy constructor
CPPSDL::Window::Window(const CPPSDL::Window& w)
	: data(w.data), caption(w.caption), xPos(w.xPos), yPos(w.yPos),
		width(w.width), height(w.height), 
			showOnCreate(w.showOnCreate),
			minimized(w.minimized), maximized(w.maximized),
			fullScreen(w.fullScreen), 
			mouseFocus(w.mouseFocus), keyFocus(w.keyFocus),
			shown(w.shown)
{
	//no code needed
}

//assignment operator
CPPSDL::Window& CPPSDL::Window::operator=(const CPPSDL::Window& src) {
	this->data = src.data; //assign the data field
	this->caption = src.caption; //assign the caption
	this->xPos = src.xPos; //assign the x-position
	this->yPos = src.yPos; //assign the y-position
	this->width = src.width; //assign the width
	this->height = src.height; //assign the height
	this->showOnCreate = src.showOnCreate; //assign the show flag
	this->minimized = src.minimized; //assign the minimized flag
	this->maximized = src.maximized; //assign the maximized flag
	this->fullScreen = src.fullScreen; //assign the fullscreen flag
	this->mouseFocus = src.mouseFocus; //assign the mouse focus flag
	this->keyFocus = src.keyFocus; //assign the key focus flag
	this->shown = src.shown; //assign the shown flag
	this->update(); //update the Window
	return *this; //and return the instance
}

//getter methods

//getCaption method - returns the caption of the Window
const std::string& CPPSDL::Window::getCaption() const {
	return this->caption; //return the caption field
}

//getXPos method - returns the x-position of the Window
int CPPSDL::Window::getXPos() const {
	return this->xPos; //return the x-position field
}

//getYPos method - returns the y-position of the Window
int CPPSDL::Window::getYPos() const {
	return this->yPos; //return the y-position field
}

//getWidth method - returns the width of the Window
int CPPSDL::Window::getWidth() const {
	return this->width; //return the width field
}

//getHeight method - returns the height of the Window
int CPPSDL::Window::getHeight() const {
	return this->height; //return the height field
}

//hasMouseFocus method - returns whether the Window has mouse focus
bool CPPSDL::Window::hasMouseFocus() const {
	return this->mouseFocus; //return the mouse focus flag
}

//hasKeyFocus method - returns whether the Window has keyboard focus
bool CPPSDL::Window::hasKeyFocus() const {
	return this->keyFocus; //return the key focus flag
}

//isShown method - returns whether the Window is shown
bool CPPSDL::Window::isShown() const {
	return this->shown; //return the shown flag
}

//shouldShowOnCreate method - returns whether the Window should show
//on creation
bool CPPSDL::Window::shouldShowOnCreate() const {
	return this->showOnCreate; //return the creation flag
}

//setCaption method - sets the caption of the Window
void CPPSDL::Window::setCaption(const std::string& newCaption) {
	this->caption = newCaption; //update the caption field

	//and update the window caption
	SDL_SetWindowTitle(this->data.get(), this->caption.c_str());
}

//isMinimized method - returns whether the Window is minimized
bool CPPSDL::Window::isMinimized() const {
	return this->minimized; //return the minimized flag
}

//isMaximized method - returns whether the Window is maximized
bool CPPSDL::Window::isMaximized() const {
	return this->maximized; //return the maximized flag
}

//isFullScreen method - returns whether the Window is fullscreen
bool CPPSDL::Window::isFullScreen() const {
	return this->fullScreen; //return the fullscreen flag
}

//getID method - returns the ID of the Window
std::uint32_t CPPSDL::Window::getID() const {
	return SDL_GetWindowID(this->data.get()); //return the window ID
}

//setWidth method - sets the width of the Window
void CPPSDL::Window::setWidth(int newWidth) {
	this->width = newWidth; //update the width field
	this->initWindowPtr(); //and update the window pointer
}

//setHeight method - sets the height of the Window
void CPPSDL::Window::setHeight(int newHeight) {
	this->height = newHeight; //update the height field
	this->initWindowPtr(); //and update the window pointer
}

//update method - updates the Window
void CPPSDL::Window::update() {
	SDL_UpdateWindowSurface(this->data.get()); //update the Window
}

//minimize method - minimizes the Window
void CPPSDL::Window::minimize() {
	this->maximized = false; //clear the maximized flag
	this->minimized = true; //and set the minimized flag
}

//maximize method - maximizes the Window
void CPPSDL::Window::maximize() {
	this->maximized = true; //set the maximized flag
	this->minimized = false; //and clear the minimized flag
}

//restore method - restores the Window from a minimized state
void CPPSDL::Window::restore() {
	this->minimized = false; //clear the minimized flag
}

//enterFullScreen method - makes the Window fullscreen
void CPPSDL::Window::enterFullScreen() {
	this->fullScreen = true; //set the fullscreen flag
	this->minimized = false; //clear the minimized flag

	//and make the window fullscreen
	SDL_SetWindowFullscreen(this->data.get(), SDL_TRUE); 
}

//exitFullScreen method - makes the Window not fullscreen
void CPPSDL::Window::exitFullScreen() {
	this->fullScreen = false; //clear the fullscreen flag

	//and make the window not fullscreen
	SDL_SetWindowFullscreen(this->data.get(), SDL_FALSE);
}

//giveMouseFocus method - gives the Window mouse focus
void CPPSDL::Window::giveMouseFocus() {
	this->mouseFocus = true; //set the mouse focus flag
}

//rmMouseFocus method - removes mouse focus from the Window
void CPPSDL::Window::rmMouseFocus() {
	this->mouseFocus = false; //clear the mouse focus flag
}

//giveKeyFocus method - gives key focus to the Window
void CPPSDL::Window::giveKeyFocus() {
	this->keyFocus = true; //set the key focus flag
}

//rmKeyFocus method - removes key focus from the Window
void CPPSDL::Window::rmKeyFocus() {
	this->keyFocus = false; //clear the key focus flag
}

//show method - shows the Window
void CPPSDL::Window::show() {
	this->shown = true; //set the shown flag
	SDL_ShowWindow(this->data.get()); //and show the window
}

//unshow method - unshows the Window
void CPPSDL::Window::unshow() {
	this->shown = false; //clear the shown flag
}

//hide method - hides the Window
void CPPSDL::Window::hide() {
	this->shown = false; //clear the shown flag
	SDL_HideWindow(this->data.get()); //and hide the window
}

//focus method - focuses the Window
void CPPSDL::Window::focus() {
	//restore the window if needed
	if(!this->shown) { //if the window is not shown
		this->show(); //then show it
	}

	//and move the window forward
	SDL_RaiseWindow(this->data.get());
}

//private initWindowPtr method - initializes the Window pointer
void CPPSDL::Window::initWindowPtr() {
	//get the Window shown flag
	auto showFlag = this->showOnCreate ? SDL_WINDOW_SHOWN :
						SDL_WINDOW_HIDDEN;
	
	//init the data pointer
	this->data = std::shared_ptr<SDL_Window>(SDL_CreateWindow(
					this->caption.c_str(),
					this->xPos,
					this->yPos,
					this->width,
					this->height,
					showFlag | SDL_WINDOW_RESIZABLE),
					[=](SDL_Window* window) {
					SDL_DestroyWindow(window);
					window = NULL; });

	//handle failed initialization of the Window
	if(this->data == NULL) { //if the Window failed to initialize
		//then throw an exception
		throw CPPSDL::WindowException();
	}

}

//end of implementation
