/*
 * Surface.h
 * Declares a class that represents an SDL surface
 * Created by Andrew Davis
 * Created on 3/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once 

//includes
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <memory>
#include <cstdlib>
#include <string>
#include "Window.h"
#include "../except/ImgException.h"
#include "../except/TextException.h"
#include "../except/SclException.h"
#include "../except/OptException.h"
#include "../except/ExtException.h"
#include "../util/Rect.h"
#include "../util/Font.h"
#include "../color/Color.h"

//namespace declaration
namespace CPPSDL {
	//forward declaration of the ImgElement class
	class ImgElement;

	//class declaration
	class Surface {
		//public fields and methods
		public:
			//window constructor
			explicit Surface(const Window& w);

			//image constructor
			explicit Surface(const std::string& path);

			//text constructor
			Surface(const Font& f, const std::string& text, 
					const Color& textColor);

			//destructor
			virtual ~Surface();

			//copy constructor
			Surface(const Surface& s);

			//assignment operator
			Surface& operator=(const Surface& src);

			//getter methods
			
			//returns the width of the Surface
			int getWidth() const;

			//returns the height of the Surface
			int getHeight() const;

			//other methods

			//returns whether the Surface is the main screen
			bool isMain() const;

			//fills the Surface with a color specified by
			//a color object
			void fill(const Color& col);

			//overlays another Surface over this Surface
			void blit(const Surface& other);

			//overlays a image element over this Surface
			void blit(ImgElement& other);
			
			//overlays another Surface over this Surface
			//and scales it according to a rectangle
			void blit(const Surface& other, Rect scale);

			//overlays an image element over this Surface
			//and scales it according to a rectangle
			void blit(ImgElement& other, Rect scale);


		//private fields and methods
		private:
			//friendship declarations
			friend class ImgElement;
			friend class Texture;

			//method
			//optimizes an image element
			void optimizeImg(ImgElement& img);
			
			//fields
			std::shared_ptr<SDL_Surface> data; //the raw pointer
			bool isScreen; //is the Surface the main screen	
			int width; //the width of the Surface
			int height; //the height of the Surface
	};
}

//end of header
