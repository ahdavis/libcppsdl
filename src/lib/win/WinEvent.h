/*
 * WinEvent.h
 * Declares constants that describe window events
 * Created by Andrew Davis
 * Created on 6/11/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <cstdint>

//outer namespace declaration
namespace CPPSDL {
	//inner namespace declaration
	namespace WinEvent {
		//constant declarations
		extern const std::uint8_t Shown; //window shown
		extern const std::uint8_t Hidden; //window hidden
		extern const std::uint8_t Exposed; //window exposed
		extern const std::uint8_t Moved; //window moved
		extern const std::uint8_t Resized; //window resized
		extern const std::uint8_t SizeChanged; //window size changed
		extern const std::uint8_t Minimized; //window minimized
		extern const std::uint8_t Maximized; //window maximized
		extern const std::uint8_t Restored; //window restored
		extern const std::uint8_t MouseEnter; //mouse focus gained
		extern const std::uint8_t MouseLeave; //mouse focus lost
		extern const std::uint8_t KeyGained; //keyboard focus gained
		extern const std::uint8_t KeyLost; //keyboard focus lost
		extern const std::uint8_t Closed; //window closed
		extern const std::uint8_t Focused; //window offered a focus
		extern const std::uint8_t HitTest; //hit test abnormal
	}
}

//end of header
