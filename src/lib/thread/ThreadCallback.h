/*
 * ThreadCallback.h
 * Defines a typedef for thread callbacks
 * Created by Andrew Davis
 * Created on 6/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//no includes

//namespace declaration
namespace CPPSDL {
	//typedef definition
	typedef int(*ThreadCallback)(void*);
}

//end of header
