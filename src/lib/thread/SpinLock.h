/*
 * SpinLock.h
 * Declares a class that represents a spinlock
 * Created by Andrew Davis
 * Created on 6/13/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include <SDL2/SDL.h>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class SpinLock final {
		//public fields and methods
		public:
			//constructor
			SpinLock();

			//destructor
			~SpinLock();

			//copy constructor
			SpinLock(const SpinLock& sl);

			//assignment operator
			SpinLock& operator=(const SpinLock& src);

			//getter method
			
			//returns whether the spinlock is locked
			bool isLocked() const;

			//other methods

			//locks the spinlock
			void lock();

			//unlocks the spinlock
			void unlock();

		//private fields and methods
		private:
			//field
			SDL_SpinLock data; //the spinlock data
			bool locked; //is the spinlock locked?
	};
}

//end of header
