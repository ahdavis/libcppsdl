/*
 * Condition.cpp
 * Implements a class that represents a thread condition
 * Created by Andrew Davis
 * Created on 6/13/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Condition.h"

//constructor
CPPSDL::Condition::Condition()
	: data(NULL) //null the data pointer
{
	//init the data pointer
	this->data = std::shared_ptr<SDL_cond>(SDL_CreateCond(),
					[=](SDL_cond* c) {
						SDL_DestroyCond(c);
						c = NULL;
					});

	//and verify that the data was initialized successfully
	if(this->data == NULL) { //if initialization failed
		//then throw an exception
		throw CPPSDL::CondException();
	}
}

//destructor
CPPSDL::Condition::~Condition() {
	//no code needed
}

//copy constructor
CPPSDL::Condition::Condition(const CPPSDL::Condition& c)
	: data(c.data) //copy the data pointer
{
	//no code needed
}

//assignment operator
CPPSDL::Condition& CPPSDL::Condition::operator=(const 
						CPPSDL::Condition& src) {
	this->data = src.data; //assign the data pointer
	return *this; //and return the instance
}

//wait method - blocks the current thread until a signal is received
//via Condition::signal
void CPPSDL::Condition::wait(const CPPSDL::Mutex& lock) {
	SDL_CondWait(this->data.get(), lock.data.get()); //block the thread
}

//static signal method - sends a signal to a Condition object
void CPPSDL::Condition::signal(const CPPSDL::Condition& target) {
	SDL_CondSignal(target.data.get()); //send the signal
}

//end of implementation
