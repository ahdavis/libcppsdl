/*
 * Semaphore.h
 * Declares a class that represents a semaphore
 * Created by Andrew Davis
 * Created on 6/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <cstdlib>
#include <cstdint>
#include <memory>
#include "../except/SemException.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Semaphore final {
		//public fields and methods
		public:
			//default constructor
			//allows for one data access before locking
			Semaphore();

			//default constructor
			explicit Semaphore(std::uint32_t newLockCount);

			//destructor
			~Semaphore();

			//copy constructor
			Semaphore(const Semaphore& s);

			//assignment operator
			Semaphore& operator=(const Semaphore& src);

			//getter methods

			//returns the current lock count for the semaphore
			std::uint32_t getLockCount() const;

			//returns whether the semaphore is currently
			//locked
			bool isLocked() const;

			//setter method
			
			//sets the lock count for the semaphore
			void setLockCount(std::uint32_t newLockCount);

			//other methods
			
			//locks the semaphore
			void lock();

			//unlocks the semaphore
			void unlock();

		//private fields and methods
		private:
			//fields
			std::shared_ptr<SDL_sem> data; //the semaphore data
			std::uint32_t lockCount; //the access count
	};
}

//end of header
