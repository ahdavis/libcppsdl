/*
 * Mutex.cpp
 * Implements a class that represents a mutex lock
 * Created by Andrew Davis
 * Created on 6/13/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Mutex.h"

//constructor
CPPSDL::Mutex::Mutex()
	: data(NULL), locked(false) //init the fields
{
	//init the data pointer
	this->data = std::shared_ptr<SDL_mutex>(SDL_CreateMutex(),
					[=](SDL_mutex* m) {
						SDL_DestroyMutex(m);
						m = NULL;
					});

	//and verify that the pointer initialized successfully
	if(this->data == NULL) { //if the pointer failed to initialize
		//then throw an exception
		throw CPPSDL::MutexException();
	}
}

//destructor
CPPSDL::Mutex::~Mutex() {
	//no code needed
}

//copy constructor
CPPSDL::Mutex::Mutex(const CPPSDL::Mutex& m)
	: data(m.data), locked(m.locked) //copy the fields
{
	//no code needed
}	

//assignment operator
CPPSDL::Mutex& CPPSDL::Mutex::operator=(const CPPSDL::Mutex& src) {
	this->data = src.data; //assign the data pointer
	this->locked = src.locked; //assign the locked flag
	return *this; //and return the instance
}

//isLocked method - returns whether the mutex is locked
bool CPPSDL::Mutex::isLocked() const {
	return this->locked; //return the locked flag
}

//lock method - locks the mutex
void CPPSDL::Mutex::lock() {
	SDL_LockMutex(this->data.get()); //lock the mutex
	this->locked = true; //and set the locked flag
}

//unlock method - unlocks the mutex
void CPPSDL::Mutex::unlock() {
	SDL_UnlockMutex(this->data.get()); //unlock the mutex
	this->locked = false; //and clear the locked flag
}

//end of implementation
