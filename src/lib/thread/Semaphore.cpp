/*
 * Semaphore.cpp
 * Implements a class that represents a semaphore
 * Created by Andrew Davis
 * Created on 6/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Semaphore.h"

//default constructor
CPPSDL::Semaphore::Semaphore()
	: CPPSDL::Semaphore(1) //call the other constructor
{
	//no code needed
}

//main constructor
CPPSDL::Semaphore::Semaphore(std::uint32_t newLockCount)
	: data(NULL), lockCount(1) //init the fields
{
	//initialize the semaphore
	this->setLockCount(newLockCount);
}

//destructor
CPPSDL::Semaphore::~Semaphore() {
	//no code needed
}

//copy constructor
CPPSDL::Semaphore::Semaphore(const CPPSDL::Semaphore& s)
	: data(s.data), lockCount(s.lockCount) //copy the fields
{
	//no code needed
}

//assignment operator
CPPSDL::Semaphore& CPPSDL::Semaphore::operator=(const CPPSDL::Semaphore&
							src) {
	this->data = src.data; //assign the data field
	this->lockCount = src.lockCount; //assign the current count
	return *this; //and return the instance
}

//getLockCount method - returns the current lock count
//for the semaphore
std::uint32_t CPPSDL::Semaphore::getLockCount() const {
	return this->lockCount; //return the lock count field
}

//isLocked method - returns whether the semaphore is locked
bool CPPSDL::Semaphore::isLocked() const {
	return this->lockCount == 0; //return whether the count is 0
}

//setLockCount method - sets the lock count for the semaphore
void CPPSDL::Semaphore::setLockCount(std::uint32_t newLockCount) {
	//update the lock count
	if(newLockCount > 0) { //if the count is valid
		this->lockCount = newLockCount; //then update the count
	} else { //invalid count
		this->lockCount = 1; //make the count 1
	}

	//update the semaphore
	this->data = std::shared_ptr<SDL_sem>(
			SDL_CreateSemaphore(this->lockCount),
			[=](SDL_sem* sem) {
				SDL_DestroySemaphore(sem);
				sem = NULL;
			});

	//and verify the updated semaphore
	if(this->data == NULL) { //if the update failed
		//then throw an exception
		throw CPPSDL::SemException(this->lockCount);
	}
	
}

//lock method - locks the semaphore
void CPPSDL::Semaphore::lock() {
	if(this->lockCount > 0) { //if the lock count is positive
		this->lockCount--; //then decrement it
		SDL_SemWait(this->data.get()); //and lock the semaphore
	}
}

//unlock method - unlocks the semaphore
void CPPSDL::Semaphore::unlock() {
	this->lockCount++; //increment the lock count
	SDL_SemPost(this->data.get()); //and unlock the semaphore
}

//end of header
