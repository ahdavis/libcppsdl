/*
 * Thread.cpp
 * Implements a class that represents a thread
 * Created by Andrew Davis
 * Created on 6/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Thread.h"

//first constructor
CPPSDL::Thread::Thread(ThreadCallback newCallback,
			const std::string& newName,
			int newData)
	: CPPSDL::Thread(newCallback, newName, (void*)(std::size_t)newData)
{
	//no code needed
}

//second constructor
CPPSDL::Thread::Thread(ThreadCallback newCallback,
			const std::string& newName,
			char newData)
	: CPPSDL::Thread(newCallback, newName, (void*)(std::size_t)newData)
{
	//no code needed
}

//third constructor
CPPSDL::Thread::Thread(ThreadCallback newCallback,
			const std::string& newName,
			double newData)
	: CPPSDL::Thread(newCallback, newName, (void*)(std::size_t)newData)
{
	//no code needed
}

//fourth constructor
CPPSDL::Thread::Thread(ThreadCallback newCallback,
			const std::string& newName,
			bool newData)
	: CPPSDL::Thread(newCallback, newName, (void*)(std::size_t)newData)
{
	//no code needed
}

//fifth constructor
CPPSDL::Thread::Thread(ThreadCallback newCallback,
			const std::string& newName,
			const std::string& newData)
	: thread(NULL), callback(newCallback), 
		name(newName), data(nullptr), memStr(nullptr)
{
	//init the pointer
	this->memStr = std::make_shared<std::string>(newData);
}

//sixth constructor
CPPSDL::Thread::Thread(ThreadCallback newCallback, 
			const std::string& newName,
			const char* newData)
	: CPPSDL::Thread(newCallback, newName, std::string(newData))
{
	//no code needed
}

//private constructor
CPPSDL::Thread::Thread(ThreadCallback newCallback,
			const std::string& newName,
			void* newData)
	: thread(NULL), callback(newCallback), 
		name(newName), data(newData), memStr(nullptr) 
{
	//no code needed
}

//destructor
CPPSDL::Thread::~Thread() {
	//no code needed
}

//copy constructor
CPPSDL::Thread::Thread(const CPPSDL::Thread& t)
	: thread(t.thread), callback(t.callback), 
		name(t.name), data(t.data), memStr(t.memStr)
{
	//no code needed
}

//assignment operator
CPPSDL::Thread& CPPSDL::Thread::operator=(const CPPSDL::Thread& src) {
	this->thread = src.thread; //assign the thread pointer
	this->callback = src.callback; //assign the callback
	this->name = src.name; //assign the name
	this->data = src.data; //assign the data
	this->memStr = src.memStr; //assign the memory string
	return *this; //and return the instance
}

//getName method - returns the name of the thread
const std::string& CPPSDL::Thread::getName() const {
	return this->name; //return the name field	
}

//start method - starts execution of the thread
void CPPSDL::Thread::start() {
	//get a thread from SDL
	if(memStr != nullptr) { //if a string was supplied
		//init the thread field
		this->thread = std::shared_ptr<SDL_Thread>(
				SDL_CreateThread(this->callback,
						this->name.c_str(),
					(void*)this->memStr->c_str()),
				[=](SDL_Thread* t) {
					//do nothing
				});
	} else { //no string supplied
		//init the thread field
		this->thread = std::shared_ptr<SDL_Thread>(
				SDL_CreateThread(this->callback,
						this->name.c_str(),
						this->data),
				[=](SDL_Thread* t) {
					//do nothing
				});
	}

	//make sure that the thread was started successfully
	if(this->thread == NULL) { //if the thread failed to start
		//then throw an exception
		throw CPPSDL::ThreadException(this->name);
	}
}

//wait method - stops execution of the thread
int CPPSDL::Thread::wait() {
	int ret = 0; //the return value

	//wait on the thread
	SDL_WaitThread(this->thread.get(), &ret);

	//zero out the thread pointer
	this->thread = NULL;

	//and return the updated return value
	return ret;
}

//detach method - detaches the thread
void CPPSDL::Thread::detach() {
	SDL_DetachThread(this->thread.get()); //detach the thread
}

//end of implementation
