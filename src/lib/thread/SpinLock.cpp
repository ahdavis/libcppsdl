/*
 * SpinLock.cpp
 * Implements a class that represents a spinlock
 * Created by Andrew Davis
 * Created on 6/13/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "SpinLock.h"

//constructor
CPPSDL::SpinLock::SpinLock()
	: data(0), locked(false) //init the fields
{
	//no code needed
}

//destructor
CPPSDL::SpinLock::~SpinLock() {
	this->data = 0; //zero out the data
	this->locked = false; //and clear the locked flag
}

//copy constructor
CPPSDL::SpinLock::SpinLock(const CPPSDL::SpinLock& sl)
	: data(sl.data), locked(sl.locked) //copy the fields
{
	//no code needed
}

//assignment operator
CPPSDL::SpinLock& CPPSDL::SpinLock::operator=(const CPPSDL::SpinLock& 
						src) {
	this->data = src.data; //assign the data field
	this->locked = src.locked; //assign the locked flag
	return *this; //and return the instance
}

//isLocked method - returns whether the spinlock is locked
bool CPPSDL::SpinLock::isLocked() const {
	return this->locked; //return the locked flag
}

//lock method - locks the spinlock
void CPPSDL::SpinLock::lock() {
	SDL_AtomicLock(&this->data); //lock the spinlock
	this->locked = true; //and set the locked flag
}

//unlock method - unlocks the spinlock
void CPPSDL::SpinLock::unlock() {
	SDL_AtomicUnlock(&this->data); //unlock the spinlock
	this->locked = false; //and clear the locked flag
}

//end of implementation
