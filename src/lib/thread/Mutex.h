/*
 * Mutex.h
 * Declares a class that represents a mutex lock
 * Created by Andrew Davis
 * Created on 6/13/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <cstdlib>
#include <memory>
#include "../except/MutexException.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Mutex final {
		//public fields and methods
		public:
			//constructor
			Mutex();

			//destructor
			~Mutex();

			//copy constructor
			Mutex(const Mutex& m);

			//assignment operator
			Mutex& operator=(const Mutex& src);

			//getter method
			
			//returns whether the mutex is currently locked
			bool isLocked() const;

			//other methods
			
			//locks the mutex
			void lock();

			//unlocks the mutex
			void unlock();

		//private fields and methods
		private:
			//friendship declaration
			friend class Condition;

			//fields
			std::shared_ptr<SDL_mutex> data; //the mutex data
			bool locked; //is the mutex locked?
			
	};
}

//end of header
