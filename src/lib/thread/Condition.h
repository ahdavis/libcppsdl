/*
 * Condition.h
 * Declares a class that represents a thread condition
 * Created by Andrew Davis
 * Created on 6/13/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <memory>
#include <cstdlib>
#include "Mutex.h"
#include "../except/CondException.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Condition final {
		//public fields and methods
		public:
			//constructor
			Condition();

			//destructor
			~Condition();

			//copy constructor
			Condition(const Condition& c);

			//assignment operator
			Condition& operator=(const Condition& src);

			//methods
			
			//blocks thread until a signal is received
			//via Condition::signal
			void wait(const Mutex& lock);

			//sends a signal to a Condition object
			static void signal(const Condition& target);

		//private fields and methods
		private:
			//field
			std::shared_ptr<SDL_cond> data; //the actual data
	};
}

//end of header
