/*
 * Thread.h
 * Declares a class that represents a thread
 * Created by Andrew Davis
 * Created on 6/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <string>
#include <cstdlib>
#include <memory>
#include "ThreadCallback.h"
#include "../except/ThreadException.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Thread final {
		//public fields and methods
		public:
			//first constructor - passes an integer
			//to the callback
			Thread(ThreadCallback newCallback, 
					const std::string& newName,
						int newData);

			//second constructor - passes a character
			//to the callback
			Thread(ThreadCallback newCallback,
					const std::string& newName,
						char newData);

			//third constructor - passes a double
			//to the callback
			Thread(ThreadCallback newCallback,
					const std::string& newName,
						double newData);

			//fourth constructor - passes a boolean
			//to the callback
			Thread(ThreadCallback newCallback,
					const std::string& newName,
						bool newData);

			//fifth constructor - passes a string
			//to the callback
			Thread(ThreadCallback newCallback,
					const std::string& newName,
					const std::string& newData);

			//sixth constructor - passes a C string
			//to the callback
			Thread(ThreadCallback newCallback,
					const std::string& newName,
					const char* newData);

			//destructor
			~Thread();

			//copy constructor
			Thread(const Thread& t);

			//assignment operator
			Thread& operator=(const Thread& src);

			//getter method
			
			//returns the name of the thread
			const std::string& getName() const;

			//other methods
			
			//starts the execution of the thread
			void start();
			
			//stops the execution of the thread
			//returns the value returned by the
			//thread's callback function
			//do not call Thread::detach and this method
			//on the same thread object
			int wait();

			//detaches the thread
			//do not call Thread::wait and this method
			//on the same thread object
			void detach();

		//private fields and methods
		private:
			//private constructor
			Thread(ThreadCallback newCallback,
					const std::string& newName,
						void* newData);

			//fields
			std::shared_ptr<SDL_Thread> thread; //the thread
			ThreadCallback callback; //the thread's callback
			std::string name; //the thread's name
			void* data; //the thread's data 
			std::shared_ptr<std::string> memStr; //fix segfault
	};
}

//end of header
