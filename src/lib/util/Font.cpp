/*
 * Font.cpp
 * Implements a class that represents a font for cppsdl
 * Created by Andrew Davis
 * Created on 4/2/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Font.h"

//constructor
CPPSDL::Font::Font(const std::string& newPath, int newSize)
	: data(NULL), path(newPath), size(newSize) //init the fields
{
	//initialize the data field
	this->data = std::shared_ptr<TTF_Font>(
			TTF_OpenFont(newPath.c_str(), newSize),
			[=](TTF_Font* font) {
				TTF_CloseFont(font);
				font = NULL; });

	//and make sure that it initialized properly
	if(this->data == NULL) { //if the Font failed to load
		//then throw an exception
		throw CPPSDL::FontException(newPath);
	}
}

//destructor
CPPSDL::Font::~Font() {
	//no code needed
}

//copy constructor
CPPSDL::Font::Font(const CPPSDL::Font& f)
	: data(f.data), path(f.path), size(f.size) //copy the fields
{
	//no code needed
}

//assignment operator
CPPSDL::Font& CPPSDL::Font::operator=(const CPPSDL::Font& src) {
	this->data = src.data; //assign the data field
	this->path = src.path; //assign the path field
	this->size = src.size; //assign the size field
	return *this; //and return the instance
}

//getPath method - returns the path to the Font
const std::string& CPPSDL::Font::getPath() const {
	return this->path; //return the path field
}

//getSize method - returns the size of the Font
int CPPSDL::Font::getSize() const {
	return this->size; //return the size field
}

//end of implementation
