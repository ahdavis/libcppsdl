/*
 * Rect.h
 * Declares a class that represents a rectangle
 * Created by Andrew Davis
 * Created on 3/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include "Point.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Rect {
		//public fields and methods
		public:
			//default constructor
			Rect();

			//main constructor
			Rect(int newX, int newY, int newWidth, 
					int newHeight);

			//destructor
			virtual ~Rect();

			//copy constructor
			Rect(const Rect& r);

			//assignment operator
			Rect& operator=(const Rect& src);

			//getter methods
			
			//returns the x-coordinate of the Rectangle
			int getX() const;

			//returns the y-coordinate of the Rectangle
			int getY() const;

			//returns the width of the Rectangle
			int getWidth() const;

			//returns the height of the Rectangle
			int getHeight() const;

			//returns the area of the Rectangle
			int getArea() const;

			//returns the perimeter of the Rectangle
			int getPerimeter() const;

			//setter methods
			
			//sets the x-coordinate of the Rectangle
			void setX(int newX);

			//sets the y-coordinate of the Rectangle
			void setY(int newY);

			//sets the width of the Rectangle
			void setWidth(int newWidth);

			//sets the height of the Rectangle
			void setHeight(int newHeight);

			//other methods
			
			//returns whether a point is inside the Rect
			bool containsPoint(const Point& p) const;

			//returns whether this Rect collides with another
			bool collidesWith(const Rect& other) const;

		//private fields and methods
		private:
			//friendship declarations
			friend class Surface;
			friend class Renderer;

			//field
			SDL_Rect data; //the underlying SDL_Rect instance
	};
}

//end of header
