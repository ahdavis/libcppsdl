/*
 * log.cpp
 * Implements functions that log a message to the console
 * Created by Andrew Davis
 * Created on 3/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "log.h"

//LogMessage function - logs a message to stdout
void CPPSDL::LogMessage(const std::string& msg) {
	SDL_Log("%s\n", msg.c_str()); //log the message
}

//LogWarning function - logs a warning to stdout
void CPPSDL::LogWarning(const std::string& msg) {
	//log the warning
	SDL_LogWarn(SDL_LOG_CATEGORY_APPLICATION, "%s\n", msg.c_str());
}

//LogError function - logs an error to stdout
void CPPSDL::LogError(const std::string& msg) {
	//log the error
	SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "%s\n", msg.c_str());
}

//end of implementation
