/*
 * fliptype.h
 * Declares constants that represent SDL texture flip types
 * Created by Andrew Davis
 * Created on 3/23/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include <SDL2/SDL.h>

//outer namespace declaration
namespace CPPSDL {
	//typedef for flip types
	typedef unsigned int fliptype_t;

	//inner namespace declaration
	namespace fliptype {
		//constant declarations
		extern const fliptype_t none; //no flip
		extern const fliptype_t horiz; //horizontal flip
		extern const fliptype_t vert; //vertical flip
	}
}

//end of header
