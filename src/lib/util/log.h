/*
 * log.h
 * Declares functions that log a message to the console
 * Created by Andrew Davis
 * Created on 3/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <string>
#include <SDL2/SDL.h>

//namespace declaration
namespace CPPSDL {
	//function prototypes
	void LogMessage(const std::string& msg); //logs a message to stdout

	void LogWarning(const std::string& msg); //logs a warning to stdout

	void LogError(const std::string& msg); //logs an error to stdout
}

//end of header
