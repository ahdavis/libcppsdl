/*
 * Serializable.h
 * Declares an interface to be implemented by serializable classes
 * Created by Andrew Davis
 * Created on 6/24/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//include
#include <string>

//namespace declaration
namespace CPPSDL {
	//interface declaration
	class Serializable {
		//public methods
		public:
			//method declarations
			
			//serializes the object to a file
			//returns true if serialization succeeded
			//false otherwise
			virtual bool serialize(const std::string& p) = 0;

			//deserializes the object from a file
			//returns true if deserialization succeeded
			//false otherwise
			virtual bool deserialize(const std::string& p) = 0;

		//protected methods
		protected:
			//default constructor
			Serializable() = default;

			//copy constructor
			Serializable(const Serializable& s) = default;

			//assignment operator
			Serializable& operator=(const Serializable& src)
				= default;
	};
}

//end of interface
