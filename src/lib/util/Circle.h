/*
 * Circle.h
 * Declares a class that represents a circle
 * Created by Andrew Davis
 * Created on 6/3/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include <cmath>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Circle final {
		//public fields and methods
		public:
			//default constructor - creates a unit circle
			//centered at (0,0)
			Circle();

			//main constructor
			Circle(int newX, int newY, int newRadius);

			//destructor
			~Circle();

			//copy constructor
			Circle(const Circle& c);

			//assignment operator
			Circle& operator=(const Circle& src);

			//getter methods
			
			//returns the x-coord of the circle's center
			int getX() const;

			//returns the y-coord of the circle's center
			int getY() const;

			//returns the radius of the circle
			int getRadius() const;

			//returns the area of the circle
			double getArea() const;

			//returns the circumference of the circle
			double getCircumference() const;

			//setter methods
			
			//sets the x-coord of the circle's center
			void setX(int newX);

			//sets the y-coord of the circle's center
			void setY(int newY);

			//sets the radius of the circle's center
			void setRadius(int newRadius);

		//private fields and methods
		private:
			//fields
			int x; //the x-coord of the circle's center
			int y; //the y-coord of the circle's center
			int radius; //the radius of the circle
	};
}

//end of header
