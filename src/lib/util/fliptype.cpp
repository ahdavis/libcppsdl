/*
 * fliptype.cpp
 * Defines constants that represent SDL texture flip types
 * Created by Andrew Davis
 * Created on 3/23/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "fliptype.h"

//constant definitions

//no flip
const CPPSDL::fliptype_t CPPSDL::fliptype::none = SDL_FLIP_NONE; 

//horizontal flip
const CPPSDL::fliptype_t CPPSDL::fliptype::horiz = SDL_FLIP_HORIZONTAL;

//vertical flip
const CPPSDL::fliptype_t CPPSDL::fliptype::vert = SDL_FLIP_VERTICAL;

//end of definitions
