/*
 * Point.h
 * Declares a class that represents a 2D point
 * Created by Andrew Davis
 * Created on 3/23/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include <SDL2/SDL.h>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Point {
		//public fields and methods
		public:
			//default constructor
			Point();

			//main constructor
			Point(int newX, int newY);

			//destructor
			virtual ~Point();

			//copy constructor
			Point(const Point& p);

			//assignment operator
			Point& operator=(const Point& src);

			//getter methods
			
			//returns the x-coord of the Point
			int getX() const;

			//returns the y-coord of the Point
			int getY() const;

			//setter methods
			
			//sets the x-coord of the Point
			void setX(int newX);

			//sets the y-coord of the Point
			void setY(int newY);

		//private fields and methods
		private:
			//friendship declaration
			friend class Renderer;

			//field
			SDL_Point data; //the SDL_Point instance
	};
}

//end of header
