/*
 * Font.h
 * Declares a class that represents a font for cppsdl
 * Created by Andrew Davis
 * Created on 4/2/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <string>
#include <memory>
#include <cstdlib>
#include <SDL2/SDL_ttf.h>
#include "../except/FontException.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Font {
		//public fields and methods
		public:
			//constructor
			Font(const std::string& newPath, int newSize);

			//destructor
			virtual ~Font();

			//copy constructor
			Font(const Font& f);

			//assignment operator
			Font& operator=(const Font& src);

			//getter methods
			
			//returns the path to the Font
			const std::string& getPath() const;

			//returns the size of the Font
			int getSize() const;

		//private fields and methods
		private:
			//friendship declaration
			friend class Surface;

			//fields
			std::shared_ptr<TTF_Font> data; //the Font data
			std::string path; //the path to the Font
			int size; //the size of the Font
	};
}

//end of header
