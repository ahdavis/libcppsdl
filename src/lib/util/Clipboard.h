/*
 * Clipboard.h
 * Declares a singleton class that represents the system clipboard
 * Created by Andrew Davis
 * Created on 6/4/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <string>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Clipboard final {
		//public fields and methods
		public:
			//constructor is private

			//destructor
			~Clipboard();

			//delete the copy constructor
			Clipboard(const Clipboard& c) = delete;

			//delete the assignment operator
			Clipboard& operator=(const Clipboard& src)
				= delete;

			//getter methods
			
			//returns an instance of the clipboard
			static Clipboard& getInstance();
			
			//returns the text on the clipboard
			const std::string& getText() const;

			//returns whether there is text on the clipboard
			bool hasText() const;

			//setter method
			
			//sets the text on the clipboard
			void setText(const std::string& newText);

			//other method
			
			//clears the clipboard
			void clear();

		//private fields and methods
		private:
			//constructor
			Clipboard();

			//method
			void updateState(); //updates the clipboard state

			//field
			std::string text; //the text on the clipboard
	};
}

//end of header
