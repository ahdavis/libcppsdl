/*
 * functions.cpp
 * Implements functions for CPPSDL
 * Created by Andrew Davis
 * Created on 3/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "functions.h"

//private shutdown function - shuts down SDL
static void shutdown() {
	Mix_Quit(); //close SDL_Mixer
	TTF_Quit(); //close SDL_ttf
	IMG_Quit(); //close SDL_img
	SDL_Quit(); //close SDL
}

//Init function - starts up SDL
void CPPSDL::Init() {
	auto cond = SDL_Init(SDL_INIT_EVERYTHING); //init SDL

	if(cond < 0) { //if initialization failed
		//then throw an exception
		throw CPPSDL::InitException("SDL", SDL_GetError());
	}

	//init SDL_image
	int imgFlags = IMG_INIT_PNG;
	if(!(IMG_Init(imgFlags) & imgFlags)) { //if initialization failed
		//then throw an exception
		throw CPPSDL::InitException("SDL_image", IMG_GetError());
	}

	//init SDL_ttf
	if(TTF_Init() == -1) { //if initialization failed
		//then throw an exception
		throw CPPSDL::InitException("SDL_ttf", TTF_GetError());
	}

	//init SDL_mixer
	if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
		//if control reaches here, then SDL_Mixer failed to
		//initialize
		//so throw an exception
		throw CPPSDL::InitException("SDL_mixer", Mix_GetError());
	}

	//register the atexit handler
	int fail = std::atexit(shutdown);

	//and make sure that the registration succeeded
	if(fail != 0) { //if registration failed
		//then log an error
		CPPSDL::LogError("Shutdown handler registration failed");

		//shut down SDL
		shutdown();

		//and exit with an error
		exit(EXIT_FAILURE);
	}
}

//Delay function - delays for a number of milliseconds
void CPPSDL::Delay(unsigned int secs) {
	SDL_Delay(secs); //delay the program
}

//GetMouseCoords function - returns a Point that contains the
//current coordinates of the mouse
CPPSDL::Point CPPSDL::GetMouseCoords() {
	//declare variables
	int x = 0; //will hold the x-coord of the mouse
	int y = 0; //will hold the y-coord of the mouse

	//read the mouse coordinates
	SDL_GetMouseState(&x, &y);

	//and return the coordinates as a point
	return CPPSDL::Point(x, y);
}

//first GetKeyboardState function - returns a pointer to an array that
//holds the current keyboard state
const std::uint8_t* CPPSDL::GetKeyboardState(int& numKeys) {
	return SDL_GetKeyboardState(&numKeys); //return the keyboard state
}

//second GetKeyboardState function - calls the first function with
//a dummy argument
const std::uint8_t* CPPSDL::GetKeyboardState() {
	int dummy = 0; //oh great compiler, accept this offering
	return CPPSDL::GetKeyboardState(dummy); //call the other function
}

//GetTicks function - returns the number of milliseconds since the
//program started
std::uint32_t CPPSDL::GetTicks() {
	return SDL_GetTicks(); //return the elapsed time
}

//GetKeyModState function - returns the current key modifier state
unsigned int CPPSDL::GetKeyModState() {
	return SDL_GetModState(); //return the key modifier state
}

//FileExists function - returns whether a file exists at a path
bool CPPSDL::FileExists(const std::string& path) {
	//attempt to open the file
	FILE* test = std::fopen(path.c_str(), "r");

	//check whether the file opened successfully
	if(test != NULL) { //if the file opened successfully
		std::fclose(test); //then close it
		return true; //and return a success
	} else { //if the file failed to open
		return false; //then return a failure
	}
}

//GetBasePath function - returns the directory that the executable
//linked with this library is run from
std::string CPPSDL::GetBasePath() {
	//declare the return value
	//SDL_GetBasePath() is an expensive call, so we make sure
	//that we only need to call it once
	static std::string ret;

	//generate the path
	if(ret.empty()) { //if the path has not been generated yet
		char* path = SDL_GetBasePath(); //then get the path

		//verify the path
		if(path != NULL) { //if the path was retrieved correctly
			ret = path; //then save it
			SDL_free(path); //and deallocate the memory
		} else { //if the path failed to be retrieved
			//then log an error
			CPPSDL::LogError("Error getting resource path.");
			CPPSDL::LogError(std::string("Message: ")
					+ SDL_GetError());

			//and set a path relative to the current
			//directory
			ret = "./";
		}
	}

	//and return the path
	return ret;
}

//end of implementation
