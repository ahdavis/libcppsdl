/*
 * Rect.cpp
 * Implements a class that represents a rectangle
 * Created by Andrew Davis
 * Created on 3/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Rect.h"

//default constructor
CPPSDL::Rect::Rect()
	: CPPSDL::Rect(0, 0, 1, 1) //call the other constructor
{
	//no code needed
}

//main constructor
CPPSDL::Rect::Rect(int newX, int newY, int newWidth, int newHeight)
	: data() //zero the data field
{
	//populate the data field
	this->setX(newX);
	this->setY(newY);
	this->setWidth(newWidth);
	this->setHeight(newHeight);
}

//destructor
CPPSDL::Rect::~Rect() {
	//no code needed
}

//copy constructor
CPPSDL::Rect::Rect(const CPPSDL::Rect& r)
	: data(r.data) //copy the field
{
	//no code needed
}

//assignment operator
CPPSDL::Rect& CPPSDL::Rect::operator=(const CPPSDL::Rect& src) {
	this->data = src.data; //assign the data field
	return *this; //and return the instance
}

//getX method - returns the x-coordinate of the Rectangle
int CPPSDL::Rect::getX() const {
	return this->data.x; //return the Rectangle's x-coord
}

//getY method - returns the y-coordinate of the Rectangle
int CPPSDL::Rect::getY() const {
	return this->data.y; //return the Rectangle's y-coord
}

//getWidth method - returns the width of the Rectangle
int CPPSDL::Rect::getWidth() const {
	return this->data.w; //return the Rectangle's width
}

//getHeight method - returns the height of the Rectangle
int CPPSDL::Rect::getHeight() const {
	return this->data.h; //return the Rectangle's height
}

//getArea method - returns the area of the Rectangle
int CPPSDL::Rect::getArea() const {
	//area of a rectangle is h * w
	return this->data.h * this->data.w;
}

//getPerimeter method - returns the perimeter of the Rectangle
int CPPSDL::Rect::getPerimeter() const {
	//perimeter of a rectangle is 2h + 2w
	return (2 * this->data.h) + (2 * this->data.w);
}

//setX method - sets the x-coordinate of the Rectangle
void CPPSDL::Rect::setX(int newX) {
	if(newX < 0) { //if the new x-coord is negative
		newX = 0; //then make it 0
	}

	this->data.x = newX; //update the Rectangle's x-coord
}

//setY method - sets the y-coordinate of the Rectangle
void CPPSDL::Rect::setY(int newY) {
	if(newY < 0) { //if the new y-coord is negative
		newY = 0; //then make it 0
	}

	this->data.y = newY; //update the Rectangle's y-coord
}

//setWidth method - sets the width of the Rectangle
void CPPSDL::Rect::setWidth(int newWidth) {
	if(newWidth < 1) { //if the new width is invalid
		newWidth = 1; //then make it 1
	}

	this->data.w = newWidth; //update the Rectangle's width
}

//setHeight method - sets the height of the Rectangle
void CPPSDL::Rect::setHeight(int newHeight) {
	if(newHeight < 1) { //if the new height is invalid
		newHeight = 1; //then make it 1
	}

	this->data.h = newHeight; //update the Rectangle's height
}

//containsPoint method - returns whether a point is inside the Rectangle
bool CPPSDL::Rect::containsPoint(const CPPSDL::Point& p) const {
	bool ret = true; //is the point in the Rectangle?

	//check for the point
	if(p.getX() < this->data.x) {
		ret = false;
	} else if(p.getX() > this->data.x + this->data.w) {
		ret = false;
	} else if(p.getY() < this->data.y) {
		ret = false;
	} else if(p.getY() > this->data.y + this->data.h) {
		ret = false;
	}

	//return whether the point is inside the Rect
	return ret;
}

//collidesWith method - returns whether this Rect collides with another
bool CPPSDL::Rect::collidesWith(const CPPSDL::Rect& other) const {
	//create a dummy SDL_Rect object
	SDL_Rect dummy = {0, 0, 1, 1};

	//get the collision between this rectangle and the other
	SDL_bool res = SDL_IntersectRect(&this->data, &other.data, &dummy);

	//and return the result as a C++ boolean
	return res == SDL_TRUE;
}

//end of implementation
