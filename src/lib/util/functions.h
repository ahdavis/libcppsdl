/*
 * functions.h
 * Declares functions for CPPSDL
 * Created by Andrew Davis
 * Created on 3/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include "log.h"
#include <cstdio>
#include <cstdlib>
#include <utility>
#include <cstdint>
#include <string>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_mixer.h>
#include "../except/InitException.h"
#include "Point.h"

//namespace declaration
namespace CPPSDL {
	//function declarations
	
	//starts up SDL
	void Init();

	//delays for a number of milliseconds
	void Delay(unsigned int secs);

	//returns a point that contains the current coordinates of
	//the mouse
	Point GetMouseCoords();

	//Returns a pointer to an array that holds the keyboard
	//state. Do not manually deallocate this pointer.
	//The parameter will hold the number of keypresses read
	//by the last event loop
	const std::uint8_t* GetKeyboardState(int& numKeys);	

	//Does the same thing as the above function, except that
	//the number of keypresses will not be read
	const std::uint8_t* GetKeyboardState();

	//returns the number of milliseconds since the program started
	std::uint32_t GetTicks();

	//returns the current key modifier state
	unsigned int GetKeyModState();

	//returns whether a file exists at a path
	bool FileExists(const std::string& path);

	//returns the path to the directory that the executable
	//linked to this library is run from
	std::string GetBasePath();
}

//end of header
