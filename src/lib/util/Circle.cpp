/*
 * Circle.cpp
 * Implements a class that represents a circle
 * Created by Andrew Davis
 * Created on 6/3/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Circle.h"

//default constructor
CPPSDL::Circle::Circle()
	: Circle(0, 0, 1) //call the other constructor
{
	//no code needed
}

//main constructor
CPPSDL::Circle::Circle(int newX, int newY, int newRadius)
	: x(0), y(0), radius(0) //zero the fields
{
	//init the fields using setter methods
	this->setX(newX);
	this->setY(newY);
	this->setRadius(newRadius);
}

//destructor
CPPSDL::Circle::~Circle() {
	//no code needed
}

//copy constructor
CPPSDL::Circle::Circle(const CPPSDL::Circle& c)
	: x(c.x), y(c.y), radius(c.radius) //copy the fields
{
	//no code needed
}

//assignment operator
CPPSDL::Circle& CPPSDL::Circle::operator=(const CPPSDL::Circle& src) {
	this->x = src.x; //assign the x-coord
	this->y = src.y; //assign the y-coord
	this->radius = src.radius; //assign the radius
	return *this; //and return the instance
}

//getX method - returns the x-coord of the center of the circle
int CPPSDL::Circle::getX() const {
	return this->x; //return the x-coord
}

//getY method - returns the y-coord of the center of the circle
int CPPSDL::Circle::getY() const {
	return this->y; //return the y-coord
}

//getRadius method - returns the radius of the circle
int CPPSDL::Circle::getRadius() const {
	return this->radius; //return the radius
}

//getArea method - returns the area of the circle
double CPPSDL::Circle::getArea() const {
	//area of a circle is pi * r^2
	return M_PI * std::pow(static_cast<double>(this->radius), 2.0);
}

//getCircumference method - returns the circumference of the circle
double CPPSDL::Circle::getCircumference() const {
	//circumference of a circle is 2 * pi * r
	return M_PI * (2.0 * static_cast<double>(this->radius));
}

//setX method - sets the x-coord of the circle
void CPPSDL::Circle::setX(int newX) {
	//validate the argument
	if(newX < 0) { //if the argument is negative
		this->x = 0; //then make the x-coord field 0
	} else { //if the argument is zero or positive
		this->x = newX; //then use it
	}
}

//setY method - sets the y-coord of the circle
void CPPSDL::Circle::setY(int newY) {
	//validate the argument
	if(newY < 0) { //if the argument is negative
		this->y = 0; //then make the y-coord field 0
	} else { //if the argument is zero or positive
		this->y = newY; //then use it
	}
}

//setRadius method - sets the radius of the circle
void CPPSDL::Circle::setRadius(int newRadius) {
	//validate the argument
	if(newRadius < 1) { //if the argument is zero or below
		this->radius = 1; //then make the radius field 1
	} else { //if the argument is 1 or more
		this->radius = newRadius; //then use it
	}
}

//end of implementation
