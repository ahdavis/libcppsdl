/*
 * Point.cpp
 * Implements a class that represents a 2D point
 * Created by Andrew Davis
 * Created on 3/23/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Point.h"

//default constructor
CPPSDL::Point::Point()
	: Point(0, 0) //call the other constructor
{
	//no code needed
}

//main constructor
CPPSDL::Point::Point(int newX, int newY)
	: data() //init the data field
{
	//and init the data field's components
	this->setX(newX);
	this->setY(newY);
}

//destructor
CPPSDL::Point::~Point() {
	//no code needed
}

//copy constructor
CPPSDL::Point::Point(const CPPSDL::Point& p)
	: data(p.data) //copy the field
{
	//no code needed
}

//assignment operator
CPPSDL::Point& CPPSDL::Point::operator=(const CPPSDL::Point& src) {
	this->data = src.data; //assign the data field
	return *this; //and return the instance
}

//getX method - returns the x-coord of the Point
int CPPSDL::Point::getX() const {
	return this->data.x; //return the data field's x-coord
}

//getY method - returns the y-coord of the Point
int CPPSDL::Point::getY() const {
	return this->data.y; //return the data field's y-coord
}

//setX method - sets the x-coord of the Point
void CPPSDL::Point::setX(int newX) {
	//validate the new x-coord
	if(newX < 0) { //if the new x-coord is negative
		newX = 0; //then make it 0
	}

	//and set the data field's x-coord
	this->data.x = newX;
}

//setY method - sets the y-coord of the Point
void CPPSDL::Point::setY(int newY) {
	//validate the new y-coord
	if(newY < 0) { //if the new y-coord is negative
		newY = 0; //then make it 0
	}

	//and set the data field's y-coord
	this->data.y = newY;
}

//end of implementation 
