/*
 * Clipboard.cpp
 * Implements a singleton class that represents the system clipboard
 * Created by Andrew Davis
 * Created on 6/4/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Clipboard.h"

//private constructor
CPPSDL::Clipboard::Clipboard()
	: text() //init the text field
{
	//update the clipboard state
	this->updateState();
}

//destructor
CPPSDL::Clipboard::~Clipboard() {
	this->clear(); //clear the clipboard
}

//static getInstance method - returns an instance of the clipboard
CPPSDL::Clipboard& CPPSDL::Clipboard::getInstance() {
	static Clipboard instance; //only one of these gets created

	//return the instance
	return instance;
}

//getText method - gets the text on the clipboard
const std::string& CPPSDL::Clipboard::getText() const {
	return this->text; //return the text field
}

//hasText method - returns whether the clipboard has text
bool CPPSDL::Clipboard::hasText() const {
	//get whether the SDL clipboard has text
	bool sdlHasText = (SDL_HasClipboardText() == SDL_TRUE);

	//and return that value
	return sdlHasText;
}

//setText method - sets the text on the clipboard
void CPPSDL::Clipboard::setText(const std::string& newText) {
	this->text = newText; //update the text field
	this->updateState(); //and update the clipboard state
}

//clear method - clears the clipboard
void CPPSDL::Clipboard::clear() {
	this->text.clear(); //clear the text
	this->updateState(); //and update the clipboard state
}

//private updateState method - updates the clipboard state
void CPPSDL::Clipboard::updateState() {
	SDL_SetClipboardText(this->text.c_str()); //set the clipboard state
}

//end of implementation
