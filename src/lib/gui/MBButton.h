/*
 * MBButton.h
 * Declares a class that represents a message box button
 * Created by Andrew Davis
 * Created on 5/22/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <string>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class MBButton final {
		//public fields and methods
		public:
			//constructor
			MBButton(unsigned int flags, int newID,
					const std::string& newText);

			//destructor
			~MBButton();

			//copy constructor
			MBButton(const MBButton& b);

			//assignment operator
			MBButton& operator=(const MBButton& src);

			//getter methods
			
			//returns the button's ID
			int getID() const;

			//returns the button's text
			const std::string& getText() const;

		//private fields and methods
		private:
			//friendship declaration
			friend class MsgBox;

			//fields
			SDL_MessageBoxButtonData data; //the button data
			int id; //the ID of the button
			std::string text; //the button's text
	};
}

//end of header
