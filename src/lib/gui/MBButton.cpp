/*
 * MBButton.cpp
 * Implements a class that represents a message box button
 * Created by Andrew Davis
 * Created on 5/22/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "MBButton.h"

//constructor
CPPSDL::MBButton::MBButton(unsigned int flags, int newID, 
				const std::string& newText)
	: data(), id(newID), text(newText) //init the fields
{
	//init the data field
	this->data = {flags, this->id, this->text.c_str()};
}

//destructor
CPPSDL::MBButton::~MBButton() {
	//no code needed
}

//copy constructor
CPPSDL::MBButton::MBButton(const CPPSDL::MBButton& b)
	: data(b.data), id(b.id), text(b.text) //copy the fields
{
	//no code needed
}

//assignment operator
CPPSDL::MBButton& CPPSDL::MBButton::operator=(const CPPSDL::MBButton& src) {
	this->data = src.data; //assign the data field
	this->id = src.id; //assign the ID field
	this->text = src.text; //assign the text field
	return *this; //and return the instance
}

//getID method - returns the button's ID
int CPPSDL::MBButton::getID() const {
	return this->id; //return the ID field
}

//getText method - returns the button's text
const std::string& CPPSDL::MBButton::getText() const {
	return this->text; //return the text field
}

//end of implementation
