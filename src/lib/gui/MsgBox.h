/*
 * MsgBox.h
 * Declares a class that represents a message box
 * Created by Andrew Davis
 * Created on 5/22/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <string>
#include <vector>
#include <cstdlib>
#include <iostream>
#include "MBButton.h"
#include "MBColorScheme.h"
#include "../win/Window.h"
#include "../except/MsgBoxException.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class MsgBox final {
		//public fields and methods
		public:
			//first constructor - does not accept a
			//window or a color scheme
			MsgBox(unsigned int flags, 
				const std::string& newTitle,
				const std::string& newMessage,
				const std::vector<MBButton>& newButtons);

			//second constructor - does not accept a window
			MsgBox(unsigned int flags,
				const std::string& newTitle,
				const std::string& newMessage,
				const std::vector<MBButton>& newButtons,
				const MBColorScheme& colorScheme);

			//third constructor - does not accept a
			//color scheme
			MsgBox(unsigned int flags,
				const std::string& newTitle,
				const std::string& newMessage,
				const std::vector<MBButton>& newButtons,
				const Window& win);

			//fourth constructor - accepts both a window
			//and a color scheme
			MsgBox(unsigned int flags,
				const std::string& newTitle,
				const std::string& newMessage,
				const std::vector<MBButton>& newButtons,
				const Window& win,
				const MBColorScheme& colorScheme);

			//destructor
			~MsgBox();

			//copy constructor
			MsgBox(const MsgBox& mb);

			//assignment operator
			MsgBox& operator=(const MsgBox& src);

			//getter methods
			
			//returns the title of the message box
			const std::string& getTitle() const;

			//returns the message of the message box
			const std::string& getMessage() const;

			//returns the message box's buttons
			const std::vector<MBButton>& getButtons() const;

			//other method
			
			//displays the message box and returns the index
			//of the clicked button
			int show();

		//private fields and methods
		private:
			//fields
			SDL_MessageBoxData data; //the message box data
			std::string title; //the title of the message box
			std::string message; //the box's message
			std::vector<MBButton> buttons; //the buttons
	};
}

//end of header
