/*
 * MsgBoxFlags.h
 * Declares message box flags
 * Created by Andrew Davis
 * Created on 5/22/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include <SDL2/SDL.h>

//outer namespace declaration
namespace CPPSDL {
	//inner namespace declaration
	namespace MsgBoxFlags {
		//constant declarations
		extern const unsigned int Error; //error dialog
		extern const unsigned int Warning; //warning dialog
		extern const unsigned int Info; //informational dialog
	}
}

//end of header
