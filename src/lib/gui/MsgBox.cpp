/*
 * MsgBox.cpp
 * Implements a class that represents a message box
 * Created by Andrew Davis
 * Created on 5/22/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "MsgBox.h"

//first constructor
CPPSDL::MsgBox::MsgBox(unsigned int flags, const std::string& newTitle,
			const std::string& newMessage,
			const std::vector<CPPSDL::MBButton>& newButtons)
	: data(), title(newTitle), message(newMessage), 
		buttons(newButtons) //init the fields
{
	//get a vector of button data instances
	std::vector<SDL_MessageBoxButtonData> bData;
	for(const CPPSDL::MBButton& button : newButtons) {
		bData.push_back(button.data);
	}

	//init the data field
	this->data = {
		flags,
		NULL,
		this->title.c_str(),
		this->message.c_str(),
		static_cast<int>(this->buttons.size()),
		&bData[0],
		NULL 
	};
}

//second constructor
CPPSDL::MsgBox::MsgBox(unsigned int flags, const std::string& newTitle,
			const std::string& newMessage,
			const std::vector<CPPSDL::MBButton>& newButtons,
			const CPPSDL::MBColorScheme& colorScheme)
	: data(), title(newTitle), message(newMessage),
		buttons(newButtons) //init the fields
{
	//get a vector of button data instances
	std::vector<SDL_MessageBoxButtonData> bData;
	for(const CPPSDL::MBButton& button : newButtons) {
		bData.push_back(button.data);
	}

	//init the data field
	this->data = {
		flags,
		NULL,
		this->title.c_str(),
		this->message.c_str(),
		static_cast<int>(this->buttons.size()),
		&bData[0],
		&colorScheme.data 
	};

}

//third constructor
CPPSDL::MsgBox::MsgBox(unsigned int flags, const std::string& newTitle,
			const std::string& newMessage,
			const std::vector<CPPSDL::MBButton>& newButtons,
			const CPPSDL::Window& win)
	: data(), title(newTitle), message(newMessage),
		buttons(newButtons)
{
	//get a vector of button data instances
	std::vector<SDL_MessageBoxButtonData> bData;
	for(const CPPSDL::MBButton& button : newButtons) {
		bData.push_back(button.data);
	}

	//init the data field
	this->data = {
		flags,
		win.data.get(),
		this->title.c_str(),
		this->message.c_str(),
		static_cast<int>(this->buttons.size()),
		&bData[0],
		NULL 
	};

}

//fourth constructor
CPPSDL::MsgBox::MsgBox(unsigned int flags, const std::string& newTitle,
			const std::string& newMessage,
			const std::vector<CPPSDL::MBButton>& newButtons,
			const CPPSDL::Window& win,
			const CPPSDL::MBColorScheme& colorScheme)
	: data(), title(newTitle), message(newMessage),
		buttons(newButtons)
{
	//get a vector of button data instances
	std::vector<SDL_MessageBoxButtonData> bData;
	for(const CPPSDL::MBButton& button : newButtons) {
		bData.push_back(button.data);
	}

	//init the data field
	this->data = {
		flags,
		win.data.get(),
		this->title.c_str(),
		this->message.c_str(),
		static_cast<int>(this->buttons.size()),
		&bData[0],
		&colorScheme.data 
	};

}

//destructor
CPPSDL::MsgBox::~MsgBox() {
	//no code needed
}

//copy constructor
CPPSDL::MsgBox::MsgBox(const CPPSDL::MsgBox& mb)
	: data(mb.data), title(mb.title), message(mb.message), 
		buttons(mb.buttons)
{
	//no code needed
}

//assignment operator
CPPSDL::MsgBox& CPPSDL::MsgBox::operator=(const CPPSDL::MsgBox& src) {
	this->data = src.data; //assign the data field
	this->title = src.title; //assign the title field
	this->message = src.message; //assign the message field
	this->buttons = src.buttons; //assign the button field
	return *this; //and return the instance
}

//getTitle method - returns the title of the message box
const std::string& CPPSDL::MsgBox::getTitle() const {
	return this->title; //return the title field
}

//getMessage method - returns the message of the message box
const std::string& CPPSDL::MsgBox::getMessage() const {
	return this->message; //return the message field
}

//getButtons method - returns the message box's buttons
const std::vector<CPPSDL::MBButton>& CPPSDL::MsgBox::getButtons() const {
	return this->buttons; //return the button vector
}

//show method - displays the message box and returns the index
//of the clicked button
int CPPSDL::MsgBox::show() {
	int idx = 0; //will contain the index of the selected button
	
	//show the message box and get whether it was successful
	int res = SDL_ShowMessageBox(&this->data, &idx);

	//handle an unsuccessful show
	if(res < 0) { //if the message box failed to show
		std::cout << res << std::endl;
		//then throw an exception
		throw CPPSDL::MsgBoxException(this->title);
	} 

	//if control reaches here, then the box showed successfully
	//so return the index of the selected button
	return idx;
}

//end of implementation
