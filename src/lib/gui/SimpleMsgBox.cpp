/*
 * SimpleMsgBox.cpp
 * Implements a class that represents a simple message box
 * Created by Andrew Davis
 * Created on 5/22/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "SimpleMsgBox.h"

//first constructor
CPPSDL::SimpleMsgBox::SimpleMsgBox(unsigned int newFlags,
					const std::string& newTitle,
					const std::string& newMessage)
	: flags(newFlags), title(newTitle), 
		message(newMessage), win(nullptr)
{
	//no code needed
}

//second constructor
CPPSDL::SimpleMsgBox::SimpleMsgBox(unsigned int newFlags,
					const std::string& newTitle,
					const std::string& newMessage,
					const CPPSDL::Window& newWin)
	: flags(newFlags), title(newTitle),
		message(newMessage), win(nullptr)
{
	//init the window pointer
	this->win = new CPPSDL::Window(newWin);
}

//destructor
CPPSDL::SimpleMsgBox::~SimpleMsgBox() {
	this->free(); //deallocate the message box
}

//copy constructor
CPPSDL::SimpleMsgBox::SimpleMsgBox(const CPPSDL::SimpleMsgBox& smb)
	: flags(smb.flags), title(smb.title),
		message(smb.message), win(nullptr)
{
	//copy the window pointer
	if(smb.win != nullptr) { //if the other message box has a parent
		this->win = new CPPSDL::Window(*smb.win); //then copy it
	}
}

//assignment operator
CPPSDL::SimpleMsgBox& CPPSDL::SimpleMsgBox::operator=(const
						CPPSDL::SimpleMsgBox& src) {
	this->flags = src.flags; //assign the flag field
	this->title = src.title; //assign the title field
	this->message = src.message; //assign the message field

	//assign the window pointer
	this->free(); //deallocate the current window pointer
	if(src.win != nullptr) { //if the other message box has a parent
		//then assign it
		this->win = new CPPSDL::Window(*src.win); 
	}

	//and return the instance
	return *this;
}

//getTitle method - returns the title of the message box
const std::string& CPPSDL::SimpleMsgBox::getTitle() const {
	return this->title; //return the title field
}

//getMessage method - returns the message of the message box
const std::string& CPPSDL::SimpleMsgBox::getMessage() const {
	return this->message; //return the message field
}

//show method - shows the message box
void CPPSDL::SimpleMsgBox::show() const {
	//get the window pointer
	auto ptr = this->win != nullptr ? this->win->data.get() : NULL;

	//attempt to show the message box
	int res = SDL_ShowSimpleMessageBox(this->flags,
						this->title.c_str(),
						this->message.c_str(),
						ptr);

	//and determine whether it showed properly
	if(res < 0) { //if the window failed to show
		//then throw an exception
		throw CPPSDL::MsgBoxException(this->title);
	}
}

//private free method - deallocates the message box
void CPPSDL::SimpleMsgBox::free() {
	delete this->win; //deallocate the window pointer
	this->win = nullptr; //and zero it out
}

//end of implementation
