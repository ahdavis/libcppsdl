/*
 * SimpleMsgBox.h
 * Declares a class that represents a simple message box
 * Created by Andrew Davis
 * Created on 5/22/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <string>
#include <cstdlib>
#include "../win/Window.h"
#include "../except/MsgBoxException.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class SimpleMsgBox final {
		//public fields and methods
		public:
			//first constructor - does not specify a parent
			//window
			SimpleMsgBox(unsigned int newFlags,
					const std::string& newTitle,
					const std::string& newMessage);

			//second constructor - specifies a parent window
			SimpleMsgBox(unsigned int newFlags,
					const std::string& newTitle,
					const std::string& newMessage,
					const Window& newWin);

			//destructor
			~SimpleMsgBox();

			//copy constructor
			SimpleMsgBox(const SimpleMsgBox& smb);

			//assignment operator
			SimpleMsgBox& operator=(const SimpleMsgBox& src);

			//getter methods
			
			//returns the title of the message box
			const std::string& getTitle() const;

			//returns the message of the message box
			const std::string& getMessage() const;

			//other method
			
			//shows the message box
			void show() const;

		//private fields and methods
		private:
			//method
			void free(); //deallocates the message box

			//fields
			unsigned int flags; //the flags of the message box
			std::string title; //the title of the message box
			std::string message; //the message of the box
			Window* win; //the parent window of the box
	};
}

//end of header
