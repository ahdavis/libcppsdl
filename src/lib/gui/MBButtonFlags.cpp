/*
 * MBButtonFlags.cpp
 * Defines message box button flags
 * Created by Andrew Davis
 * Created on 5/22/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "MBButtonFlags.h"

//constant definitions
const unsigned int CPPSDL::MBButtonFlags::None = 0;
const unsigned int CPPSDL::MBButtonFlags::Return =
				SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT;
const unsigned int CPPSDL::MBButtonFlags::Escape =
				SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT;

//end of definitions
