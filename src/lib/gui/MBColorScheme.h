/*
 * MBColorScheme.h
 * Declares a class that represents a message box color scheme
 * Created by Andrew Davis
 * Created on 5/22/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include "../color/Color.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class MBColorScheme final {
		//public fields and methods
		public:
			//constructor
			MBColorScheme(const Color& newBGColor, 
					const Color& newTextColor,
					const Color& newBorderColor,
					const Color& newButtonBGColor,
					const Color& newSelectedColor);

			//destructor
			~MBColorScheme();

			//copy constructor
			MBColorScheme(const MBColorScheme& cs);

			//assignment operator
			MBColorScheme& operator=(const MBColorScheme& src);

			//getter methods
			
			//returns the background color
			const Color& getBGColor() const;

			//returns the text color
			const Color& getTextColor() const;

			//returns the border color
			const Color& getBorderColor() const;

			//returns the button background color
			const Color& getButtonBGColor() const;

			//returns the selected color
			const Color& getSelectedColor() const;

		//private fields and methods
		private:
			//friendship declaration
			friend class MsgBox;

			//fields
			SDL_MessageBoxColorScheme data; //the color scheme
			Color bgColor; //the background color
			Color textColor; //the text color
			Color borderColor; //the border color
			Color buttonBGColor; //the button background color
			Color selectedColor; //the selected color
	};
}

//end of header
