/*
 * MBColorScheme.cpp
 * Implements a class that represents a message box color scheme
 * Created by Andrew Davis
 * Created on 5/22/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "MBColorScheme.h"

//constructor
CPPSDL::MBColorScheme::MBColorScheme(const CPPSDL::Color& newBGColor,
				const CPPSDL::Color& newTextColor,
				const CPPSDL::Color& newBorderColor,
				const CPPSDL::Color& newButtonBGColor,
				const CPPSDL::Color& newSelectedColor)
	: data(), bgColor(newBGColor), textColor(newTextColor),
		borderColor(newBorderColor), 
			buttonBGColor(newButtonBGColor),
			selectedColor(newSelectedColor)
{
	//init the data field
	this->data = {
			{
				{this->bgColor.getRed(),
					this->bgColor.getGreen(),
						this->bgColor.getBlue()},
				
				{this->textColor.getRed(),
					this->textColor.getGreen(),
					this->textColor.getBlue()},

				{this->borderColor.getRed(),
					this->borderColor.getGreen(),
					this->borderColor.getBlue()},

				{this->buttonBGColor.getRed(),
					this->buttonBGColor.getGreen(),
					this->buttonBGColor.getBlue()},

				{this->selectedColor.getRed(),
					this->selectedColor.getGreen(),
					this->selectedColor.getBlue()}
			}
	};
}

//destructor
CPPSDL::MBColorScheme::~MBColorScheme() {
	//no code needed
}

//copy constructor
CPPSDL::MBColorScheme::MBColorScheme(const CPPSDL::MBColorScheme& cs)
	: data(cs.data), bgColor(cs.bgColor), textColor(cs.textColor),
		borderColor(cs.borderColor), 
			buttonBGColor(cs.buttonBGColor),
				selectedColor(cs.selectedColor)
{
	//no code needed
}

//assignment operator
CPPSDL::MBColorScheme& CPPSDL::MBColorScheme::operator=(const
						CPPSDL::MBColorScheme& 
							src) {
	this->data = src.data; //assign the data field
	this->bgColor = src.bgColor; //assign the background color
	this->textColor = src.textColor; //assign the text color
	this->borderColor = src.borderColor; //assign the border color
	this->buttonBGColor = src.buttonBGColor; //assign the button color
	this->selectedColor = src.selectedColor; //assign the selected color
	return *this; //and return the instance
}

//getBGColor method - returns the background color
const CPPSDL::Color& CPPSDL::MBColorScheme::getBGColor() const {
	return this->bgColor; //return the background color field
}

//getTextColor method - returns the text color
const CPPSDL::Color& CPPSDL::MBColorScheme::getTextColor() const {
	return this->textColor; //return the text color field
}

//getBorderColor method - returns the border color
const CPPSDL::Color& CPPSDL::MBColorScheme::getBorderColor() const {
	return this->borderColor; //return the border color field
}

//getButtonBGColor method - returns the button background color
const CPPSDL::Color& CPPSDL::MBColorScheme::getButtonBGColor() const {
	return this->buttonBGColor; //return the button color field
}

//returns the selected color
const CPPSDL::Color& CPPSDL::MBColorScheme::getSelectedColor() const {
	return this->selectedColor; //return the selected color field
}

//end of implementation
