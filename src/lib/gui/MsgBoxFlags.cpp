/*
 * MsgBoxFlags.cpp
 * Defines message box flags
 * Created by Andrew Davis
 * Created on 5/22/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "MsgBoxFlags.h"

//constant definitions
const unsigned int CPPSDL::MsgBoxFlags::Error = SDL_MESSAGEBOX_ERROR;
const unsigned int CPPSDL::MsgBoxFlags::Warning =
				SDL_MESSAGEBOX_WARNING;
const unsigned int CPPSDL::MsgBoxFlags::Info =
				SDL_MESSAGEBOX_INFORMATION;

//end of definitions
