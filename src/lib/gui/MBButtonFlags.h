/*
 * MBButtonFlags.h
 * Declares message box button flags
 * Created by Andrew Davis
 * Created on 5/22/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include <SDL2/SDL.h>

//outer namespace declaration
namespace CPPSDL {
	//inner namespace declaration
	namespace MBButtonFlags {
		//constant declarations
		extern const unsigned int None; //no flags
		extern const unsigned int Return; //return button
		extern const unsigned int Escape; //escape button
	}
}

//end of header
