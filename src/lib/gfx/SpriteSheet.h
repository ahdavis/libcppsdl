/*
 * SpriteSheet.h
 * Declares a class that represents a spritesheet
 * Created by Andrew Davis
 * Created on 3/19/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <string>
#include "Texture.h"
#include "../util/Rect.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class SpriteSheet {
		//public fields and methods
		public:
			//constructor
			SpriteSheet(const Texture& newTex,
					int newWidth, int newHeight);

			//destructor
			virtual ~SpriteSheet();

			//copy constructor
			SpriteSheet(const SpriteSheet& ss);

			//assignment operator
			SpriteSheet& operator=(const SpriteSheet& src);

			//getter methods
			
			//returns the path to the SpriteSheet's image
			const std::string& getPath() const;

			//returns the texture that represents the
			//SpriteSheet
			const Texture& getTexture() const;

			//returns the width of a sprite in the sheet
			int getWidth() const;

			//returns the height of a sprite in the sheet
			int getHeight() const;

			//returns a rectangle containing the bounds
			//of a sprite in terms of its pixel position
			//the sprite is indexed according to its
			//position on the SpriteSheet, not the pixel
			//position
			Rect getSpriteBounds(int x, int y) const;

		//private fields and methods
		private:
			//fields
			std::string path; //the path to the sheet's image
			Texture tex; //the hardware texture for the image
			int width; //the width of each sprite (in pixels)
			int height; //the height of each sprite (in pixels)

	};
}

//end of header
