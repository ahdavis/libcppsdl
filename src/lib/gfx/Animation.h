/*
 * Animation.h
 * Declares a class that represents a hardware animation
 * Created by Andrew Davis
 * Created on 3/28/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include "SpriteSheet.h"
#include "../util/Rect.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Animation {
		//public fields and methods
		public:
			//first constructor - does not specify an interval
			Animation(const SpriteSheet& newSheet,
					int newFrameCount);

			//second constructor - specifies an interval
			Animation(const SpriteSheet& newSheet,
					int newFrameCount,
					int newInterval);

			//destructor
			virtual ~Animation();

			//copy constructor
			Animation(const Animation& a);

			//assignment operator
			Animation& operator=(const Animation& src);

			//getter methods
			
			//returns the spritesheet for the Animation
			const SpriteSheet& getSheet() const;

			//returns the bounds of the 
			//current texture being shown
			Rect getCurrentTextureBounds() const;

			//returns the current frame being shown
			int getFrame() const;

			//other method
			
			//advances the frame count
			void nextFrame() const;

		//private fields and methods
		private:
			//fields
			
			//the spritesheet that holds the Animation
			SpriteSheet sheet; 
			
			//the number of frames in the Animation
			int frameCount;

			//the number of times to repeat each frame
			int interval;

			//the index of the current frame
			//Declared as mutable to allow calling
			//the nextFrame method from constant Animation
			//objects
			mutable int frame;

	};
}

//end of header
