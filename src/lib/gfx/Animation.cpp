/*
 * Animation.cpp
 * Implements a class that represents a hardware animation
 * Created by Andrew Davis
 * Created on 3/28/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Animation.h"

//first constructor
CPPSDL::Animation::Animation(const CPPSDL::SpriteSheet& newSheet,
				int newFrameCount)
	: Animation(newSheet, newFrameCount, 1) //call the other ctor
{
	//no code needed
}

//second constructor
CPPSDL::Animation::Animation(const CPPSDL::SpriteSheet& newSheet,
				int newFrameCount,
				int newInterval)
	: sheet(newSheet), frameCount(newFrameCount), 
		interval(newInterval), frame(0) //init the fields
{
	//no code needed
}

//destructor
CPPSDL::Animation::~Animation() {
	//no code needed
}

//copy constructor
CPPSDL::Animation::Animation(const CPPSDL::Animation& a)
	: sheet(a.sheet), frameCount(a.frameCount), interval(a.interval),
		frame(a.frame) //copy the fields
{
	//no code needed
}

//assignment operator
CPPSDL::Animation& CPPSDL::Animation::operator=(const CPPSDL::Animation&
							src) {
	this->sheet = src.sheet; //assign the spritesheet field
	this->frameCount = src.frameCount; //assign the frame count
	this->interval = src.interval; //assign the interval
	this->frame = src.frame; //assign the frame index
	return *this; //and return the instance
}

//getSheet method - returns the spritesheet for the Animation
const CPPSDL::SpriteSheet& CPPSDL::Animation::getSheet() const {
	return this->sheet; //return the sheet field
}

//getCurrentTextureBounds method - returns the bounds of the
//current texture of the Animation
CPPSDL::Rect CPPSDL::Animation::getCurrentTextureBounds() const {
	//get the number of the frame
	int frameNum = this->frame / this->interval;

	//and return the bounds of the current texture
	return this->sheet.getSpriteBounds(frameNum, 0);
}

//getFrame method - returns the current frame index
int CPPSDL::Animation::getFrame() const {
	return this->frame; //return the frame field
}

//nextFrame method - moves the Animation to the next frame
void CPPSDL::Animation::nextFrame() const {
	//increment the frame index
	this->frame++;

	//make sure that the frame index is not too high
	if((this->frame / this->interval) >= this->frameCount) {
		this->frame = 0;
	}
}

//end of implementation
