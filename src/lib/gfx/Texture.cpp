/*
 * Texture.cpp
 * Implements a class that represents a hardware texture
 * Created by Andrew Davis
 * Created on 3/15/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Texture.h"

//include the renderer class
#include "Renderer.h"

//first constructor
CPPSDL::Texture::Texture(const CPPSDL::Renderer& r,
				const CPPSDL::ImgElement& ie)
	: Texture(r, ie, 0xFF) //call the other constructor
{
	//no code needed
}

//second constructor
CPPSDL::Texture::Texture(const CPPSDL::Renderer& r,
				const CPPSDL::ImgElement& ie,
					unsigned char newAlphaMod)
	: data(NULL), alphaMod(newAlphaMod), blendEnabled(false),
		width(ie.getWidth()), height(ie.getHeight()),
		path(ie.getPath())
{
	//init the Texture
	this->data = std::shared_ptr<SDL_Texture>(
			SDL_CreateTextureFromSurface(r.data.get(), 
					ie.getSurface().data.get()),
			[=](SDL_Texture* texture) {
			SDL_DestroyTexture(texture);
			texture = NULL; });

	//make sure that the Texture initialized successfully
	if(this->data == NULL) { //if the Texture failed to load
		//then throw an exception
		throw CPPSDL::TextureException(ie.getPath());
	}
}

//third constructor
CPPSDL::Texture::Texture(const CPPSDL::Renderer& r, 
				const CPPSDL::TextElement& te)
	: data(NULL), alphaMod(0xFF), blendEnabled(false),
		width(te.getWidth()), height(te.getHeight()),
		path(te.getText())
{

	//init the Texture
	this->data = std::shared_ptr<SDL_Texture>(
			SDL_CreateTextureFromSurface(r.data.get(),
					te.getSurface().data.get()),
			[=](SDL_Texture* texture) {
				SDL_DestroyTexture(texture);
				texture = NULL; });

	//and make sure that the Texture loaded successfully
	if(this->data == NULL) { //if the Texture failed to load
		//then throw an exception
		throw CPPSDL::TextureException(te.getText());
	}

}

//destructor
CPPSDL::Texture::~Texture() {
	//no code needed
}

//copy constructor
CPPSDL::Texture::Texture(const CPPSDL::Texture& t)
	: data(t.data), alphaMod(t.alphaMod), 
		blendEnabled(t.blendEnabled), width(t.width),
		height(t.height), path(t.path)
{
	//enable blending if the source Texture has blending enabled
	if(t.blendEnabled) {
		this->enableAlphaBlending();
	}
}

//assignment operator
CPPSDL::Texture& CPPSDL::Texture::operator=(const CPPSDL::Texture& src) {
	this->data = src.data; //assign the data field
	this->alphaMod = src.alphaMod; //assign the alpha modulation field
	this->blendEnabled = src.blendEnabled; //assign the blending flag
	this->width = src.width; //assign the width field
	this->height = src.height; //assign the height field
	this->path = src.path; //assign the path field

	//enable blending if the source Texture has blending enabled
	if(src.blendEnabled) {
		this->enableAlphaBlending();
	}

	return *this; //and return the instance
}

//getWidth method - returns the width of the Texture
int CPPSDL::Texture::getWidth() const {
	return this->width; //return the Texture's width
}

//getHeight method - returns the height of the Texture
int CPPSDL::Texture::getHeight() const {
	return this->height; //return the Texture's height
}

//getPath method - returns the path to the image stored in the Texture
const std::string& CPPSDL::Texture::getPath() const {
	return this->path; //return the path to the Texture
}

//getAlpha method - returns the alpha modulation value of the Texture
unsigned char CPPSDL::Texture::getAlphaMod() const {
	return this->alphaMod; //return the alpha modulation value field
}

//canBlend method - returns whether blending is enabled for the Texture
bool CPPSDL::Texture::canBlend() const {
	return this->blendEnabled; //return the blending enabled field
}

//setColorMod method - sets the color modulation of the Texture
void CPPSDL::Texture::setColorMod(const CPPSDL::Color& col) {
	//modulate the color of the Texture
	SDL_SetTextureColorMod(this->data.get(), col.getRed(), 
				col.getGreen(), col.getBlue());
}

//enableAlphaBlending method - enables alpha blending for the Texture
void CPPSDL::Texture::enableAlphaBlending() {
	//enable alpha blending
	SDL_SetTextureBlendMode(this->data.get(), SDL_BLENDMODE_BLEND);

	//and set the blending enabled flag
	this->blendEnabled = true;
}

//setAlphaMod method - sets the alpha modulation value of the Texture
void CPPSDL::Texture::setAlphaMod(unsigned char newAlphaMod) {
	//make sure that blending is enabled
	if(!blendEnabled) { //if blending is not enabled
		//then throw an exception
		throw CPPSDL::BlendException(this->getPath());
	}
	
	this->alphaMod = newAlphaMod; //change the alpha modulation field

	//and set the Texture's alpha mod
	SDL_SetTextureAlphaMod(this->data.get(), this->alphaMod);
}

//end of implementation
