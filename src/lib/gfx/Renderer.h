/*
 * Renderer.h
 * Declares a class that represents a hardware renderer
 * Created by Andrew Davis
 * Created on 3/15/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <cstdlib>
#include <memory>
#include <cmath>
#include <SDL2/SDL.h>
#include "Texture.h"
#include "../win/Window.h"
#include "../except/InitException.h"
#include "../util/Rect.h"
#include "../util/Circle.h"
#include "../util/Point.h"
#include "../color/Color.h"
#include "../util/fliptype.h"
#include "SpriteSheet.h"
#include "Animation.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Renderer {
		//public fields and methods
		public:
			//constructor
			explicit Renderer(const Window& win);

			//destructor
			virtual ~Renderer();

			//copy constructor
			Renderer(const Renderer& r);

			//assignment operator
			Renderer& operator=(const Renderer& src);

			//clears the screen
			void clear();

			//sets the draw color for the Renderer
			void setDrawColor(const Color& col);

			//updates the screen
			void update();

			//renders a texture with no scaling or positioning
			void renderTexture(const Texture& tex);

			//renders a texture with positioning
			void renderTexture(const Texture& tex,
						int x, int y);

			//renders a texture with scaling and positioning
			void renderTexture(const Texture& tex,
						const Rect& src,
						int x, int y,
						int w, int h);

			//renders a texture with positioning and clipping
			void renderTexture(const Texture& tex, 
						int x, int y,
						int w, int h);

			//renders a texture with scaling
			void renderTexture(const Texture& tex,
						const Rect& src);

			//renders a texture stored in a spritesheet
			void renderTexture(const SpriteSheet& sheet,
						int sprX, int sprY,
						int posX, int posY);

			//renders a texture stored in a spritesheet
			//with flipping and rotating
			void renderTexture(const SpriteSheet& sheet,
						int sprX, int sprY,
						const Point& pos,
						double angle,
						const Point& center,
						fliptype_t flip);

			//renders a texture with flipping and rotating
			void renderTexture(const Texture& tex,
						const Rect& src,
						const Point& pos,
						int w, int h, double angle,
						const Point& center, 
						fliptype_t flip);

			//renders an animation without modifications
			void renderAnimation(const Animation& anim,
						const Point& pos);

			//renders an animation with flipping and
			//rotating
			void renderAnimation(const Animation& anim,
						const Point& pos,
						double angle,
						const Point& center,
						fliptype_t flip);

			//renders a rectangle with the option to
			//have it filled
			void renderRect(const Rect& r, bool shouldFill);

			//renders a circle with the option to
			//have it filled
			void renderCircle(const Circle& c, 
						bool shouldFill);

			//renders a line
			void renderLine(int x1, int y1, int x2, int y2);

			//renders a point
			void renderPoint(const Point& p);

			//sets the rendering viewport
			void setViewport(const Rect& r);

		//private fields and methods
		private:
			//friendship declaration
			friend class Texture;

			//field
			std::shared_ptr<SDL_Renderer> data; //the renderer
	};
}

//end of header
