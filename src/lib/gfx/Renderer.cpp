/*
 * Renderer.cpp
 * Implements a class that represents a hardware renderer
 * Created by Andrew Davis
 * Created on 3/15/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Renderer.h"

//constructor
CPPSDL::Renderer::Renderer(const CPPSDL::Window& win)
	: data(NULL) //init the field
{
	//init the data field
	this->data = std::shared_ptr<SDL_Renderer>(SDL_CreateRenderer(
				win.data.get(), -1, 
				SDL_RENDERER_ACCELERATED | 
				SDL_RENDERER_PRESENTVSYNC),
				[=](SDL_Renderer* renderer) {
				SDL_DestroyRenderer(renderer);
				renderer = NULL; });

	//make sure the initialization succeeded
	if(this->data == NULL) { //if the initialization failed
		//then throw an exception
		throw CPPSDL::InitException("Renderer", SDL_GetError());
	}
}

//destructor
CPPSDL::Renderer::~Renderer() {
	//no code needed
}

//copy constructor
CPPSDL::Renderer::Renderer(const CPPSDL::Renderer& r)
	: data(r.data)  //copy the field
{
	//no code needed
}

//assignment operator
CPPSDL::Renderer& CPPSDL::Renderer::operator=(const CPPSDL::Renderer& src) {
	this->data = src.data; //assign the data field
	return *this; //and return the instance
}

//clear method - clears the screen
void CPPSDL::Renderer::clear() {
	SDL_RenderClear(this->data.get()); //clear the Renderer
}

//setDrawColor method - sets the draw color for the Renderer
void CPPSDL::Renderer::setDrawColor(const CPPSDL::Color& col) {
	//set the render draw color
	SDL_SetRenderDrawColor(this->data.get(), col.getRed(),
						col.getGreen(),
						col.getBlue(),
						col.getAlpha());
}

//update method - updates the Renderer
void CPPSDL::Renderer::update() {
	SDL_RenderPresent(this->data.get()); //update the screen
}

//first renderTexture method - renders a texture with no scaling
//or positioning
void CPPSDL::Renderer::renderTexture(const CPPSDL::Texture& tex) {
	//render the texture
	SDL_RenderCopy(this->data.get(), tex.data.get(), NULL, NULL);
}

//second renderTexture method - renders a texture with positioning
void CPPSDL::Renderer::renderTexture(const CPPSDL::Texture& tex,
					int x, int y) {
	//get a rect that describes the bounds of the texture
	CPPSDL::Rect pos(x, y, tex.getWidth(), tex.getHeight());

	//and render the texture
	SDL_RenderCopy(this->data.get(), tex.data.get(), NULL,
				&pos.data);
}

//third renderTexture method - renders a texture with scaling and
//positioning
void CPPSDL::Renderer::renderTexture(const CPPSDL::Texture& tex,
					const CPPSDL::Rect& src,
					int x, int y,
					int w, int h) {
	//get a rect from the arguments
	CPPSDL::Rect pos(x, y, w, h);

	//render the texture
	SDL_RenderCopy(this->data.get(), tex.data.get(), &src.data,
			&pos.data);
}

//fourth renderTexture method - renders a texture with positioning
//and clipping
void CPPSDL::Renderer::renderTexture(const CPPSDL::Texture& tex,
					int x, int y,
					int w, int h) {
	//get a rect from the parameters
	CPPSDL::Rect pos(x, y, w, h);

	//render the texture
	SDL_RenderCopy(this->data.get(), tex.data.get(), NULL, 
			&pos.data);
}

//fifth renderTexture method - renders a texture with scaling
void CPPSDL::Renderer::renderTexture(const CPPSDL::Texture& tex,
					const CPPSDL::Rect& src) {
	//render the texture
	SDL_RenderCopy(this->data.get(), tex.data.get(), &src.data, NULL);
}

//sixth renderTexture method - renders a texture stored in a spritesheet
void CPPSDL::Renderer::renderTexture(const CPPSDL::SpriteSheet& sheet,
					int sprX, int sprY,
					int posX, int posY) {
	//get a rect that contains the bounds of the indicated sprite
	CPPSDL::Rect bounds = sheet.getSpriteBounds(sprX, sprY);

	//and render the indicated sprite using another renderTexture
	//method
	this->renderTexture(sheet.getTexture(), bounds, posX, posY,
				bounds.getWidth(), bounds.getHeight());
}

//seventh renderTexture method - renders a texture stored in a spritesheet
//with flipping and rotating
void CPPSDL::Renderer::renderTexture(const CPPSDL::SpriteSheet& sheet,
					int sprX, int sprY,
					const CPPSDL::Point& pos,
					double angle,
					const CPPSDL::Point& center,
					CPPSDL::fliptype_t flip) {
	//get a rect that contains the bounds of the indicated sprite
	CPPSDL::Rect bounds = sheet.getSpriteBounds(sprX, sprY);

	//and render the sprite
	this->renderTexture(sheet.getTexture(), bounds, pos, 
				bounds.getWidth(), bounds.getHeight(), 
				angle, center, flip);
}

//eighth renderTexture method - renders a texture with flipping and
//rotating
void CPPSDL::Renderer::renderTexture(const CPPSDL::Texture& tex,
					const CPPSDL::Rect& src,
					const CPPSDL::Point& pos,
					int w, int h, double angle,
					const CPPSDL::Point& center,
					CPPSDL::fliptype_t flip) {
	//get the rendering space
	CPPSDL::Rect renderRect(pos.getX(), pos.getY(), w, h);

	//and render the texture
	SDL_RenderCopyEx(this->data.get(), tex.data.get(), &src.data,
				&renderRect.data, angle, &center.data,
				static_cast<SDL_RendererFlip>(
					static_cast<unsigned int>(flip)));
}

//first renderAnimation method - renders an animation with no modifications
void CPPSDL::Renderer::renderAnimation(const CPPSDL::Animation& anim,
					const CPPSDL::Point& pos) {
	//get a rect that specifies the frame bounds
	CPPSDL::Rect bounds = anim.getCurrentTextureBounds();

	//render the current texture of the animation
	this->renderTexture(anim.getSheet().getTexture(), 
				bounds, pos.getX(), pos.getY(),
				bounds.getWidth(), bounds.getHeight());

	//and move to the next frame of the animation
	anim.nextFrame();
}

//second renderAnimation method - renders an animation with flipping
//and rotating
void CPPSDL::Renderer::renderAnimation(const CPPSDL::Animation& anim,
					const CPPSDL::Point& pos,
					double angle, 
					const CPPSDL::Point& center,
					CPPSDL::fliptype_t flip) {
	//get the bounds of the current animation frame
	CPPSDL::Rect bounds = anim.getCurrentTextureBounds();

	//render the animation
	this->renderTexture(anim.getSheet().getTexture(), bounds, pos,
				anim.getSheet().getWidth(),
				anim.getSheet().getHeight(),
				angle, center, flip);

	//and move to the next frame of the animation
	anim.nextFrame();
}


//renderRect method - renders a rectangle with the option to fill it
void CPPSDL::Renderer::renderRect(const CPPSDL::Rect& r, bool shouldFill) {
	if(shouldFill) { //if the rectangle should be filled
		//then draw a filled rectangle
		SDL_RenderFillRect(this->data.get(), &r.data);
	} else { //if the rectangle should not be filled
		//then draw an empty rectangle
		SDL_RenderDrawRect(this->data.get(), &r.data);
	}
}

//renderCircle method - renders a circle with the option to fill it
void CPPSDL::Renderer::renderCircle(const CPPSDL::Circle& c, 
					bool shouldFill) {
	if(shouldFill) { //if the circle should be filled
		//then draw a filled circle
		for(int t = 0; t < 360; t++) { //loop over 360 degrees
			//get the x-coord of a point on the perimeter
			int pX = c.getX() + 
				(c.getRadius() * std::cos(t * 
							(M_PI / 180.0)));

			//get the y-coord of that same point
			int pY = c.getY() +
				(c.getRadius() * std::sin(t *
							(M_PI / 180.0)));

			//and draw a line between the origin of the circle
			//and the outer point
			this->renderLine(c.getX(), c.getY(), pX, pY);
		}
	} else { //if the circle should not be filled
		//then draw an empty circle
		for(int t = 0; t < 360; t++) { //loop over 360 degrees
			//get the x-coord of a point on the perimeter
			int pX = c.getX() +
				(c.getRadius() * std::cos(t * 
							(M_PI / 180.0)));

			//get the y-coord of that same point
			int pY = c.getY() +
				(c.getRadius() * std::sin(t *
							(M_PI / 180.0)));

			//create a Point object from the coordinates
			CPPSDL::Point p(pX, pY);

			//and render the point
			this->renderPoint(p);
		}
	}
}

//renderLine method - renders a line
void CPPSDL::Renderer::renderLine(int x1, int y1, int x2, int y2) {
	//render the line
	SDL_RenderDrawLine(this->data.get(), x1, y1, x2, y2);
}

//renderPoint method - renders a point
void CPPSDL::Renderer::renderPoint(const CPPSDL::Point& p) {
	//render the point
	SDL_RenderDrawPoint(this->data.get(), p.getX(), p.getY());
}

//setViewport method - sets the Renderer viewport
void CPPSDL::Renderer::setViewport(const CPPSDL::Rect& r) {
	//set the Renderer viewport
	SDL_RenderSetViewport(this->data.get(), &r.data);
}

//end of implementation
