/*
 * Texture.h
 * Declares a class that represents a hardware texture
 * Created by Andrew Davis
 * Created on 3/15/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once 

//includes
#include <SDL2/SDL.h>
#include <cstdlib>
#include <memory>
#include <string>
#include "../element/ImgElement.h"
#include "../element/TextElement.h"
#include "../win/Surface.h"
#include "../except/TextureException.h"
#include "../except/BlendException.h"
#include "../color/Color.h"

//namespace declaration
namespace CPPSDL {
	//forward declaration of the renderer class
	class Renderer;

	//class declaration
	class Texture {
		//public fields and methods
		public:
			//first constructor - does not specify an initial
			//alpha value
			Texture(const Renderer& r, 
					const ImgElement& ie);

			//second constructor - specifies an initial alpha
			//value
			Texture(const Renderer& r,
					const ImgElement& ie,
						unsigned char newAlphaMod);

			//third constructor - renders a text element
			Texture(const Renderer& r,
					const TextElement& te);

			//destructor
			virtual ~Texture();

			//copy constructor
			Texture(const Texture& t);

			//assignment operator
			Texture& operator=(const Texture& src);

			//getter methods
			
			//returns the width of the Texture
			int getWidth() const;

			//returns the height of the Texture
			int getHeight() const;

			//returns the path to the Texture's image
			const std::string& getPath() const;

			//returns the alpha modulation value of the Texture
			unsigned char getAlphaMod() const;

			//returns whether the Texture has alpha blending
			//enabled
			bool canBlend() const;

			//other methods
			
			//sets the color modulation of the Texture
			void setColorMod(const Color& col);

			//enables alpha blending for the Texture
			void enableAlphaBlending();

			//sets the alpha modulation for the Texture
			void setAlphaMod(unsigned char newAlphaMod);

		//private fields and methods
		private:
			//friendship declaration
			friend class Renderer;


			//fields
			std::shared_ptr<SDL_Texture> data; //the Texture
			unsigned char alphaMod; //the alpha modulation value
			bool blendEnabled; //is alpha blending enabled?
			int width; //the width of the Texture
			int height; //the height of the Texture
			std::string path; //the path to the Texture
	};
}

//end of header
