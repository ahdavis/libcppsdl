/*
 * SpriteSheet.cpp
 * Implements a class that represents a spritesheet
 * Created by Andrew Davis
 * Created on 3/19/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "SpriteSheet.h"

//constructor
CPPSDL::SpriteSheet::SpriteSheet(const CPPSDL::Texture& newTex, 
					int newWidth, int newHeight)
	: path(newTex.getPath()), tex(newTex),
		width(newWidth), height(newHeight) //init the fields
{
	//no code needed
}

//destructor
CPPSDL::SpriteSheet::~SpriteSheet() {
	//no code needed
}

//copy constructor
CPPSDL::SpriteSheet::SpriteSheet(const CPPSDL::SpriteSheet& ss)
	: path(ss.path), tex(ss.tex), width(ss.width), height(ss.height)
{
	//no code needed
}

//assignment operatpr
CPPSDL::SpriteSheet& CPPSDL::SpriteSheet::operator=(const 
						CPPSDL::SpriteSheet& src) {
	this->path = src.path; //assign the path field
	this->tex = src.tex; //assign the texture field
	this->width = src.width; //assign the width field
	this->height = src.height; //assign the height field
	return *this; //and return the instance
}

//getPath method - returns the path to the SpriteSheet's image
const std::string& CPPSDL::SpriteSheet::getPath() const {
	return this->path; //return the path field
}

//getTexture method - returns the hardware texture that defines the
//SpriteSheet
const CPPSDL::Texture& CPPSDL::SpriteSheet::getTexture() const {
	return this->tex; //return the texture field
}

//getWidth method - returns the width of a sprite in the sheet
int CPPSDL::SpriteSheet::getWidth() const {
	return this->width; //return the width field
}

//getHeight method - returns the height of a sprite in the sheet
int CPPSDL::SpriteSheet::getHeight() const {
	return this->height; //return the height field
}

//getSpriteBounds method - returns the bounds (in pixels) of a sprite
//referenced by its x and y coordinates on the sheet. The coordinates
//are not in pixels. They are measured in sprites.
CPPSDL::Rect CPPSDL::SpriteSheet::getSpriteBounds(int x, int y) const {
	//get the x-coord (in pixels) of the upper left corner 
	//of the sprite
	int px = this->width * x;

	//get the y-coord (in pixels) of the upper left corner
	//of the sprite
	int py = this->height * y;

	//create and return a rect containing the x-coord, y-coord,
	//width, and height of the indicated sprite
	return CPPSDL::Rect(px, py, this->width, this->height);
}

//end of implementation
