/*
 * JoystickException.cpp
 * Implements an exception that is thrown when a joystick fails to connect
 * Created by Andrew Davis
 * Created on 5/31/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "JoystickException.h"

//constructor
CPPSDL::JoystickException::JoystickException(int badID)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "Unable to connect to joystick ";
	ss << badID;
	ss << "! Message: ";
	ss << SDL_GetError();
	this->errMsg = ss.str();
}

//destructor
CPPSDL::JoystickException::~JoystickException() {
	//no code needed
}

//copy constructor
CPPSDL::JoystickException::JoystickException(const 
					CPPSDL::JoystickException& jse)
	: errMsg(jse.errMsg) //copy the field
{
	//no code needed
}

//assignment operator
CPPSDL::JoystickException& CPPSDL::JoystickException::operator=(const
					CPPSDL::JoystickException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::JoystickException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
