/*
 * AssetException.cpp
 * Implements an exception that is thrown when a requested asset
 * is not found
 * Created by Andrew Davis
 * Created on 6/22/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "AssetException.h"

//include
#include <sstream>

//constructor
CPPSDL::AssetException::AssetException(const std::string& badName)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "Unable to locate asset with name ";
	ss << badName;
	ss << "!";
	this->errMsg = ss.str();
}

//destructor
CPPSDL::AssetException::~AssetException() {
	//no code needed
}

//copy constructor
CPPSDL::AssetException::AssetException(const CPPSDL::AssetException& ae)
	: errMsg(ae.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
CPPSDL::AssetException& CPPSDL::AssetException::operator=(const
						CPPSDL::AssetException&
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::AssetException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
