/*
 * InitException.h
 * Declares an exception that is thrown when SDL itself fails to init
 * Created by Andrew Davis
 * Created on 3/7/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>
#include <sstream>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class InitException : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			InitException(const std::string& libName, 
					const std::string& sdlError);

			//destructor
			virtual ~InitException();

			//copy constructor
			InitException(const InitException& ie);

			//assignment operator
			InitException& operator=(const InitException&
							src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
