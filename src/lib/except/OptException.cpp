/*
 * OptException.cpp
 * Implements an exception that is thrown when optimization 
 * of an image fails
 * Created by Andrew Davis
 * Created on 3/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "OptException.h"

//constructor
CPPSDL::OptException::OptException(const std::string& path)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "Unable to optimize image ";
	ss << path;
	ss << "! Message: ";
	ss << SDL_GetError();
	this->errMsg = ss.str();
}

//destructor
CPPSDL::OptException::~OptException() {
	//no code needed
}

//copy constructor
CPPSDL::OptException::OptException(const CPPSDL::OptException& oe)
	: errMsg(oe.errMsg) //copy the field
{
	//no code needed
}

//assignment operator
CPPSDL::OptException& CPPSDL::OptException::operator=(const
						CPPSDL::OptException&
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::OptException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
