/*
 * ExtException.cpp
 * Implements an exception that is thrown when a file has a bad extension
 * Created by Andrew Davis
 * Created on 3/15/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "ExtException.h"

//constructor
CPPSDL::ExtException::ExtException(const std::string& path)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "Bad extension for file with path ";
	ss << path;
	this->errMsg = ss.str();
}

//destructor
CPPSDL::ExtException::~ExtException() {
	//no code needed
}

//copy constructor
CPPSDL::ExtException::ExtException(const CPPSDL::ExtException& ee)
	: errMsg(ee.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
CPPSDL::ExtException& CPPSDL::ExtException::operator=(const
						CPPSDL::ExtException&
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::ExtException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
