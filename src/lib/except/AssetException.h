/*
 * AssetException.h
 * Declares an exception that is thrown when a requested asset
 * is not found
 * Created by Andrew Davis
 * Created on 6/22/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class AssetException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit AssetException(const std::string& 
						badName);

			//destructor
			~AssetException();

			//copy constructor
			AssetException(const AssetException& ae);

			//assignment operator
			AssetException& operator=(const AssetException& 
							src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
