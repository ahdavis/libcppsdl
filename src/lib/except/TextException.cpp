/*
 * TextException.cpp
 * Implements an exception that is thrown when TTF text fails to load
 * Created by Andrew Davis
 * Created on 4/1/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "TextException.h"

//constructor
CPPSDL::TextException::TextException(const std::string& text)
	: errMsg() //init the field
{
	//assemble the error message
	std::stringstream ss;
	ss << "Unable to render text! Text: ";
	ss << text;
	ss << " Message: ";
	ss << TTF_GetError();
	this->errMsg = ss.str();
}

//destructor
CPPSDL::TextException::~TextException() {
	//no code needed
}

//copy constructor
CPPSDL::TextException::TextException(const CPPSDL::TextException& te)
	: errMsg(te.errMsg) //copy the field
{
	//no code needed
}

//assignment operator
CPPSDL::TextException& CPPSDL::TextException::operator=(const 
						CPPSDL::TextException&
								src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::TextException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
