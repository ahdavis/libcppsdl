/*
 * BlendException.h
 * Declares an exception that is thrown when blending is attempted on
 * a texture that blending has not been enabled for
 * Created by Andrew Davis
 * Created on 3/23/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>
#include <sstream>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class BlendException : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit BlendException(const std::string& path);

			//destructor
			virtual ~BlendException();

			//copy constructor
			BlendException(const BlendException& be);

			//assignment operator
			BlendException& operator=(const BlendException&
							src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
