/*
 * ImgException.h
 * Declares an exception that is thrown when an image fails to load
 * Created by Andrew Davis
 * Created on 3/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>
#include <sstream>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class ImgException : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			ImgException(const std::string& path,
					const std::string& sdlError);

			//destructor
			virtual ~ImgException();

			//copy constructor
			ImgException(const ImgException& ie);

			//assignment operator
			ImgException& operator=(const ImgException& src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
