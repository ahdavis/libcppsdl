/*
 * SoundException.h
 * Declares an exception that is thrown when sound fails to load
 * Created by Andrew Davis
 * Created on 5/21/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL_mixer.h>
#include <exception>
#include <string>
#include <sstream>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class SoundException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit SoundException(const std::string&
							badPath);

			//destructor
			~SoundException();

			//copy constructor
			SoundException(const SoundException& se);

			//assignment operator
			SoundException& operator=(const SoundException& 
							src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
