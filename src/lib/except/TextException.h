/*
 * TextException.h
 * Declares an exception that is thrown when TTF text fails to load
 * Created by Andrew Davis
 * Created on 4/1/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>
#include <sstream>
#include <SDL2/SDL_ttf.h>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class TextException : public std::exception
	{	
		//public fields and methods
		public:
			//constructor
			explicit TextException(const std::string& text);

			//destructor
			virtual ~TextException();

			//copy constructor
			TextException(const TextException& te);

			//assignment operator
			TextException& operator=(const TextException& 
							src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
