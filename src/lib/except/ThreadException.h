/*
 * ThreadException.h
 * Declares an exception that is thrown when a thread fails to execute
 * Created by Andrew Davis
 * Created on 6/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <exception>
#include <string>
#include <sstream>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class ThreadException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit ThreadException(const std::string& 
							threadName);

			//destructor
			~ThreadException();

			//copy constructor
			ThreadException(const ThreadException& te);

			//assignment operator
			ThreadException& operator=(const ThreadException&
							src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
