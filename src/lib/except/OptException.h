/*
 * OptException.h
 * Declares an exception that is thrown when optimization 
 * of an image fails
 * Created by Andrew Davis
 * Created on 3/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <exception>
#include <string>
#include <sstream>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class OptException : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			OptException(const std::string& path);

			//destructor
			virtual ~OptException();

			//copy constructor
			OptException(const OptException& oe);

			//assignment operator
			OptException& operator=(const OptException& src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
