/*
 * MutexException.cpp
 * Implements an exception that is thrown when a mutex fails to initialize
 * Created by Andrew Davis
 * Created on 6/13/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "MutexException.h"

//constructor
CPPSDL::MutexException::MutexException()
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "Mutex failed to initialize! Message: ";
	ss << SDL_GetError();
	this->errMsg = ss.str();
}

//destructor
CPPSDL::MutexException::~MutexException() {
	//no code needed
}

//copy constructor
CPPSDL::MutexException::MutexException(const CPPSDL::MutexException& me)
	: errMsg(me.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
CPPSDL::MutexException& CPPSDL::MutexException::operator=(const
						CPPSDL::MutexException&
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::MutexException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
