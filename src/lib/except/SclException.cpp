/*
 * SclException.cpp
 * Implements an exception that is thrown when 
 * scaling of an image fails
 * Created by Andrew Davis
 * Created on 3/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "SclException.h"

//constructor
CPPSDL::SclException::SclException()
	: errMsg() //init the field
{
	//assemble the error message
	std::stringstream ss;
	ss << "Unable to scale image! Message: ";
	ss << SDL_GetError();
	this->errMsg = ss.str();
}

//destructor
CPPSDL::SclException::~SclException() {
	//no code needed
}

//copy constructor
CPPSDL::SclException::SclException(const CPPSDL::SclException& se)
	: errMsg(se.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
CPPSDL::SclException& CPPSDL::SclException::operator=(const
						CPPSDL::SclException& 
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::SclException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
