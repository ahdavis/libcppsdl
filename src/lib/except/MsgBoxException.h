/*
 * MsgBoxException.h
 * Declares an exception that is thrown when a message box fails to load
 * Created by Andrew Davis
 * Created on 5/22/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <exception>
#include <string>
#include <sstream>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class MsgBoxException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit MsgBoxException(const std::string&
							badTitle);

			//destructor
			~MsgBoxException();

			//copy constructor
			MsgBoxException(const MsgBoxException& mbe);

			//assignment operator
			MsgBoxException& operator=(const MsgBoxException&
							src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
