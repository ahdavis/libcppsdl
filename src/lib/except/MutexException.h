/*
 * MutexException.h
 * Declares an exception that is thrown when a mutex fails to initialize
 * Created by Andrew Davis
 * Created on 6/13/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <exception>
#include <string>
#include <sstream>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class MutexException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			MutexException();

			//destructor
			~MutexException();

			//copy constructor
			MutexException(const MutexException& me);

			//assignment operator
			MutexException& operator=(const MutexException&
							src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
