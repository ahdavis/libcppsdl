/*
 * SemException.cpp
 * Implements an exception that is thrown when a semaphore fails to init
 * Created by Andrew Davis
 * Created on 6/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "SemException.h"

//constructor
CPPSDL::SemException::SemException(std::uint32_t badLockCount)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "Semaphore with lock count ";
	ss << badLockCount;
	ss << " failed to initialize! Message: ";
	ss << SDL_GetError();
	this->errMsg = ss.str();
}

//destructor
CPPSDL::SemException::~SemException() {
	//no code needed
}

//copy constructor
CPPSDL::SemException::SemException(const CPPSDL::SemException& se)
	: errMsg(se.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
CPPSDL::SemException& CPPSDL::SemException::operator=(const
						CPPSDL::SemException& 
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::SemException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
