/*
 * SoundException.cpp
 * Implements an exception that is thrown when sound fails to load
 * Created by Andrew Davis
 * Created on 5/21/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "SoundException.h"

//constructor
CPPSDL::SoundException::SoundException(const std::string& badPath)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "Failed to load sound with path ";
	ss << badPath;
	ss << "! Message: ";
	ss << Mix_GetError();
	this->errMsg = ss.str();
}

//destructor
CPPSDL::SoundException::~SoundException() {
	//no code needed
}

//copy constructor
CPPSDL::SoundException::SoundException(const CPPSDL::SoundException& se)
	: errMsg(se.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
CPPSDL::SoundException& CPPSDL::SoundException::operator=(const 
					CPPSDL::SoundException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::SoundException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
