/*
 * WindowException.cpp
 * Implements an exception that is thrown when an SDL window fails to init
 * Created by Andrew Davis
 * Created on 3/7/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "WindowException.h"

//constructor
CPPSDL::WindowException::WindowException()
	: errMsg(SDL_GetError()) //init the field
{
	//init the error message
	this->errMsg = "Error initializing the SDL window. Message: " +
				this->errMsg;
}

//destructor
CPPSDL::WindowException::~WindowException() {
	//no code needed
}

//copy constructor
CPPSDL::WindowException::WindowException(const CPPSDL::WindowException&
						we)
	: errMsg(we.errMsg) //copy the field
{
	//no code needed
}

//assignment operator
CPPSDL::WindowException& CPPSDL::WindowException::operator=(
				const CPPSDL::WindowException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::WindowException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
