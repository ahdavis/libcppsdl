/*
 * MsgBoxException.cpp
 * Implements an exception that is thrown when a message box fails to load
 * Created by Andrew Davis
 * Created on 5/22/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "MsgBoxException.h"

//constructor
CPPSDL::MsgBoxException::MsgBoxException(const std::string& badTitle)
	: errMsg() //init the field
{
	//assemble the error message
	std::stringstream ss;
	ss << "Message box with title ";
	ss << badTitle;
	ss << " failed to load! Message: ";
	ss << SDL_GetError();
	this->errMsg = ss.str();
}

//destructor
CPPSDL::MsgBoxException::~MsgBoxException() {
	//no code needed
}

//copy constructor
CPPSDL::MsgBoxException::MsgBoxException(const CPPSDL::MsgBoxException& mbe)
	: errMsg(mbe.errMsg) //copy the field
{
	//no code needed
}

//assignment operator
CPPSDL::MsgBoxException& CPPSDL::MsgBoxException::operator=(
					const CPPSDL::MsgBoxException& 
						src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::MsgBoxException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
