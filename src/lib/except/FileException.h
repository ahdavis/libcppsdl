/*
 * FileException.h
 * Declares an exception that is thrown when a nonexistent file is opened
 * Created by Andrew Davis
 * Created on 6/6/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <exception>
#include <string>
#include <sstream>
#include "../file/FileMode.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class FileException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			FileException(const std::string& badPath,
					FileMode badMode);

			//destructor
			~FileException();

			//copy constructor
			FileException(const FileException& fe);

			//assignment operator
			FileException& operator=(const FileException& src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
