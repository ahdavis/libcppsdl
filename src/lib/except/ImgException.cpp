/*
 * ImgException.cpp
 * Implements an exception that is thrown when an image fails to load
 * Created by Andrew Davis
 * Created on 3/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "ImgException.h"

//constructor
CPPSDL::ImgException::ImgException(const std::string& path,
					const std::string& sdlError)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "Unable to load image ";
	ss << path;
	ss << "! Message: ";
	ss << sdlError; 
	this->errMsg = ss.str();
}

//destructor
CPPSDL::ImgException::~ImgException() {
	//no code needed
}

//copy constructor
CPPSDL::ImgException::ImgException(const CPPSDL::ImgException& ie)
	: errMsg(ie.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
CPPSDL::ImgException& CPPSDL::ImgException::operator=(const 
						CPPSDL::ImgException& 
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::ImgException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
