/*
 * BundleException.h
 * Declares an exception that is thrown when a bundle file is not found
 * Created by Andrew Davis
 * Created on 6/24/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class BundleException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit BundleException(const std::string& 
							badPath);

			//destructor
			~BundleException();

			//copy constructor
			BundleException(const BundleException& be);

			//assignment operator
			BundleException& operator=(const BundleException&
							src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
