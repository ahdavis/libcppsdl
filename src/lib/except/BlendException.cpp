/*
 * BlendException.cpp
 * Implements an exception that is thrown when blending is attempted on
 * a texture that blending has not been enabled for
 * Created by Andrew Davis
 * Created on 3/23/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "BlendException.h"

//constructor
CPPSDL::BlendException::BlendException(const std::string& path) 
	: errMsg() //init the field
{
	//assemble the error message
	std::stringstream ss;
	ss << "Blending not enabled for texture with path ";
	ss << path;
	ss << "!";
	this->errMsg = ss.str();
}

//destructor
CPPSDL::BlendException::~BlendException() {
	//no code needed
}

//copy constructor
CPPSDL::BlendException::BlendException(const CPPSDL::BlendException& be)
	: errMsg(be.errMsg) //copy the field
{
	//no code needed
}

//assignment operator
CPPSDL::BlendException& CPPSDL::BlendException::operator=(const
						CPPSDL::BlendException&
								src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::BlendException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
