/*
 * CondException.h
 * Declares an exception that is thrown when a condition 
 * fails to initialize
 * Created by Andrew Davis
 * Created on 6/13/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <exception>
#include <string>
#include <sstream>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class CondException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			CondException();

			//destructor
			~CondException();

			//copy constructor
			CondException(const CondException& ce);

			//assignment operator
			CondException& operator=(const CondException&
							src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
