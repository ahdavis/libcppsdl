/*
 * WindowException.h
 * Declares an exception that is thrown when an SDL window fails to init
 * Created by Andrew Davis
 * Created on 3/7/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <exception>
#include <string>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class WindowException : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			WindowException();

			//destructor
			virtual ~WindowException();

			//copy constructor
			WindowException(const WindowException& we);

			//assignment operator
			WindowException& operator=(const WindowException&
							src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
