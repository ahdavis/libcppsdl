/*
 * TextureException.cpp
 * Implements an exception that is thrown when a texture fails to load
 * Created by Andrew Davis
 * Created on 3/15/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "TextureException.h"

//constructor
CPPSDL::TextureException::TextureException(const std::string& path)
	: errMsg() //init the field
{
	//assemble the error message
	std::stringstream ss;
	ss << "Failed to load texture from ";
	ss << path;
	ss << "! Message: ";
	ss << SDL_GetError();
	this->errMsg = ss.str();
}

//destructor
CPPSDL::TextureException::~TextureException() {
	//no code needed
}

//copy constructor
CPPSDL::TextureException::TextureException(const 
					CPPSDL::TextureException& te)
	: errMsg(te.errMsg) //copy the field
{
	//no code needed
}

//assignment operator
CPPSDL::TextureException& CPPSDL::TextureException::operator=(
					const CPPSDL::TextureException&
						src) {
	this->errMsg = src.errMsg; //assign the field
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::TextureException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
