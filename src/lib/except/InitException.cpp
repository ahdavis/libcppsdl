/*
 * InitException.cpp
 * Implements an exception that is thrown when SDL itself fails to init
 * Created by Andrew Davis
 * Created on 3/7/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "InitException.h"

//constructor
CPPSDL::InitException::InitException(const std::string& libName,
					const std::string& sdlError)
	: errMsg() //init the field
{
	//assemble the error message
	std::stringstream ss;
	ss << libName;
	ss << " failed to initialize! Message: ";
	ss << sdlError;
	this->errMsg = ss.str();
}

//destructor
CPPSDL::InitException::~InitException() {
	//no code needed
}

//copy constructor
CPPSDL::InitException::InitException(const CPPSDL::InitException&
						ie)
	: errMsg(ie.errMsg) //copy the field
{
	//no code needed
}

//assignment operator
CPPSDL::InitException& CPPSDL::InitException::operator=(
				const CPPSDL::InitException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::InitException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
