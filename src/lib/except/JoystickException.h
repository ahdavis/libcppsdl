/*
 * JoystickException.h
 * Declares an exception that is thrown when a joystick fails to connect
 * Created by Andrew Davis
 * Created on 5/31/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <exception>
#include <string>
#include <sstream>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class JoystickException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit JoystickException(int badID);

			//destructor
			~JoystickException();

			//copy constructor
			JoystickException(const JoystickException& jse);

			//assignment operator
			JoystickException& operator=(const
						JoystickException& src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
