/*
 * CondException.cpp
 * Implements an exception that is thrown when a condition
 * fails to initialize
 * Created by Andrew Davis
 * Created on 6/13/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "CondException.h"

//constructor
CPPSDL::CondException::CondException()
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "Condition failed to initialize! Message: ";
	ss << SDL_GetError();
	this->errMsg = ss.str();
}

//destructor
CPPSDL::CondException::~CondException() {
	//no code needed
}

//copy constructor
CPPSDL::CondException::CondException(const CPPSDL::CondException& ce)
	: errMsg(ce.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
CPPSDL::CondException& CPPSDL::CondException::operator=(const
						CPPSDL::CondException&
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::CondException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
