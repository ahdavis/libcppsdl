/*
 * FileException.cpp
 * Implements an exception that is thrown when a nonexistent file is opened
 * Created by Andrew Davis
 * Created on 6/6/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "FileException.h"

//constructor
CPPSDL::FileException::FileException(const std::string& badPath,
					CPPSDL::FileMode badMode)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "Failed to open file ";
	ss << badPath;
	ss << " with mode ";
	ss << CPPSDL::modeStr(badMode);
	ss << "! Message: ";
	ss << SDL_GetError();
	this->errMsg = ss.str();
}

//destructor
CPPSDL::FileException::~FileException() {
	//no code needed
}

//copy constructor
CPPSDL::FileException::FileException(const CPPSDL::FileException& fe)
	: errMsg(fe.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
CPPSDL::FileException& CPPSDL::FileException::operator=(const
					CPPSDL::FileException& src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::FileException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
