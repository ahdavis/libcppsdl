/*
 * FontException.cpp
 * Implements an exception that is thrown when a font fails to load
 * Created by Andrew Davis
 * Created on 4/1/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "FontException.h"

//constructor
CPPSDL::FontException::FontException(const std::string& path)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "Failed to load font ";
	ss << path;
	ss << "! Message: ";
	ss << TTF_GetError();
	this->errMsg = ss.str();
}

//destructor
CPPSDL::FontException::~FontException() {
	//no code needed
}

//copy constructor
CPPSDL::FontException::FontException(const CPPSDL::FontException& fe)
	: errMsg(fe.errMsg) //copy the error message
{
	//no code needed
}

//assignment operator
CPPSDL::FontException& CPPSDL::FontException::operator=(const
						CPPSDL::FontException&
								src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::FontException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
