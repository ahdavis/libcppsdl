/*
 * ThreadException.cpp
 * Implements an exception that is thrown when a thread fails to execute
 * Created by Andrew Davis
 * Created on 6/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "ThreadException.h"

//constructor
CPPSDL::ThreadException::ThreadException(const std::string& threadName)
	: errMsg() //init the field
{
	//assemble the error message
	std::stringstream ss;
	ss << "Thread ";
	ss << threadName;
	ss << " failed to execute! Message: ";
	ss << SDL_GetError();
	this->errMsg = ss.str();
}

//destructor
CPPSDL::ThreadException::~ThreadException() {
	//no code needed
}

//copy constructor
CPPSDL::ThreadException::ThreadException(const CPPSDL::ThreadException& 
						te)
	: errMsg(te.errMsg) //copy the field
{
	//no code needed
}

//assignment operator
CPPSDL::ThreadException& CPPSDL::ThreadException::operator=(const
						CPPSDL::ThreadException&
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::ThreadException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
