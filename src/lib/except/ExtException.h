/*
 * ExtException.h
 * Declares an exception that is thrown when a file has a bad extension
 * Created by Andrew Davis
 * Created on 3/15/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <exception>
#include <string>
#include <sstream>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class ExtException : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit ExtException(const std::string& path);

			//destructor
			virtual ~ExtException();

			//copy constructor
			ExtException(const ExtException& ee);

			//assignment operator
			ExtException& operator=(const ExtException& src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
