/*
 * TextureException.h
 * Declares an exception that is thrown when a texture fails to load
 * Created by Andrew Davis
 * Created on 3/15/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <exception>
#include <string>
#include <sstream>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class TextureException : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			TextureException(const std::string& path);

			//destructor
			virtual ~TextureException();

			//copy constructor
			TextureException(const TextureException& te);

			//assignment operator
			TextureException& operator=(const 
							TextureException&
								src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
