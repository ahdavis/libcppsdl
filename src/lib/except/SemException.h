/*
 * SemException.h
 * Declares an exception that is thrown when a semaphore fails to init
 * Created by Andrew Davis
 * Created on 6/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <exception>
#include <string>
#include <sstream>
#include <cstdint>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class SemException final : public std::exception
	{
		//public fields and methods
		public:
			//constructor
			explicit SemException(std::uint32_t badLockCount);

			//destructor
			~SemException();

			//copy constructor
			SemException(const SemException& se);

			//assignment operator
			SemException& operator=(const SemException& src);

			//called when the exception is thrown
			const char* what() const throw() override;

		//private fields and methods
		private:
			//field
			std::string errMsg; //the error message
	};
}

//end of header
