/*
 * BundleException.cpp
 * Implements an exception that is thrown when a bundle file is not found
 * Created by Andrew Davis
 * Created on 6/24/2018
 *
 * Copyright (C) 2018 Andrew Davis
 *
 * Licensed under the MIT License
 */

//include header
#include "BundleException.h"

//include
#include <sstream>

//constructor
CPPSDL::BundleException::BundleException(const std::string& badPath)
	: errMsg() //init the error message
{
	//assemble the error message
	std::stringstream ss;
	ss << "Bundle file not found at path ";
	ss << badPath;
	ss << ".";
	this->errMsg = ss.str();
}

//destructor
CPPSDL::BundleException::~BundleException() {
	//no code needed
}

//copy constructor
CPPSDL::BundleException::BundleException(const CPPSDL::BundleException& be)
	: errMsg(be.errMsg)
{
	//no code needed
}

//assignment operator
CPPSDL::BundleException& CPPSDL::BundleException::operator=(const
						CPPSDL::BundleException& 
							src) {
	this->errMsg = src.errMsg; //assign the error message
	return *this; //and return the instance
}

//overridden what method - called when the exception is thrown
const char* CPPSDL::BundleException::what() const throw() {
	return this->errMsg.c_str(); //return the error message
}

//end of implementation
