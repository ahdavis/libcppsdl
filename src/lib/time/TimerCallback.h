/*
 * TimerCallback.h
 * Defines a typedef for timer callbacks
 * Created by Andrew Davis
 * Created on 6/3/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include <cstdint>

//namespace declaration
namespace CPPSDL {
	//typedef definition
	typedef std::uint32_t(*TimerCallback)(std::uint32_t interval,
						void* param);
}

//end of header
