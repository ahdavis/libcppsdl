/*
 * CountdownTimer.cpp
 * Implements a class that represents a countdown timer
 * Created by Andrew Davis
 * Created on 6/3/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "CountdownTimer.h"

//constructor
CPPSDL::CountdownTimer::CountdownTimer(std::uint32_t newLength,
				const CPPSDL::TimerCallback& newCallback,
				const std::string& newMessage)
	: length(newLength), callback(newCallback), msg(newMessage),
		data(), didStart(false) //init the fields
{
	//zero out the data field
	memset(&this->data, 0, sizeof(this->data));
}

//destructor
CPPSDL::CountdownTimer::~CountdownTimer() {
	if(this->didStart) { //if the timer started
		SDL_RemoveTimer(this->data); //then stop it
	}
}

//copy constructor
CPPSDL::CountdownTimer::CountdownTimer(const CPPSDL::CountdownTimer& cdt)
	: length(cdt.length), callback(cdt.callback), msg(cdt.msg),
		data(), didStart(false) //copy the fields
{
	//zero out the data field
	memset(&this->data, 0, sizeof(this->data));
}

//assignment operator
CPPSDL::CountdownTimer& CPPSDL::CountdownTimer::operator=(
				const CPPSDL::CountdownTimer& src) {
	this->length = src.length; //assign the length field
	this->callback = src.callback; //assign the callback field
	this->msg = src.msg; //assign the message field
	memset(&this->data, 0, sizeof(this->data)); //zero the data field
	this->didStart = false; //clear the start flag
	return *this; //and return the instance
}

//getLength method - returns the length of the timer
std::uint32_t CPPSDL::CountdownTimer::getLength() const {
	return this->length; //return the length field
}

//start method - starts the timer
void CPPSDL::CountdownTimer::start() {
	//start the timer
	this->data = SDL_AddTimer(this->length, this->callback,
					(void*)this->msg.c_str());

	//and set the start flag
	this->didStart = true;
}

//end of implementation
