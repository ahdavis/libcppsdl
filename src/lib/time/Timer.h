/*
 * Timer.h
 * Declares a class that represents a timer
 * Created by Andrew Davis
 * Created on 6/3/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <cstdint>
#include "../util/functions.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Timer final {
		//public fields and methods
		public:
			//default constructor
			Timer();

			//main constructor - allows the user to specify an
			//initial number of elapsed milliseconds
			explicit Timer(std::uint32_t startTime);

			//destructor
			~Timer();

			//copy constructor
			Timer(const Timer& t);

			//assignment operator
			Timer& operator=(const Timer& src);

			//getter methods
			
			//returns the number of milliseconds
			//elapsed on the timer
			std::uint32_t getElapsedTicks() const;

			//returns whether the timer is started
			bool isStarted() const;

			//returns whether the timer is paused
			bool isPaused() const;

			//other methods
			
			//starts the timer
			void start();

			//stops the timer
			void stop();

			//pauses the timer
			void pause();

			//unpauses the timer
			void unpause();

		//private fields and methods
		private:
			//fields
			
			//the clock time when the timer was started
			std::uint32_t startTicks;

			//the stored ticks during a pause
			std::uint32_t pausedTicks;

			//the status flags
			bool paused; //is the timer paused?
			bool started; //is the timer started?
	};
}

//end of header
