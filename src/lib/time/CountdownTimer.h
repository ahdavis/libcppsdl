/*
 * CountdownTimer.h
 * Declares a class that represents a countdown timer
 * Created by Andrew Davis
 * Created on 6/3/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <cstdint>
#include <cstdlib>
#include <string>
#include "TimerCallback.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class CountdownTimer final {
		//public fields and methods
		public:
			//constructor
			CountdownTimer(std::uint32_t newLength,
					const TimerCallback& newCallback,
					const std::string& newMessage);

			//destructor
			~CountdownTimer();

			//copy constructor
			CountdownTimer(const CountdownTimer& cdt);

			//assignment operator
			CountdownTimer& operator=(const CountdownTimer& 
							src);

			//getter method
			
			//returns the length of the timer
			std::uint32_t getLength() const;

			//other method
			
			//starts the timer
			void start();

		//private fields and methods
		private:
			//fields
			std::uint32_t length; //the length of the timer
			TimerCallback callback; //the timer's callback fn
			std::string msg; //the message passed to the timer
			SDL_TimerID data; //the timer data itself
			bool didStart; //is the timer started?
	};
}

//end of header
