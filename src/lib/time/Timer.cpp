/*
 * Timer.cpp
 * Implements a class that represents a timer
 * Created by Andrew Davis
 * Created on 6/3/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Timer.h"

//default constructor
CPPSDL::Timer::Timer()
	: Timer(0) //call the other constructor
{
	//no code needed
}

//main constructor
CPPSDL::Timer::Timer(std::uint32_t startTime)
	: startTicks(startTime), pausedTicks(0), paused(false),
		started(false) //init the fields
{
	//no code needed
}

//destructor
CPPSDL::Timer::~Timer() {
	this->stop(); //stop the timer
}

//copy constructor
CPPSDL::Timer::Timer(const CPPSDL::Timer& t)
	: startTicks(t.startTicks), pausedTicks(t.pausedTicks),
		paused(t.paused), started(t.started) //copy the fields
{
	//no code needed
}

//assignment operator
CPPSDL::Timer& CPPSDL::Timer::operator=(const CPPSDL::Timer& src) {
	this->startTicks = src.startTicks; //assign the start ticks
	this->pausedTicks = src.pausedTicks; //assign the paused ticks
	this->paused = src.paused; //assign the paused flag
	this->started = src.started; //assign the started flag
	return *this; //and return the instance
}

//getElapsedTicks method - returns the number of milliseconds 
//elapsed on the timer
std::uint32_t CPPSDL::Timer::getElapsedTicks() const {
	//the actual time
	std::uint32_t time = 0;

	//calculate the time elapsed
	if(this->started) { //if the timer is running
		if(this->paused) { //if the timer is paused
			//then the time is equal to the number
			//of ticks elapsed when the timer was paused
			time = this->pausedTicks;
		} else { //if the timer is running
			//then the time is equal to the current time
			//minus the start time
			time = CPPSDL::GetTicks() - this->startTicks;	
		}
	}

	//and return the elapsed time
	return time;
}

//isStarted method - returns whether the timer has started
bool CPPSDL::Timer::isStarted() const {
	return this->started; //return the started flag
}

//isPaused method - returns whether the timer is paused
bool CPPSDL::Timer::isPaused() const {
	//return whether the timer is both running and paused
	return this->paused && this->started;
}

//start method - starts the timer
void CPPSDL::Timer::start() {
	//start the timer
	this->started = true;

	//unpause the timer
	this->paused = false;

	//get the current clock time
	this->startTicks = CPPSDL::GetTicks();

	//and reset the paused ticks
	this->pausedTicks = 0;
}

//stop method - stops the timer
void CPPSDL::Timer::stop() {
	//stop the timer
	this->started = false;

	//unpause the timer
	this->paused = false;

	//and clear the tick fields
	this->startTicks = 0;
	this->pausedTicks = 0;
}

//pause method - pauses the timer
void CPPSDL::Timer::pause() {
	//determine whether to pause the timer
	if(this->started && !this->paused) { //if the timer is not paused
		//then pause the timer
		this->paused = true;

		//calculate the paused ticks
		this->pausedTicks = CPPSDL::GetTicks() - this->startTicks;

		//and clear the start ticks
		this->startTicks = 0;
	}
}

//unpause method - unpauses the timer
void CPPSDL::Timer::unpause() {
	//determine whether to unpause the timer
	if(this->started && this->paused) { //if the timer is paused
		//then unpause the timer
		this->paused = false;

		//calculate the starting ticks
		this->startTicks = CPPSDL::GetTicks() - this->pausedTicks;

		//and reset the paused ticks
		this->pausedTicks = 0;
	}
}

//end of implementation
