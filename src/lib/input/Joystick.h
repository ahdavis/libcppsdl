/*
 * Joystick.h
 * Declares a class that represents a joystick
 * Created by Andrew Davis
 * Created on 5/31/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <memory>
#include <cstdlib>
#include <string>
#include "../except/JoystickException.h"
#include "../except/InitException.h"
#include "../event/Event.h"
#include "../util/log.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Joystick final {
		//public fields and methods
		public:
			//default constructor - uses 0 as the joystick ID
			Joystick();

			//first main constructor
			explicit Joystick(unsigned int newID);

			//second main constructor
			Joystick(unsigned int newID, int newDeadZone);

			//destructor
			~Joystick();

			//copy constructor
			Joystick(const Joystick& js);

			//assignment operator
			Joystick& operator=(const Joystick& src);

			//getter methods
			
			//returns the ID of the joystick
			unsigned int getID() const;

			//returns the joystick's dead zone
			int getDeadZone() const;

			//returns the name of the joystick
			std::string getName() const;

			//returns the number of axes on the joystick
			int getAxisCount() const;

			//returns the number of buttons on the joystick
			int getButtonCount() const;

			//returns the number of balls on the joystick
			int getBallCount() const;

			//setter methods
			
			//sets the ID of the joystick
			void setID(int newID);

			//sets the joystick's dead zone
			void setDeadZone(int newDeadZone);

			//other methods
			
			//returns whether a joystick event is below
			//the dead zone
			bool isBelowDeadZone(const Event& evnt) const;

			//returns whether a joystick event is above
			//the dead zone
			bool isAboveDeadZone(const Event& evnt) const;

			//returns whether a joystick event is left of
			//the dead zone
			bool isLeftOfDeadZone(const Event& evnt) const;

			//returns whether a joystick event is right of
			//the dead zone
			bool isRightOfDeadZone(const Event& evnt) const;

			//rumbles the joystick at a specified strength
			//for a specified number of milliseconds
			void rumble(double strength, unsigned int ms);

			//static methods
			
			//returns whether a joystick is connected
			static bool hasConnection();

			//returns the number of connected joysticks
			static unsigned int getNumberOfConnections();

		//private fields and methods
		private:
			//method
			void initDataPtr(); //initializes the data pointer

			//static constant
			static const int DEFAULT_DEAD_ZONE;

			//fields
			std::shared_ptr<SDL_Joystick> data;
			unsigned int id; //the ID of the joystick
			int deadZone; //specifies a stick range to ignore
	};
}

//end of header
