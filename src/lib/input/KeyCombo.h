/*
 * KeyCombo.h
 * Enumerates key combinations for libcppsdl
 * Created by Andrew Davis
 * Created on 6/5/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//no includes

//namespace declaration
namespace CPPSDL {
	//enum definition
	enum class KeyCombo {
		None = 0x00, //no key combo
		Copy, //copy command
		Cut, //cut command
		Paste, //paste command
		Undo, //undo command
		Redo, //redo command
	};
}

//end of header
