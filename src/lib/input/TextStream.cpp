/*
 * TextStream.cpp
 * Implements a singleton class that represents a text stream
 * Created by Andrew Davis
 * Created on 6/5/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "TextStream.h"

//private constructor
CPPSDL::TextStream::TextStream()
	: text(), undoStack(), redoStack(),
		openFlag(false), updateFlag(false) 
{
	//no code needed
}

//destructor
CPPSDL::TextStream::~TextStream() {
	this->close(); //close the stream
}

//static getInstance method - returns an instance of the stream
CPPSDL::TextStream& CPPSDL::TextStream::getInstance() {
	static CPPSDL::TextStream instance; //get an instance of the class

	return instance; //and return it
}

//getText method - returns the text read by the stream
const std::string& CPPSDL::TextStream::getText() const {
	return this->text; //return the text field
}

//isOpen method - returns whether the stream is open
bool CPPSDL::TextStream::isOpen() const {
	return this->openFlag; //return the open flag
}

//hasText method - returns whether the stream has text
bool CPPSDL::TextStream::hasText() const {
	return this->text != ""; //return whether the text is not empty
}

//didUpdate method - returns whether the stream was updated
bool CPPSDL::TextStream::didUpdate() const {
	return this->updateFlag; //return the update flag
}

//open method - opens the stream
void CPPSDL::TextStream::open() {
	//open the stream
	if(!this->openFlag) { //if the stream is not open
		SDL_StartTextInput(); //then open it
		this->openFlag = true; //and update the flag
	}
}

//close method - closes the stream
void CPPSDL::TextStream::close() {
	//close the stream
	if(this->openFlag) { //if the stream is open
		SDL_StopTextInput(); //then close it
		this->flush(); //flush it
		this->openFlag = false; //and update the flag
	}
}

//update method - reads text from an event and updates the text field
CPPSDL::KeyCombo CPPSDL::TextStream::update(const CPPSDL::Event& e) {
	//declare the return value
	CPPSDL::KeyCombo ret = CPPSDL::KeyCombo::None;

	//clear the update flag
	this->updateFlag = false;

	//handle the event
	if(e.getType() == CPPSDL::Events::KeyDown) { //key was pressed
		//get the key modulation code
		std::uint16_t keyMod = e.getKeyMod();

		//get the keypress
		auto key = e.getKey();

		//check for backspace
		if((key == CPPSDL::Keys::backspace) &&
				(this->text.length() > 0)) {
			//remove the last character from the text
			this->text.pop_back(); 

			//and set the update flag
			this->updateFlag = true;
		} else if(keyMod & CPPSDL::KeyMod::Ctrl) { //control key
			//check for key combinations
			if(key == CPPSDL::Keys::c) { //copy
				//set the return value to copy
				ret = CPPSDL::KeyCombo::Copy; 

				//and copy the text to the clipboard
				CPPSDL::Clipboard::getInstance()
					.setText(this->text);

			} else if(key == CPPSDL::Keys::v) { 
				//set the return value to paste
				ret = CPPSDL::KeyCombo::Paste;

				//put the current text onto the undo
				//stack
				this->undoStack.push(this->text);

				//and retrieve the clipboard text if
				//there is any
				if(CPPSDL::Clipboard::getInstance()
					.hasText()) {
					this->text = 
					CPPSDL::Clipboard::getInstance()
						.getText();
				}

			} else if(key == CPPSDL::Keys::x) {
				//set the return value to cut
				ret = CPPSDL::KeyCombo::Cut;

				//put the current text onto the undo stack
				this->undoStack.push(this->text);

				//set the clipboard text
				CPPSDL::Clipboard::getInstance()
					.setText(this->text);

				//and flush the stream
				this->flush();

			} else if(key == CPPSDL::Keys::z) {
				//set the return value to undo
				ret = CPPSDL::KeyCombo::Undo;

				//push the text field onto the redo stack
				this->redoStack.push(this->text);

				//and undo the text
				if(this->undoStack.empty()) {
					//empty undo stack, so clear
					//the text
					this->flush();
				} else {
					//put the top of the undo stack
					//into the text field
					this->text = this->undoStack.top();
					
					//and pop it off
					this->undoStack.pop();
				}

			} else if(key == CPPSDL::Keys::y) {
				//set the return value to redo
				ret = CPPSDL::KeyCombo::Redo;

				//push the text field onto the undo stack
				this->undoStack.push(this->text);

				//and redo the text
				if(this->redoStack.empty()) {
					//empty stack, so clear the text
					this->flush();
				} else {
					//put the top of the redo stack
					//into the text field
					this->text = this->redoStack.top();

					//and pop it off
					this->redoStack.pop();
				}
			}

			//set the update flag
			this->updateFlag = true;
		}

	} else if(e.getType() == CPPSDL::Events::TextInput) { //text read
		//get the text
		std::string readText = e.getText();

		//and store it
		this->undoStack.push(this->text); //save the text
		this->text += readText; //then update the text
		this->updateFlag = true; //and set the update flag
		
	}

	//return the key combo
	return ret;
}

//flush method - flushes the text stream
void CPPSDL::TextStream::flush() {
	this->text.clear(); //clear the text field
}

//end of implementation
