/*
 * TextStream.h
 * Declares a singleton class that represents a text stream
 * Created by Andrew Davis
 * Created on 6/5/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <string>
#include <stack>
#include <cstdint>
#include "KeyCombo.h"
#include "../event/Event.h"
#include "../event/Events.h"
#include "../util/functions.h"
#include"../util/Clipboard.h"
#include "Keys.h"
#include "KeyMod.h"


//namespace declaration
namespace CPPSDL {
	//class declaration
	class TextStream final {
		//public fields and methods
		public:
			//constructor is private
			
			//destructor
			~TextStream();

			//delete the copy constructor
			TextStream(const TextStream& ts) = delete;

			//delete the assignment operator
			TextStream& operator=(const TextStream& src)
				= delete;

			//getter methods
			
			//returns an instance of the stream
			static TextStream& getInstance();
			
			//returns the text read by the stream
			const std::string& getText() const;

			//returns whether the stream is currently open
			bool isOpen() const;

			//returns whether the stream currently has text
			bool hasText() const;

			//returns whether the stream was updated during
			//the last call to read()
			bool didUpdate() const;
			
			//other methods
			
			//opens the text stream
			void open();

			//closes the text stream
			void close();

			//Reads text from an event and returns 
			//a key combination value denoting the
			//key combination in the event (if any).
			//Returns KeyCombo::None if no key combination
			//was entered. If a key combo is read, then
			//the stream's text will not be updated.
			KeyCombo update(const Event& e);

			//flushes the text stream
			void flush();

		//private fields and methods
		private:
			//constructor
			TextStream();

			//fields
			std::string text; //the text that was read
			std::stack<std::string> undoStack; //the undo stack
			std::stack<std::string> redoStack; //the redo stack
			bool openFlag; //whether the stream is open
			bool updateFlag; //whether the stream was updated
	};
}

//end of header
