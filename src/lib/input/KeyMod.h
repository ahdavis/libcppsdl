/*
 * KeyMod.h
 * Declares constants that describe key modifier codes
 * Created by Andrew Davis
 * Created on 6/4/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include <SDL2/SDL.h>

//outer namespace declaration
namespace CPPSDL {
	//inner namespace declaration
	namespace KeyMod {
		//constant declarations
		
		//primary modifiers
		extern const unsigned int None; //no modifier
		extern const unsigned int LShift; //left shift key
		extern const unsigned int RShift; //right shift key
		extern const unsigned int LCtrl; //left control key
		extern const unsigned int RCtrl; //right control key
		extern const unsigned int LAlt; //left alt key
		extern const unsigned int RAlt; //right alt key
		extern const unsigned int LGUI; //left GUI (Windows) key
		extern const unsigned int RGUI; //right GUI (Windows) key
		extern const unsigned int NumLock; //Num Lock key
		extern const unsigned int CapsLock; //Caps Lock key
		extern const unsigned int AltGr; //AltGR key

		//composite modifiers
		extern const unsigned int Ctrl; //either Ctrl key
		extern const unsigned int Shift; //either shift key
		extern const unsigned int Alt; //either Alt key
		extern const unsigned int GUI; //either GUI (Windows) key
	}
}

//end of header
