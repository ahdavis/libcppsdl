/*
 * Button.cpp
 * Implements a class that represents a button for CPPSDL
 * Created by Andrew Davis
 * Created on 4/2/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Button.h"

//constructor
CPPSDL::Button::Button(const CPPSDL::Rect& newBounds, 
			const CPPSDL::ButtonCallback& newCallback, 
				const CPPSDL::Texture& newTex)
	: bounds(newBounds), callback(newCallback), tex(newTex)
{
	//no code needed
}

//destructor
CPPSDL::Button::~Button() {
	//no code needed
}

//copy constructor
CPPSDL::Button::Button(const CPPSDL::Button& b)
	: bounds(b.bounds), callback(b.callback), tex(b.tex) 
{
	//no code needed
}

//assignment operator
CPPSDL::Button& CPPSDL::Button::operator=(const CPPSDL::Button& src) {
	this->bounds = src.bounds; //assign the bounds
	this->callback = src.callback; //assign the callback
	this->tex = src.tex; //assign the texture
	return *this; //and return the instance
}

//getTexture method - returns the Button's texture
const CPPSDL::Texture& CPPSDL::Button::getTexture() const {
	return this->tex; //return the texture field
}

//getBounds method - returns the bounds of the Button
const CPPSDL::Rect& CPPSDL::Button::getBounds() const {
	return this->bounds; //return the bounds field
}

//setTexture method - sets the Button's texture
void CPPSDL::Button::setTexture(const CPPSDL::Texture& newTex) {
	this->tex = newTex; //set the texture field
}

//setCallback method - sets the Button's callback function
void CPPSDL::Button::setCallback(const CPPSDL::ButtonCallback&
					newCallback) {
	this->callback = newCallback; //set the callback field
}

//onClick method - called when the Button is clicked
void CPPSDL::Button::onClick(const CPPSDL::Event& e) {
	this->callback(*this, e); //execute the callback
}

//end of implementation
