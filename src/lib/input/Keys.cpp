/*
 * Keys.cpp
 * Defines constants that represent keycodes for cppsdl
 * Created by Andrew Davis
 * Created on 3/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Keys.h"

//constant definitions

//number keys
const unsigned int CPPSDL::Keys::zero = SDLK_0; //numeric 0
const unsigned int CPPSDL::Keys::one = SDLK_1; //numeric 1
const unsigned int CPPSDL::Keys::two = SDLK_2; //numeric 2
const unsigned int CPPSDL::Keys::three = SDLK_3; //numeric 3
const unsigned int CPPSDL::Keys::four = SDLK_4; //numeric 4
const unsigned int CPPSDL::Keys::five = SDLK_5; //numeric 5
const unsigned int CPPSDL::Keys::six = SDLK_6; //numeric 6
const unsigned int CPPSDL::Keys::seven = SDLK_7; //numeric 7
const unsigned int CPPSDL::Keys::eight = SDLK_8; //numeric 8
const unsigned int CPPSDL::Keys::nine = SDLK_9; //numeric 9

//alphabet keys
const unsigned int CPPSDL::Keys::a = SDLK_a; //letter A
const unsigned int CPPSDL::Keys::b = SDLK_b; //letter B
const unsigned int CPPSDL::Keys::c = SDLK_c; //letter C
const unsigned int CPPSDL::Keys::d = SDLK_d; //letter D
const unsigned int CPPSDL::Keys::e = SDLK_e; //letter E
const unsigned int CPPSDL::Keys::f = SDLK_f; //letter F
const unsigned int CPPSDL::Keys::g = SDLK_g; //letter G
const unsigned int CPPSDL::Keys::h = SDLK_h; //letter H
const unsigned int CPPSDL::Keys::i = SDLK_i; //letter I
const unsigned int CPPSDL::Keys::j = SDLK_j; //letter J
const unsigned int CPPSDL::Keys::k = SDLK_k; //letter K
const unsigned int CPPSDL::Keys::l = SDLK_l; //letter L
const unsigned int CPPSDL::Keys::m = SDLK_m; //letter M
const unsigned int CPPSDL::Keys::n = SDLK_n; //letter N
const unsigned int CPPSDL::Keys::o = SDLK_o; //letter O
const unsigned int CPPSDL::Keys::p = SDLK_p; //letter P
const unsigned int CPPSDL::Keys::q = SDLK_q; //letter Q
const unsigned int CPPSDL::Keys::r = SDLK_r; //letter R
const unsigned int CPPSDL::Keys::s = SDLK_s; //letter S
const unsigned int CPPSDL::Keys::t = SDLK_t; //letter T
const unsigned int CPPSDL::Keys::u = SDLK_u; //letter U
const unsigned int CPPSDL::Keys::v = SDLK_v; //letter V
const unsigned int CPPSDL::Keys::w = SDLK_w; //letter W
const unsigned int CPPSDL::Keys::x = SDLK_x; //letter X
const unsigned int CPPSDL::Keys::y = SDLK_y; //letter Y
const unsigned int CPPSDL::Keys::z = SDLK_z; //letter Z

//utility keys
const unsigned int CPPSDL::Keys::context = SDLK_APPLICATION; //Windows key
const unsigned int CPPSDL::Keys::amute = SDLK_AUDIOMUTE; //audio mute key
const unsigned int CPPSDL::Keys::next = SDLK_AUDIONEXT; //next track key
const unsigned int CPPSDL::Keys::play = SDLK_AUDIOPLAY; //play key
const unsigned int CPPSDL::Keys::prev = SDLK_AUDIOPREV; //prev track key
const unsigned int CPPSDL::Keys::stop = SDLK_AUDIOSTOP; //stop key
const unsigned int CPPSDL::Keys::brightdown = SDLK_BRIGHTNESSDOWN;
const unsigned int CPPSDL::Keys::brightup = SDLK_BRIGHTNESSUP;
const unsigned int CPPSDL::Keys::capslock = SDLK_CAPSLOCK; //caps lock key
const unsigned int CPPSDL::Keys::dswitch = SDLK_DISPLAYSWITCH;
const unsigned int CPPSDL::Keys::eject = SDLK_EJECT; //eject key
const unsigned int CPPSDL::Keys::insert = SDLK_INSERT; //insert key
const unsigned int CPPSDL::Keys::mute = SDLK_MUTE; //mute key
const unsigned int CPPSDL::Keys::pgdown = SDLK_PAGEDOWN; //page down key
const unsigned int CPPSDL::Keys::pgup = SDLK_PAGEUP; //page up key
const unsigned int CPPSDL::Keys::pause = SDLK_PAUSE; //pause key
const unsigned int CPPSDL::Keys::space = SDLK_SPACE; //spacebar
const unsigned int CPPSDL::Keys::tab = SDLK_TAB; //tab key
const unsigned int CPPSDL::Keys::voldown = SDLK_VOLUMEDOWN; //volume down
const unsigned int CPPSDL::Keys::volup = SDLK_VOLUMEUP; //volume up

//symbol keys
const unsigned int CPPSDL::Keys::quote = SDLK_QUOTE; //quote key
const unsigned int CPPSDL::Keys::backslash = SDLK_BACKSLASH; //backslash
const unsigned int CPPSDL::Keys::comma = SDLK_COMMA; //comma
const unsigned int CPPSDL::Keys::equals = SDLK_EQUALS; //equals sign
const unsigned int CPPSDL::Keys::bquote = SDLK_BACKQUOTE; //backquote
const unsigned int CPPSDL::Keys::lbckt = SDLK_LEFTBRACKET; //left bracket
const unsigned int CPPSDL::Keys::minus = SDLK_MINUS; //minus sign
const unsigned int CPPSDL::Keys::period = SDLK_PERIOD; //period key
const unsigned int CPPSDL::Keys::rbckt = SDLK_RIGHTBRACKET; //right bracket
const unsigned int CPPSDL::Keys::scolon = SDLK_SEMICOLON; //semicolon
const unsigned int CPPSDL::Keys::slash = SDLK_SLASH; //forward slash

//abstract keys
const unsigned int CPPSDL::Keys::backspace = SDLK_BACKSPACE; //backspace
const unsigned int CPPSDL::Keys::copy = SDLK_COPY; //copy command
const unsigned int CPPSDL::Keys::cut = SDLK_CUT; //cut command
const unsigned int CPPSDL::Keys::del = SDLK_DELETE; //delete key
const unsigned int CPPSDL::Keys::esc = SDLK_ESCAPE; //escape key
const unsigned int CPPSDL::Keys::lalt = SDLK_LALT; //left alt key
const unsigned int CPPSDL::Keys::lctrl = SDLK_LCTRL; //left control key
const unsigned int CPPSDL::Keys::lgui = SDLK_LGUI; //left alt context key
const unsigned int CPPSDL::Keys::lshift = SDLK_LSHIFT; //left shift key
const unsigned int CPPSDL::Keys::numlock = SDLK_NUMLOCKCLEAR; //numlock key
const unsigned int CPPSDL::Keys::paste = SDLK_PASTE; //paste command
const unsigned int CPPSDL::Keys::prtscr = SDLK_PRINTSCREEN; //print screen
const unsigned int CPPSDL::Keys::ralt = SDLK_RALT; //right alt key
const unsigned int CPPSDL::Keys::rctrl = SDLK_RCTRL; //right control key
const unsigned int CPPSDL::Keys::ret = SDLK_RETURN; //return key
const unsigned int CPPSDL::Keys::rgui = SDLK_RGUI; //right alt context key
const unsigned int CPPSDL::Keys::rshift = SDLK_RSHIFT; //right shift key
const unsigned int CPPSDL::Keys::scrlock = SDLK_SCROLLLOCK; //scroll lock
const unsigned int CPPSDL::Keys::undo = SDLK_UNDO; //undo command

//function keys
const unsigned int CPPSDL::Keys::f1 = SDLK_F1; //F1 key
const unsigned int CPPSDL::Keys::f2 = SDLK_F2; //F2 key
const unsigned int CPPSDL::Keys::f3 = SDLK_F3; //F3 key
const unsigned int CPPSDL::Keys::f4 = SDLK_F4; //F4 key
const unsigned int CPPSDL::Keys::f5 = SDLK_F5; //F5 key
const unsigned int CPPSDL::Keys::f6 = SDLK_F6; //F6 key
const unsigned int CPPSDL::Keys::f7 = SDLK_F7; //F7 key
const unsigned int CPPSDL::Keys::f8 = SDLK_F8; //F8 key
const unsigned int CPPSDL::Keys::f9 = SDLK_F9; //F9 key
const unsigned int CPPSDL::Keys::f10 = SDLK_F10; //F10 key
const unsigned int CPPSDL::Keys::f11 = SDLK_F11; //F11 key
const unsigned int CPPSDL::Keys::f12 = SDLK_F12; //F12 key

//arrow keys
const unsigned int CPPSDL::Keys::up = SDLK_UP; //up arrow
const unsigned int CPPSDL::Keys::down = SDLK_DOWN; //down arrow
const unsigned int CPPSDL::Keys::left = SDLK_LEFT; //left arrow
const unsigned int CPPSDL::Keys::right = SDLK_RIGHT; //right arrow

//keypad keys
const unsigned int CPPSDL::Keys::k0 = SDLK_KP_0; //keypad 0
const unsigned int CPPSDL::Keys::k1 = SDLK_KP_1; //keypad 1
const unsigned int CPPSDL::Keys::k2 = SDLK_KP_2; //keypad 2
const unsigned int CPPSDL::Keys::k3 = SDLK_KP_3; //keypad 3
const unsigned int CPPSDL::Keys::k4 = SDLK_KP_4; //keypad 4
const unsigned int CPPSDL::Keys::k5 = SDLK_KP_5; //keypad 5
const unsigned int CPPSDL::Keys::k6 = SDLK_KP_6; //keypad 6
const unsigned int CPPSDL::Keys::k7 = SDLK_KP_7; //keypad 7
const unsigned int CPPSDL::Keys::k8 = SDLK_KP_8; //keypad 8
const unsigned int CPPSDL::Keys::k9 = SDLK_KP_9; //keypad 9

//virtual keys
const unsigned int CPPSDL::Keys::amp = SDLK_AMPERSAND; //ampersand
const unsigned int CPPSDL::Keys::star = SDLK_ASTERISK; //asterisk
const unsigned int CPPSDL::Keys::at = SDLK_AT; //@ sign
const unsigned int CPPSDL::Keys::caret = SDLK_CARET; //^ sign
const unsigned int CPPSDL::Keys::colon = SDLK_COLON; //colon
const unsigned int CPPSDL::Keys::dollar = SDLK_DOLLAR; //$ sign
const unsigned int CPPSDL::Keys::exclaim = SDLK_EXCLAIM; //exclamation point
const unsigned int CPPSDL::Keys::gt = SDLK_GREATER; //greater than
const unsigned int CPPSDL::Keys::hash = SDLK_HASH; //# sign
const unsigned int CPPSDL::Keys::lparen = SDLK_LEFTPAREN; //left parenthesis
const unsigned int CPPSDL::Keys::lt = SDLK_LESS; //less than
const unsigned int CPPSDL::Keys::percent = SDLK_PERCENT; //percent sign
const unsigned int CPPSDL::Keys::plus = SDLK_PLUS; //plus sign
const unsigned int CPPSDL::Keys::question = SDLK_QUESTION; //question mark
const unsigned int CPPSDL::Keys::dblquote = SDLK_QUOTEDBL; //" mark
const unsigned int CPPSDL::Keys::rparen = SDLK_RIGHTPAREN; //) mark
const unsigned int CPPSDL::Keys::underscr = SDLK_UNDERSCORE; //underscore

//end of definitions
