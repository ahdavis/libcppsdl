/*
 * Joystick.cpp
 * Implements a class that represents a joystick
 * Created by Andrew Davis
 * Created on 5/31/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Joystick.h"

//init the constant
const int CPPSDL::Joystick::DEFAULT_DEAD_ZONE = 8000;

//default constructor
CPPSDL::Joystick::Joystick()
	: Joystick(0) //call the first main constructor
{
	//no code needed
}

//first main constructor
CPPSDL::Joystick::Joystick(unsigned int newID)
	: Joystick(newID, DEFAULT_DEAD_ZONE) //call the second main ctor
{
	//no code needed
}

//second main constructor
CPPSDL::Joystick::Joystick(unsigned int newID, int newDeadZone)
	: data(NULL), id(newID), deadZone(newDeadZone) //init the fields
{
	//init the data pointer
	this->initDataPtr();
}

//destructor
CPPSDL::Joystick::~Joystick() {
	//no code needed
}

//copy constructor
CPPSDL::Joystick::Joystick(const CPPSDL::Joystick& js)
	: data(js.data), id(js.id), deadZone(js.deadZone) //copy the fields
{
	//no code needed
}

//assignment operator
CPPSDL::Joystick& CPPSDL::Joystick::operator=(const CPPSDL::Joystick& 
						src) {
	this->data = src.data; //assign the data pointer
	this->id = src.id; //assign the ID field
	this->deadZone = src.deadZone; //assign the dead zone
	return *this; //and return the instance
}

//getID method - returns the ID of the joystick
unsigned int CPPSDL::Joystick::getID() const {
	return this->id; //return the ID field
}

//getDeadZone method - returns the joystick's dead zone
int CPPSDL::Joystick::getDeadZone() const {
	return this->deadZone; //return the dead zone field
}

//getName method - returns the name of the joystick
std::string CPPSDL::Joystick::getName() const {
	//return the name of the joystick as a string object
	return std::string(SDL_JoystickNameForIndex(this->id));
}

//getAxisCount method - returns the number of axes on the joystick
int CPPSDL::Joystick::getAxisCount() const {
	//return the number of axes on the joystick
	return SDL_JoystickNumAxes(this->data.get());
}

//getButtonCount method - returns the number of buttons on the joystick
int CPPSDL::Joystick::getButtonCount() const {
	//return the number of buttons on the joystick
	return SDL_JoystickNumButtons(this->data.get());
}

//getBallCount method - returns the number of balls on the joystick
int CPPSDL::Joystick::getBallCount() const {
	//return the number of balls on the joystick
	return SDL_JoystickNumBalls(this->data.get());
}

//setID method - sets the joystick's ID
void CPPSDL::Joystick::setID(int newID) {
	this->id = newID; //update the ID field
	this->initDataPtr(); //and update the data pointer
}

//setDeadZone method - sets the joystick's dead zone
void CPPSDL::Joystick::setDeadZone(int newDeadZone) {
	this->deadZone = newDeadZone; //update the dead zone
}

//isBelowDeadZone method - returns whether the joystick is below the
//dead zone
bool CPPSDL::Joystick::isBelowDeadZone(const CPPSDL::Event& evnt) const {
	if(evnt.getJoyAxis() == 1) { //y-axis motion
		//return whether the joystick is below the dead zone
		return evnt.getJoyValue() < -this->deadZone;
	} else { //x-axis motion
		return false;
	}
}

//isAboveDeadZone method - returns whether the joystick is above the
//dead zone
bool CPPSDL::Joystick::isAboveDeadZone(const CPPSDL::Event& evnt) const {
	if(evnt.getJoyAxis() == 1) { //y-axis motion
		//return whether the joystick is above the dead zone
		return evnt.getJoyValue() > this->deadZone;
	} else { //x-axis motion
		return false;
	}
}

//isLeftOfDeadZone method - returns whether the joystick is left of the
//dead zone
bool CPPSDL::Joystick::isLeftOfDeadZone(const CPPSDL::Event& evnt) const {
	if(evnt.getJoyAxis() == 0) { //x-axis motion
		//return whether the joystick is below the dead zone
		return evnt.getJoyValue() < -this->deadZone;
	} else { //y-axis motion
		return false;
	}
}

//isRightOfDeadZone method - returns whether the joystick is right of the
//dead zone
bool CPPSDL::Joystick::isRightOfDeadZone(const CPPSDL::Event& evnt) const {
	if(evnt.getJoyAxis() == 0) { //x-axis motion
		//return whether the joystick is above the dead zone
		return evnt.getJoyValue() > this->deadZone;
	} else { //x-axis motion
		return false;
	}
}

//rumble method - rumbles the joystick at a specified strength for
//a specified number of milliseconds
void CPPSDL::Joystick::rumble(double strength, unsigned int ms) {
	//get a haptic device from the controller
	SDL_Haptic* hapt = SDL_HapticOpenFromJoystick(this->data.get());

	//verify that the controller supports haptics
	if(hapt == NULL) { //if the controller does not support haptics
		//then log a warning
		CPPSDL::LogWarning("Haptics not supported for controller "
			+ this->getName() + "! Message: " 
				+ SDL_GetError());
	} else { //if haptics are supported
		//then initialize rumble
		auto res = SDL_HapticRumbleInit(hapt);

		//verify that haptics were initialized successfully
		if(res < 0) { //if initialization failed
			//then close the haptics interface
			SDL_HapticClose(hapt);
			hapt = NULL;

			//and throw an exception
			throw CPPSDL::InitException("SDL_Haptic", 
						SDL_GetError());
		} else { //if initialization succeeded
			//then rumble the controller
			auto hRes = SDL_HapticRumblePlay(hapt,
						strength, ms);

			//and make sure that the rumble succeeded
			if(hRes != 0) { //if the rumble failed
				//then log a warning
				CPPSDL::LogWarning(
			std::string("Unable to play rumble! Message: ")
					+ SDL_GetError());
			}

			//finally, deallocate the haptic interface
			SDL_HapticClose(hapt);
			hapt = NULL;
		}
	}
}

//static hasConnection method - returns whether a joystick is
//currently connected
bool CPPSDL::Joystick::hasConnection() {
	//return whether at least one joystick is connected
	return getNumberOfConnections() >= 1; 
}

//static getNumberOfConnections method - returns the number of joysticks
//that are currently connected
unsigned int CPPSDL::Joystick::getNumberOfConnections() {
	//return the number of connected joysticks
	return SDL_NumJoysticks();
}

//private initDataPtr method - initializes the data pointer
void CPPSDL::Joystick::initDataPtr() {
	this->data = std::shared_ptr<SDL_Joystick>(
			SDL_JoystickOpen(this->id), 
			[=](SDL_Joystick* stick) {
				if(SDL_JoystickGetAttached(stick)) {
					SDL_JoystickClose(stick);
					stick = NULL;
				}
			});

	//make sure that the joystick opened successfully
	if(this->data == NULL) { //if the joystick failed to open
		//then throw an exception
		throw CPPSDL::JoystickException(this->id);
	}

}

//end of implementation
