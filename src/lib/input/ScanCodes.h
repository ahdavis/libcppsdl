/*
 * ScanCodes.h
 * Declares constants that represent scancodes for CPPSDL
 * Created by Andrew Davis
 * Created on 4/13/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include <SDL2/SDL.h>

//outer namespace declaration
namespace CPPSDL {
	//inner namespace declaration
	namespace ScanCodes {
		//constant declarations

		//number scancodes
		extern const unsigned int zero; //numeric 0
		extern const unsigned int one; //numeric 1
		extern const unsigned int two; //numeric 2
		extern const unsigned int three; //numeric 3
		extern const unsigned int four; //numeric 4
		extern const unsigned int five; //numeric 5
		extern const unsigned int six; //numeric 6
		extern const unsigned int seven; //numeric 7
		extern const unsigned int eight; //numeric 8
		extern const unsigned int nine; //numeric 9

		//alphabet scancodes
		extern const unsigned int a; //letter A
		extern const unsigned int b; //letter B
		extern const unsigned int c; //letter C
		extern const unsigned int d; //letter D
		extern const unsigned int e; //letter E
		extern const unsigned int f; //letter F
		extern const unsigned int g; //letter G
		extern const unsigned int h; //letter H
		extern const unsigned int i; //letter I
		extern const unsigned int j; //letter J
		extern const unsigned int k; //letter K
		extern const unsigned int l; //letter L
		extern const unsigned int m; //letter M
		extern const unsigned int n; //letter N
		extern const unsigned int o; //letter O
		extern const unsigned int p; //letter P
		extern const unsigned int q; //letter Q
		extern const unsigned int r; //letter R
		extern const unsigned int s; //letter S
		extern const unsigned int t; //letter T
		extern const unsigned int u; //letter U
		extern const unsigned int v; //letter V
		extern const unsigned int w; //letter W
		extern const unsigned int x; //letter X
		extern const unsigned int y; //letter Y
		extern const unsigned int z; //letter Z

		//utility scancodes
		extern const unsigned int context; //Windows key
		extern const unsigned int amute; //audio mute key
		extern const unsigned int next; //next track key
		extern const unsigned int play; //play key
		extern const unsigned int prev; //previous track key
		extern const unsigned int stop; //stop key
		extern const unsigned int brightdown; //brightness down key
		extern const unsigned int brightup; //brightness up key
		extern const unsigned int capslock; //caps lock key
		extern const unsigned int dswitch; //display switch key
		extern const unsigned int eject; //eject key
		extern const unsigned int insert; //insert key
		extern const unsigned int mute; //mute key
		extern const unsigned int pgdown; //page down key
		extern const unsigned int pgup; //page up key
		extern const unsigned int pause; //pause key
		extern const unsigned int space; //spacebar
		extern const unsigned int tab; //tab key
		extern const unsigned int voldown; //volume down key
		extern const unsigned int volup; //volume up key

		//symbol scancodes
		extern const unsigned int quote; //quote key
		extern const unsigned int backslash; //backslash key
		extern const unsigned int comma; //comma key
		extern const unsigned int equals; //equals key
		extern const unsigned int bquote; //backquote key
		extern const unsigned int lbckt; //left bracket key
		extern const unsigned int minus; //minus key
		extern const unsigned int period; //period key
		extern const unsigned int rbckt; //right bracket key
		extern const unsigned int scolon; //semicolon key
		extern const unsigned int slash; //forward slash key

		//abstract scancodes
		extern const unsigned int backspace; //backspace key
		extern const unsigned int copy; //copy command
		extern const unsigned int cut; //cut command
		extern const unsigned int del; //delete key
		extern const unsigned int esc; //escape key
		extern const unsigned int lalt; //left alt key
		extern const unsigned int lctrl; //left control key
		extern const unsigned int lgui; //left alternate context key
		extern const unsigned int lshift; //left shift key
		extern const unsigned int numlock; //numlock key
		extern const unsigned int paste; //paste command
		extern const unsigned int prtscr; //print screen key
		extern const unsigned int ralt; //right alt key
		extern const unsigned int rctrl; //right control key
		extern const unsigned int ret; //return key
		extern const unsigned int rgui; //right alt context key
		extern const unsigned int rshift; //right shift key
		extern const unsigned int scrlock; //scroll lock key
		extern const unsigned int undo; //undo command

		//function scancodes
		extern const unsigned int f1; //F1 key
		extern const unsigned int f2; //F2 key
		extern const unsigned int f3; //F3 key
		extern const unsigned int f4; //F4 key
		extern const unsigned int f5; //F5 key
		extern const unsigned int f6; //F6 key
		extern const unsigned int f7; //F7 key
		extern const unsigned int f8; //F8 key
		extern const unsigned int f9; //F9 key
		extern const unsigned int f10; //F10 key
		extern const unsigned int f11; //F11 key
		extern const unsigned int f12; //F12 key

		//arrow scancodes
		extern const unsigned int up; //up-arrow key
		extern const unsigned int down; //down-arrow key
		extern const unsigned int left; //left-arrow key
		extern const unsigned int right; //right-arrow key

		//keypad scancodes
		extern const unsigned int k0; //keypad 0
		extern const unsigned int k1; //keypad 1
		extern const unsigned int k2; //keypad 2
		extern const unsigned int k3; //keypad 3
		extern const unsigned int k4; //keypad 4
		extern const unsigned int k5; //keypad 5
		extern const unsigned int k6; //keypad 6
		extern const unsigned int k7; //keypad 7
		extern const unsigned int k8; //keypad 8
		extern const unsigned int k9; //keypad 9

		//international scancodes
		extern const unsigned int in1;
		extern const unsigned int in2;
		extern const unsigned int in3;
		extern const unsigned int in4;
		extern const unsigned int in5;
		extern const unsigned int in6;
		extern const unsigned int in7;
		extern const unsigned int in8;
		extern const unsigned int in9;
		extern const unsigned int nonusbs; //non-US backslash
		extern const unsigned int nonushash; //non-US hash

		//language scancodes
		extern const unsigned int lang1; //Hangul/English toggle
		extern const unsigned int lang2; //Hanja conversion
		extern const unsigned int lang3; //Katakana
		extern const unsigned int lang4; //Hiragana
		extern const unsigned int lang5; //Zenkaku/Hankaku
		extern const unsigned int lang6;
		extern const unsigned int lang7; 
		extern const unsigned int lang8;
		extern const unsigned int lang9;

	}
}

//end of header
