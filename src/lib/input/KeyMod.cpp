/*
 * KeyMod.cpp
 * Defines constants that describe key modifier codes
 * Created by Andrew Davis
 * Created on 6/4/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "KeyMod.h"

//constant definitions

//primary modifiers
const unsigned int CPPSDL::KeyMod::None = KMOD_NONE; //no key
const unsigned int CPPSDL::KeyMod::LShift = KMOD_LSHIFT; //left shift key
const unsigned int CPPSDL::KeyMod::RShift = KMOD_RSHIFT; //right shift key
const unsigned int CPPSDL::KeyMod::LCtrl = KMOD_LCTRL; //left control key
const unsigned int CPPSDL::KeyMod::RCtrl = KMOD_RCTRL; //right control key
const unsigned int CPPSDL::KeyMod::LAlt = KMOD_LALT; //left alt key
const unsigned int CPPSDL::KeyMod::RAlt = KMOD_RALT; //right alt key
const unsigned int CPPSDL::KeyMod::LGUI = KMOD_LGUI; //left GUI key
const unsigned int CPPSDL::KeyMod::RGUI = KMOD_RGUI; //right GUI key
const unsigned int CPPSDL::KeyMod::NumLock = KMOD_NUM; //Num Lock key
const unsigned int CPPSDL::KeyMod::CapsLock = KMOD_CAPS; //Caps Lock key
const unsigned int CPPSDL::KeyMod::AltGr = KMOD_MODE; //AltGr key

//composite modifiers
const unsigned int CPPSDL::KeyMod::Ctrl = KMOD_CTRL; //either control key
const unsigned int CPPSDL::KeyMod::Shift = KMOD_SHIFT; //either shift key
const unsigned int CPPSDL::KeyMod::Alt = KMOD_ALT; //either alt key
const unsigned int CPPSDL::KeyMod::GUI = KMOD_GUI; //either GUI key

//end of definitions
