/*
 * Button.h
 * Declares a class that represents a button for CPPSDL
 * Created by Andrew Davis
 * Created on 4/2/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include "ButtonCallback.h"
#include "../util/Rect.h"
#include "../gfx/Texture.h"
#include "../event/Event.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Button {
		//public fields and methods
		public:
			//constructor
			Button(const Rect& newBounds, 
				const ButtonCallback& newCallback, 
					const Texture& 
							newTex);

			//destructor
			~Button();

			//copy constructor
			Button(const Button& b);

			//assignment operator
			Button& operator=(const Button& src);

			//getter methods
			
			//returns the Button's texture
			const Texture& getTexture() const;

			//returns the bounds of the Button
			const Rect& getBounds() const;

			//setter methods
			
			//sets the Button's texture
			void setTexture(const Texture& newTex);

			//sets the Button's callback function
			void setCallback(const ButtonCallback&
						newCallback);

			//other method
			
			//call this when the Button is clicked
			void onClick(const Event& e);

		//private fields and methods
		private:
			//fields
			Rect bounds; //the bounds of the Button
			ButtonCallback callback; //the callback function
			Texture tex; //the texture of the Button
			
	};
}

//end of header
