/*
 * ScanCodes.cpp
 * Defines constants that represent scancodes for cppsdl
 * Created by Andrew Davis
 * Created on 3/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "ScanCodes.h"

//constant definitions

//number scancodes
const unsigned int CPPSDL::ScanCodes::zero = SDL_SCANCODE_0; //numeric 0
const unsigned int CPPSDL::ScanCodes::one = SDL_SCANCODE_1; //numeric 1
const unsigned int CPPSDL::ScanCodes::two = SDL_SCANCODE_2; //numeric 2
const unsigned int CPPSDL::ScanCodes::three = SDL_SCANCODE_3; //numeric 3
const unsigned int CPPSDL::ScanCodes::four = SDL_SCANCODE_4; //numeric 4
const unsigned int CPPSDL::ScanCodes::five = SDL_SCANCODE_5; //numeric 5
const unsigned int CPPSDL::ScanCodes::six = SDL_SCANCODE_6; //numeric 6
const unsigned int CPPSDL::ScanCodes::seven = SDL_SCANCODE_7; //numeric 7
const unsigned int CPPSDL::ScanCodes::eight = SDL_SCANCODE_8; //numeric 8
const unsigned int CPPSDL::ScanCodes::nine = SDL_SCANCODE_9; //numeric 9

//alphabet scancodes
const unsigned int CPPSDL::ScanCodes::a = SDL_SCANCODE_A; //letter A
const unsigned int CPPSDL::ScanCodes::b = SDL_SCANCODE_B; //letter B
const unsigned int CPPSDL::ScanCodes::c = SDL_SCANCODE_C; //letter C
const unsigned int CPPSDL::ScanCodes::d = SDL_SCANCODE_D; //letter D
const unsigned int CPPSDL::ScanCodes::e = SDL_SCANCODE_E; //letter E
const unsigned int CPPSDL::ScanCodes::f = SDL_SCANCODE_F; //letter F
const unsigned int CPPSDL::ScanCodes::g = SDL_SCANCODE_G; //letter G
const unsigned int CPPSDL::ScanCodes::h = SDL_SCANCODE_H; //letter H
const unsigned int CPPSDL::ScanCodes::i = SDL_SCANCODE_I; //letter I
const unsigned int CPPSDL::ScanCodes::j = SDL_SCANCODE_J; //letter J
const unsigned int CPPSDL::ScanCodes::k = SDL_SCANCODE_K; //letter K
const unsigned int CPPSDL::ScanCodes::l = SDL_SCANCODE_L; //letter L
const unsigned int CPPSDL::ScanCodes::m = SDL_SCANCODE_M; //letter M
const unsigned int CPPSDL::ScanCodes::n = SDL_SCANCODE_N; //letter N
const unsigned int CPPSDL::ScanCodes::o = SDL_SCANCODE_O; //letter O
const unsigned int CPPSDL::ScanCodes::p = SDL_SCANCODE_P; //letter P
const unsigned int CPPSDL::ScanCodes::q = SDL_SCANCODE_Q; //letter Q
const unsigned int CPPSDL::ScanCodes::r = SDL_SCANCODE_R; //letter R
const unsigned int CPPSDL::ScanCodes::s = SDL_SCANCODE_S; //letter S
const unsigned int CPPSDL::ScanCodes::t = SDL_SCANCODE_T; //letter T
const unsigned int CPPSDL::ScanCodes::u = SDL_SCANCODE_U; //letter U
const unsigned int CPPSDL::ScanCodes::v = SDL_SCANCODE_V; //letter V
const unsigned int CPPSDL::ScanCodes::w = SDL_SCANCODE_W; //letter W
const unsigned int CPPSDL::ScanCodes::x = SDL_SCANCODE_X; //letter X
const unsigned int CPPSDL::ScanCodes::y = SDL_SCANCODE_Y; //letter Y
const unsigned int CPPSDL::ScanCodes::z = SDL_SCANCODE_Z; //letter Z

//utility scancodes
const unsigned int CPPSDL::ScanCodes::context =
	 SDL_SCANCODE_APPLICATION; //Windows key
const unsigned int CPPSDL::ScanCodes::amute =
	 SDL_SCANCODE_AUDIOMUTE; //audio mute key
const unsigned int CPPSDL::ScanCodes::next =
	 SDL_SCANCODE_AUDIONEXT; //next track key
const unsigned int CPPSDL::ScanCodes::play =
	 SDL_SCANCODE_AUDIOPLAY; //play key
const unsigned int CPPSDL::ScanCodes::prev =
	 SDL_SCANCODE_AUDIOPREV; //prev track key
const unsigned int CPPSDL::ScanCodes::stop =
	 SDL_SCANCODE_AUDIOSTOP; //stop key
const unsigned int CPPSDL::ScanCodes::brightdown =
	 SDL_SCANCODE_BRIGHTNESSDOWN;
const unsigned int CPPSDL::ScanCodes::brightup =
	 SDL_SCANCODE_BRIGHTNESSUP;
const unsigned int CPPSDL::ScanCodes::capslock =
	 SDL_SCANCODE_CAPSLOCK; //caps lock key
const unsigned int CPPSDL::ScanCodes::dswitch =
	 SDL_SCANCODE_DISPLAYSWITCH;
const unsigned int CPPSDL::ScanCodes::eject =
	 SDL_SCANCODE_EJECT; //eject key
const unsigned int CPPSDL::ScanCodes::insert =
	 SDL_SCANCODE_INSERT; //insert key
const unsigned int CPPSDL::ScanCodes::mute =
	 SDL_SCANCODE_MUTE; //mute key
const unsigned int CPPSDL::ScanCodes::pgdown =
	 SDL_SCANCODE_PAGEDOWN; //page down key
const unsigned int CPPSDL::ScanCodes::pgup =
	 SDL_SCANCODE_PAGEUP; //page up key
const unsigned int CPPSDL::ScanCodes::pause =
	 SDL_SCANCODE_PAUSE; //pause key
const unsigned int CPPSDL::ScanCodes::space =
	 SDL_SCANCODE_SPACE; //spacebar
const unsigned int CPPSDL::ScanCodes::tab =
	 SDL_SCANCODE_TAB; //tab key
const unsigned int CPPSDL::ScanCodes::voldown =
	 SDL_SCANCODE_VOLUMEDOWN; //volume down
const unsigned int CPPSDL::ScanCodes::volup =
	 SDL_SCANCODE_VOLUMEUP; //volume up

//symbol scancodes
const unsigned int CPPSDL::ScanCodes::quote =
	 SDL_SCANCODE_APOSTROPHE; //quote key
const unsigned int CPPSDL::ScanCodes::backslash =
	 SDL_SCANCODE_BACKSLASH; //backslash
const unsigned int CPPSDL::ScanCodes::comma =
	 SDL_SCANCODE_COMMA; //comma
const unsigned int CPPSDL::ScanCodes::equals =
	 SDL_SCANCODE_EQUALS; //equals sign
const unsigned int CPPSDL::ScanCodes::bquote =
	 SDL_SCANCODE_GRAVE; //backquote
const unsigned int CPPSDL::ScanCodes::lbckt =
	 SDL_SCANCODE_LEFTBRACKET; //left bracket
const unsigned int CPPSDL::ScanCodes::minus =
	 SDL_SCANCODE_MINUS; //minus sign
const unsigned int CPPSDL::ScanCodes::period =
	 SDL_SCANCODE_PERIOD; //period key
const unsigned int CPPSDL::ScanCodes::rbckt =
	 SDL_SCANCODE_RIGHTBRACKET; //right bracket
const unsigned int CPPSDL::ScanCodes::scolon =
	 SDL_SCANCODE_SEMICOLON; //semicolon
const unsigned int CPPSDL::ScanCodes::slash =
	 SDL_SCANCODE_SLASH; //forward slash

//abstract scancodes
const unsigned int CPPSDL::ScanCodes::backspace =
	 SDL_SCANCODE_BACKSPACE; //backspace
const unsigned int CPPSDL::ScanCodes::copy =
	 SDL_SCANCODE_COPY; //copy command
const unsigned int CPPSDL::ScanCodes::cut =
	 SDL_SCANCODE_CUT; //cut command
const unsigned int CPPSDL::ScanCodes::del =
	 SDL_SCANCODE_DELETE; //delete key
const unsigned int CPPSDL::ScanCodes::esc =
	 SDL_SCANCODE_ESCAPE; //escape key
const unsigned int CPPSDL::ScanCodes::lalt =
	 SDL_SCANCODE_LALT; //left alt key
const unsigned int CPPSDL::ScanCodes::lctrl =
	 SDL_SCANCODE_LCTRL; //left control key
const unsigned int CPPSDL::ScanCodes::lgui =
	 SDL_SCANCODE_LGUI; //left alt context key
const unsigned int CPPSDL::ScanCodes::lshift =
	 SDL_SCANCODE_LSHIFT; //left shift key
const unsigned int CPPSDL::ScanCodes::numlock =
	 SDL_SCANCODE_NUMLOCKCLEAR; //numlock key
const unsigned int CPPSDL::ScanCodes::paste =
	 SDL_SCANCODE_PASTE; //paste command
const unsigned int CPPSDL::ScanCodes::prtscr =
	 SDL_SCANCODE_PRINTSCREEN; //print screen
const unsigned int CPPSDL::ScanCodes::ralt =
	 SDL_SCANCODE_RALT; //right alt key
const unsigned int CPPSDL::ScanCodes::rctrl =
	 SDL_SCANCODE_RCTRL; //right control key
const unsigned int CPPSDL::ScanCodes::ret =
	 SDL_SCANCODE_RETURN; //return key
const unsigned int CPPSDL::ScanCodes::rgui =
	 SDL_SCANCODE_RGUI; //right alt context key
const unsigned int CPPSDL::ScanCodes::rshift =
	 SDL_SCANCODE_RSHIFT; //right shift key
const unsigned int CPPSDL::ScanCodes::scrlock =
	 SDL_SCANCODE_SCROLLLOCK; //scroll lock
const unsigned int CPPSDL::ScanCodes::undo =
	SDL_SCANCODE_UNDO; //undo command

//function scancodes
const unsigned int CPPSDL::ScanCodes::f1 = SDL_SCANCODE_F1; //F1 key
const unsigned int CPPSDL::ScanCodes::f2 = SDL_SCANCODE_F2; //F2 key
const unsigned int CPPSDL::ScanCodes::f3 = SDL_SCANCODE_F3; //F3 key
const unsigned int CPPSDL::ScanCodes::f4 = SDL_SCANCODE_F4; //F4 key
const unsigned int CPPSDL::ScanCodes::f5 = SDL_SCANCODE_F5; //F5 key
const unsigned int CPPSDL::ScanCodes::f6 = SDL_SCANCODE_F6; //F6 key
const unsigned int CPPSDL::ScanCodes::f7 = SDL_SCANCODE_F7; //F7 key
const unsigned int CPPSDL::ScanCodes::f8 = SDL_SCANCODE_F8; //F8 key
const unsigned int CPPSDL::ScanCodes::f9 = SDL_SCANCODE_F9; //F9 key
const unsigned int CPPSDL::ScanCodes::f10 = SDL_SCANCODE_F10; //F10 key
const unsigned int CPPSDL::ScanCodes::f11 = SDL_SCANCODE_F11; //F11 key
const unsigned int CPPSDL::ScanCodes::f12 = SDL_SCANCODE_F12; //F12 key

//arrow scancodes
const unsigned int CPPSDL::ScanCodes::up = SDL_SCANCODE_UP; //up arrow
const unsigned int CPPSDL::ScanCodes::down = SDL_SCANCODE_DOWN; //down arrow
const unsigned int CPPSDL::ScanCodes::left = SDL_SCANCODE_LEFT; //left arrow
const unsigned int CPPSDL::ScanCodes::right = 
	SDL_SCANCODE_RIGHT; //right arrow

//keypad scancodes
const unsigned int CPPSDL::ScanCodes::k0 = SDL_SCANCODE_KP_0; //keypad 0
const unsigned int CPPSDL::ScanCodes::k1 = SDL_SCANCODE_KP_1; //keypad 1
const unsigned int CPPSDL::ScanCodes::k2 = SDL_SCANCODE_KP_2; //keypad 2
const unsigned int CPPSDL::ScanCodes::k3 = SDL_SCANCODE_KP_3; //keypad 3
const unsigned int CPPSDL::ScanCodes::k4 = SDL_SCANCODE_KP_4; //keypad 4
const unsigned int CPPSDL::ScanCodes::k5 = SDL_SCANCODE_KP_5; //keypad 5
const unsigned int CPPSDL::ScanCodes::k6 = SDL_SCANCODE_KP_6; //keypad 6
const unsigned int CPPSDL::ScanCodes::k7 = SDL_SCANCODE_KP_7; //keypad 7
const unsigned int CPPSDL::ScanCodes::k8 = SDL_SCANCODE_KP_8; //keypad 8
const unsigned int CPPSDL::ScanCodes::k9 = SDL_SCANCODE_KP_9; //keypad 9

//international scancodes
const unsigned int CPPSDL::ScanCodes::in1 = SDL_SCANCODE_INTERNATIONAL1;
const unsigned int CPPSDL::ScanCodes::in2 = SDL_SCANCODE_INTERNATIONAL2;
const unsigned int CPPSDL::ScanCodes::in3 = SDL_SCANCODE_INTERNATIONAL3;
const unsigned int CPPSDL::ScanCodes::in4 = SDL_SCANCODE_INTERNATIONAL4;
const unsigned int CPPSDL::ScanCodes::in5 = SDL_SCANCODE_INTERNATIONAL5;
const unsigned int CPPSDL::ScanCodes::in6 = SDL_SCANCODE_INTERNATIONAL6;
const unsigned int CPPSDL::ScanCodes::in7 = SDL_SCANCODE_INTERNATIONAL7;
const unsigned int CPPSDL::ScanCodes::in8 = SDL_SCANCODE_INTERNATIONAL8;
const unsigned int CPPSDL::ScanCodes::in9 = SDL_SCANCODE_INTERNATIONAL9;
const unsigned int CPPSDL::ScanCodes::nonusbs =
	SDL_SCANCODE_NONUSBACKSLASH; //non-US backslash
const unsigned int CPPSDL::ScanCodes::nonushash =
	SDL_SCANCODE_NONUSHASH; //non-US hash

//language scancodes
const unsigned int CPPSDL::ScanCodes::lang1 = 
	SDL_SCANCODE_LANG1; //Hangul/English toggle
const unsigned int CPPSDL::ScanCodes::lang2 =
	SDL_SCANCODE_LANG2; //Hanja conversion
const unsigned int CPPSDL::ScanCodes::lang3 =
	SDL_SCANCODE_LANG3; //Katakana
const unsigned int CPPSDL::ScanCodes::lang4 = 
	SDL_SCANCODE_LANG4; //Hiragana 
const unsigned int CPPSDL::ScanCodes::lang5 =
	SDL_SCANCODE_LANG2; //Zenkaku/Hankaku
const unsigned int CPPSDL::ScanCodes::lang6 = SDL_SCANCODE_LANG6;
const unsigned int CPPSDL::ScanCodes::lang7 = SDL_SCANCODE_LANG7;
const unsigned int CPPSDL::ScanCodes::lang8 = SDL_SCANCODE_LANG8;
const unsigned int CPPSDL::ScanCodes::lang9 = SDL_SCANCODE_LANG9; 

//end of definitions
