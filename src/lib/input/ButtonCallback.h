/*
 * ButtonCallback.h
 * Defines a type for button callback functions
 * Created by Andrew Davis
 * Created on 5/21/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <functional>
#include "../event/Event.h"

//namespace declaration
namespace CPPSDL {
	//forward declaration of the Button class
	class Button;

	//typedef definition
	typedef std::function<void(Button&, 
				const Event&)> ButtonCallback;
		
}

//end of header
