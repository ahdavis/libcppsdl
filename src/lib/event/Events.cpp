/*
 * Events.cpp
 * Defines constants that represent events for CPPSDL
 * Created by Andrew Davis
 * Created on 3/9/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Events.h"

//constant definitions

//application Events
const unsigned int CPPSDL::Events::Quit = SDL_QUIT; //user requests a quit

//Android, iOS, and WinRT Events
const unsigned int CPPSDL::Events::Terminate = SDL_APP_TERMINATING;
const unsigned int CPPSDL::Events::LowMem = SDL_APP_LOWMEMORY;
const unsigned int CPPSDL::Events::WillBack = SDL_APP_WILLENTERBACKGROUND;
const unsigned int CPPSDL::Events::DidBack = SDL_APP_DIDENTERBACKGROUND;
const unsigned int CPPSDL::Events::WillFore = SDL_APP_WILLENTERFOREGROUND;
const unsigned int CPPSDL::Events::DidFore = SDL_APP_DIDENTERFOREGROUND;

//window event
const unsigned int CPPSDL::Events::WindowEvent = SDL_WINDOWEVENT;

//keyboard Events
const unsigned int CPPSDL::Events::KeyDown = SDL_KEYDOWN; //key pressed
const unsigned int CPPSDL::Events::KeyUp = SDL_KEYUP; //key released
const unsigned int CPPSDL::Events::TextEdit = SDL_TEXTEDITING; 
const unsigned int CPPSDL::Events::TextInput = SDL_TEXTINPUT;
const unsigned int CPPSDL::Events::KeyMapChg = SDL_KEYMAPCHANGED;

//mouse Events
const unsigned int CPPSDL::Events::MouseMoved = SDL_MOUSEMOTION;
const unsigned int CPPSDL::Events::MouseDown = SDL_MOUSEBUTTONDOWN;
const unsigned int CPPSDL::Events::MouseUp = SDL_MOUSEBUTTONUP;
const unsigned int CPPSDL::Events::MouseWheel = SDL_MOUSEWHEEL;

//joystick Events
const unsigned int CPPSDL::Events::JoyAxis = SDL_JOYAXISMOTION;
const unsigned int CPPSDL::Events::JoyBall = SDL_JOYBALLMOTION;
const unsigned int CPPSDL::Events::JoyHat = SDL_JOYHATMOTION;
const unsigned int CPPSDL::Events::JoyDown = SDL_JOYBUTTONDOWN;
const unsigned int CPPSDL::Events::JoyUp = SDL_JOYBUTTONUP;
const unsigned int CPPSDL::Events::JoyAdd = SDL_JOYDEVICEADDED;
const unsigned int CPPSDL::Events::JoyRM = SDL_JOYDEVICEREMOVED;

//controller Events
const unsigned int CPPSDL::Events::CAxis = SDL_CONTROLLERAXISMOTION;
const unsigned int CPPSDL::Events::CDown = SDL_CONTROLLERBUTTONDOWN;
const unsigned int CPPSDL::Events::CUp = SDL_CONTROLLERBUTTONUP;
const unsigned int CPPSDL::Events::CAdd = SDL_CONTROLLERDEVICEADDED;
const unsigned int CPPSDL::Events::CRM = SDL_CONTROLLERDEVICEREMOVED;
const unsigned int CPPSDL::Events::CReMap = SDL_CONTROLLERDEVICEREMAPPED;

//touch Events
const unsigned int CPPSDL::Events::FingerDown = SDL_FINGERDOWN;
const unsigned int CPPSDL::Events::FingerUp = SDL_FINGERUP;
const unsigned int CPPSDL::Events::FingerMotion = SDL_FINGERMOTION;

//gesture Events
const unsigned int CPPSDL::Events::DollarGest = SDL_DOLLARGESTURE;
const unsigned int CPPSDL::Events::DollarRec = SDL_DOLLARRECORD;
const unsigned int CPPSDL::Events::MultiGest = SDL_MULTIGESTURE;

//clipboard event
const unsigned int CPPSDL::Events::CBoardUpdate = SDL_CLIPBOARDUPDATE;

//drag and drop Events
const unsigned int CPPSDL::Events::DropFile = SDL_DROPFILE;
const unsigned int CPPSDL::Events::DropText = SDL_DROPTEXT;
const unsigned int CPPSDL::Events::DropBegin = SDL_DROPBEGIN;
const unsigned int CPPSDL::Events::DropComp = SDL_DROPCOMPLETE;

//audio hotplug Events
const unsigned int CPPSDL::Events::AudioAdd = SDL_AUDIODEVICEADDED;
const unsigned int CPPSDL::Events::AudioRM = SDL_AUDIODEVICEREMOVED;

//render Events
const unsigned int CPPSDL::Events::RTargRes = SDL_RENDER_TARGETS_RESET;
const unsigned int CPPSDL::Events::RDevRes = SDL_RENDER_DEVICE_RESET;

//end of definitions
