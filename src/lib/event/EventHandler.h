/*
 * EventHandler.h
 * Declares a class that handles events for CPPSDL
 * Created by Andrew Davis
 * Created on 3/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include "Event.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class EventHandler {
		//public fields and methods
		public:
			//constructor
			EventHandler();

			//destructor
			virtual ~EventHandler();

			//copy constructor
			EventHandler(const EventHandler& eh);

			//assignment operator
			EventHandler& operator=(const EventHandler& src);

			//getter method

			//returns the event in the handler
			const Event& getEvent() const;

			//other method
			
			//polls the next event in the queue
			int pollEvent();

		//private fields and methods
		private:
			//field
			Event evnt; //the event being handled
			
	};
}

//end of header
