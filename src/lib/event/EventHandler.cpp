/*
 * EventHandler.cpp
 * Implements a class that handles events for CPPSDL
 * Created by Andrew Davis
 * Created on 3/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "EventHandler.h"

//constructor
CPPSDL::EventHandler::EventHandler()
	: evnt() //init the event field
{
	//no code needed
}

//destructor
CPPSDL::EventHandler::~EventHandler() {
	//no code needed
}

//copy constructor
CPPSDL::EventHandler::EventHandler(const CPPSDL::EventHandler& eh)
	: evnt(eh.evnt) //copy the event field
{
	//no code needed
}

//assignment operator
CPPSDL::EventHandler& CPPSDL::EventHandler::operator=(const
						CPPSDL::EventHandler& 
							src) {
	this->evnt = src.evnt; //assign the event field
	return *this; //and return the instance
}

//getEvent method - returns the event being handled
const CPPSDL::Event& CPPSDL::EventHandler::getEvent() const {
	return this->evnt; //return the event field
}

//pollEvent method - polls the next event in the queue
int CPPSDL::EventHandler::pollEvent() {
	return SDL_PollEvent(&this->evnt.evnt); //poll the next event
}

//end of implementation
