/*
 * Events.h
 * Declares constants that represent events for CPPSDL
 * Created by Andrew Davis
 * Created on 3/9/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//include
#include <SDL2/SDL.h>

//global namespace declaration
namespace CPPSDL {
	//local namespace declaration
	namespace Events {
		//event constant declarations
		
		//application Events
		extern const unsigned int Quit; //user requests a quit

		//Android, iOS and WinRT Events
		extern const unsigned int Terminate; //app quit by OS
		extern const unsigned int LowMem; //OS is low on memory
		extern const unsigned int WillBack; //app is entering BG
		extern const unsigned int DidBack; //app entered BG
		extern const unsigned int WillFore; //app is entering FG
		extern const unsigned int DidFore; //app entered FG

		//window event
		extern const unsigned int WindowEvent; //window state change

		//keyboard Events
		extern const unsigned int KeyDown; //key pressed
		extern const unsigned int KeyUp; //key released
		extern const unsigned int TextEdit; //keyboard text editing
		extern const unsigned int TextInput; //keyboard text input
		extern const unsigned int KeyMapChg; //keymap changed

		//mouse Events
		extern const unsigned int MouseMoved; //mouse moved
		extern const unsigned int MouseDown; //mouse button pressed
		extern const unsigned int MouseUp; //mouse button released
		extern const unsigned int MouseWheel; //mouse wheel moved

		//joystick Events
		extern const unsigned int JoyAxis; //joystick axis motion
		extern const unsigned int JoyBall; //joystick ball motion
		extern const unsigned int JoyHat; //joystick hat pos change
		extern const unsigned int JoyDown; //joystick button press
		extern const unsigned int JoyUp; //joystick button release
		extern const unsigned int JoyAdd; //joystick connected
		extern const unsigned int JoyRM; //joystick disconnected

		//controller Events
		extern const unsigned int CAxis; //controller axis motion
		extern const unsigned int CDown; //controller button down
		extern const unsigned int CUp; //controller button up
		extern const unsigned int CAdd; //controller connected
		extern const unsigned int CRM; //controller disconnected
		extern const unsigned int CReMap; //controller remapped

		//touch Events
		extern const unsigned int FingerDown; //screen touched
		extern const unsigned int FingerUp; //screen released
		extern const unsigned int FingerMotion; //screen dragged

		//gesture Events
		extern const unsigned int DollarGest; //dollar gesture
		extern const unsigned int DollarRec; //dollar record
		extern const unsigned int MultiGest; //multigesture

		//clipboard event
		extern const unsigned int CBoardUpdate; //clipboard update

		//drag and drop Events
		extern const unsigned int DropFile; //file open request
		extern const unsigned int DropText; //text drag-and-drop
		extern const unsigned int DropBegin; //new drop set
		extern const unsigned int DropComp; //current drops complete
		
		//audio hotplug Events
		extern const unsigned int AudioAdd; //audio device added
		extern const unsigned int AudioRM; //audio device removed

		//render Events
		extern const unsigned int RTargRes; //render targets reset
		extern const unsigned int RDevRes; //render device reset
	}
}

//end of header
