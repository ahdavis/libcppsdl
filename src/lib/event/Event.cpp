/*
 * Event.cpp
 * Implements a class that represents an SDL Event
 * Created by Andrew Davis
 * Created on 3/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "Event.h"

//constructor
CPPSDL::Event::Event()
	: evnt() //init the Event
{
	//no code needed
}

//destructor
CPPSDL::Event::~Event() {
	//no code needed
}

//copy constructor
CPPSDL::Event::Event(const CPPSDL::Event& e)
	: evnt(e.evnt) //copy the Event field
{
	//no code needed
}

//assignment operator
CPPSDL::Event& CPPSDL::Event::operator=(const CPPSDL::Event& src) {
	this->evnt = src.evnt; //assign the Event field
	return *this; //and return the instance
}

//getType method - returns the type of the Event
unsigned int CPPSDL::Event::getType() const {
	return this->evnt.type; //return the Event's type field
}

//getKey method - returns the keypress associated with the Event
unsigned int CPPSDL::Event::getKey() const {
	return this->evnt.key.keysym.sym; //return the Event's keypress
}

//getKeyRepeat method - returns the key repeat value
int CPPSDL::Event::getKeyRepeat() const {
	return this->evnt.key.repeat;
}

//getJoyAxis method - returns the joystick axis
unsigned int CPPSDL::Event::getJoyAxis() const {
	return this->evnt.jaxis.axis; //return the joystick axis
}

//getJoyID method - returns the joystick ID
unsigned int CPPSDL::Event::getJoyID() const {
	return this->evnt.jaxis.which; //return the joystick ID
}

//getJoyValue method - returns the value of the joystick
int CPPSDL::Event::getJoyValue() const {
	return this->evnt.jaxis.value; //return the joystick value
}

//getJoyButton method - returns the index of the joystick button
//currently being pressed
unsigned int CPPSDL::Event::getJoyButton() const {
	return this->evnt.jbutton.button; //return the button index
}

//getText method - returns the text entered from a text event
std::string CPPSDL::Event::getText() const {
	return std::string(this->evnt.text.text); //return the text
}

//getKeyMod method - returns the key modulation code from a keydown event
std::uint16_t CPPSDL::Event::getKeyMod() const {
	return this->evnt.key.keysym.mod;
}

//getWindowEvent method - returns the data for a window event
std::uint8_t CPPSDL::Event::getWindowEvent() const {
	return this->evnt.window.event; //return the window event
}

//getWindowResizeWidth method - returns the resize width
//from a window event
int CPPSDL::Event::getWindowResizeWidth() const {
	return this->evnt.window.data1; //return the resize width
}

//getWindowResizeHeight method - returns the resize height
//from a window event
int CPPSDL::Event::getWindowResizeHeight() const {
	return this->evnt.window.data2; //return the resize height
}

//getWindowID method - returns the window ID for a window event
std::uint32_t CPPSDL::Event::getWindowID() const {
	return this->evnt.window.windowID; //return the window ID
}

//end of implementation
