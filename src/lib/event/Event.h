/*
 * Event.h
 * Declares a class that represents an SDL Event
 * Created by Andrew Davis
 * Created on 3/12/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <string>
#include <cstdint>

//namespace declaration
namespace CPPSDL {
	//class declaration
	class Event final {
		//public fields and methods
		public:
			//constructor
			Event();

			//destructor
			~Event();

			//copy constructor
			Event(const Event& e);

			//assignment operator
			Event& operator=(const Event& src);

			//getter methods
			
			//returns the type of the Event
			unsigned int getType() const;

			//returns the keypress for a keypress Event
			unsigned int getKey() const;

			//returns the key repeat value
			int getKeyRepeat() const;

			//returns the joystick axis
			unsigned int getJoyAxis() const;

			//returns the ID of the joystick
			unsigned int getJoyID() const;

			//returns the value of the joystick
			int getJoyValue() const;

			//returns the index of the button being pressed
			//on the joystick
			unsigned int getJoyButton() const;

			//returns the text entered from a text event
			std::string getText() const;

			//returns the key modifier code from a key event
			std::uint16_t getKeyMod() const;

			//returns the data for a window event
			std::uint8_t getWindowEvent() const;

			//returns the resize width from a window event
			int getWindowResizeWidth() const;

			//returns the resize height from a window event
			int getWindowResizeHeight() const;

			//returns the window ID for a window event
			std::uint32_t getWindowID() const;

		//private fields and methods
		private:
			//friendship declaration
			friend class EventHandler;

			//field
			SDL_Event evnt; //the SDL Event instance
	};
}

//end of header
