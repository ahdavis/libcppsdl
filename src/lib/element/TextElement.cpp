/*
 * TextElement.cpp
 * Implements a class that represents a text element for CPPSDL
 * Created by Andrew Davis
 * Created on 4/1/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "TextElement.h"

//constructor
CPPSDL::TextElement::TextElement(const CPPSDL::Font& f,
					const std::string& newText,
					const CPPSDL::Color& textColor)
	: surf(f, newText, textColor), text(newText), 
		fontSize(f.getSize())
{
	//no code needed
}

//destructor
CPPSDL::TextElement::~TextElement() {
	//no code needed
}

//copy constructor
CPPSDL::TextElement::TextElement(const CPPSDL::TextElement& te)
	: surf(te.surf), text(te.text), fontSize(te.fontSize)
{
	//no code needed
}

//assignment operator
CPPSDL::TextElement& CPPSDL::TextElement::operator=(const 
						CPPSDL::TextElement&
							src) {
	this->surf = src.surf; //assign the surface
	this->text = src.text; //assign the text
	this->fontSize = src.fontSize; //assign the font size
	return *this; //and return the instance
}

//getSurface method - returns the element's surface
const CPPSDL::Surface& CPPSDL::TextElement::getSurface() const {
	return this->surf; //return the surface field
}

//getText method - returns the element's text
const std::string& CPPSDL::TextElement::getText() const {
	return this->text; //return the text field
}

//getFontSize method - returns the element's font size
int CPPSDL::TextElement::getFontSize() const {
	return this->fontSize; //return the font size field
}

//getWidth method - returns the width of the element
int CPPSDL::TextElement::getWidth() const {
	return this->surf.getWidth(); //return the surface's width
}

//getHeight method - returns the height of the element
int CPPSDL::TextElement::getHeight() const {
	return this->surf.getHeight(); //return the surface's height
}

//end of implementation
