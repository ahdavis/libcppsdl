/*
 * TextElement.h
 * Declares a class that represents a text element for CPPSDL
 * Created by Andrew Davis
 * Created on 4/1/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <string>
#include "../win/Surface.h"
#include "../color/Color.h"
#include "../util/Font.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class TextElement {
		//public fields and methods
		public:
			//constructor
			TextElement(const Font& f,
					const std::string& newText,
					const Color& textColor);

			//destructor
			virtual ~TextElement();

			//copy constructor
			TextElement(const TextElement& te);

			//assignment operator
			TextElement& operator=(const TextElement& src);

			//getter methods
			
			//returns the element's surface
			const Surface& getSurface() const;

			//returns the text as a string
			const std::string& getText() const;

			//returns the font size of the text
			int getFontSize() const;

			//returns the width of the text
			int getWidth() const;

			//returns the height of the text
			int getHeight() const;

		//private fields and methods
		private:
			//fields
			Surface surf; //the surface that contains the text
			std::string text; //the element's text
			int fontSize; //the size of the text
	};
}

//end of header
