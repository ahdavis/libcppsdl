/*
 * ImgElement.cpp
 * Implements a class that represents an image element for CPPSDL
 * Created by Andrew Davis
 * Created on 3/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include header
#include "ImgElement.h"

//constructor
CPPSDL::ImgElement::ImgElement(const std::string& newPath)
	: surf(newPath), path(newPath) //init the fields
{
	//no code needed
}

//destructor
CPPSDL::ImgElement::~ImgElement() {
	//no code needed
}

//copy constructor
CPPSDL::ImgElement::ImgElement(const CPPSDL::ImgElement& ie)
	: surf(ie.surf), path(ie.path) //copy the fields
{
	//no code needed
}

//assignment operator
CPPSDL::ImgElement& CPPSDL::ImgElement::operator=(const
						CPPSDL::ImgElement& src) {
	this->surf = src.surf; //assign the surface field
	this->path = src.path; //assign the path field
	return *this; //and return the instance
}

//getWidth method - returns the width of the image
int CPPSDL::ImgElement::getWidth() const {
	return this->surf.getWidth(); //return the surface's width
}

//getHeight method - returns the height of the image
int CPPSDL::ImgElement::getHeight() const {
	return this->surf.getHeight(); //return the surface's height
}

//getSurface method - returns the surface holding the image
const CPPSDL::Surface& CPPSDL::ImgElement::getSurface() const {
	return this->surf; //return the surface field
}

//getPath method - returns the path to the image
const std::string& CPPSDL::ImgElement::getPath() const {
	return this->path; //return the path field
}

//setColorKey method - sets the color key for the image
void CPPSDL::ImgElement::setColorKey(const CPPSDL::Color& col, 
					bool enable) {
	//convert the bool parameter to SDL format
	auto senable = enable ? SDL_TRUE : SDL_FALSE;

	//set the color key for the image
	SDL_SetColorKey(this->surf.data.get(), senable, 
				SDL_MapRGB(this->surf.data->format,
						col.getRed(), 
						col.getGreen(), 
						col.getBlue()));
}

//end of implementation
