/*
 * ImgElement.h
 * Declares a class that represents an image element for cppsdl
 * Created by Andrew Davis
 * Created on 3/8/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//include guard
#pragma once

//includes
#include <SDL2/SDL.h>
#include <string>
#include "../win/Surface.h"
#include "../color/Color.h"

//namespace declaration
namespace CPPSDL {
	//class declaration
	class ImgElement {
		//public fields and methods
		public:
			//constructor
			explicit ImgElement(const std::string& newPath);

			//destructor
			virtual ~ImgElement();

			//copy constructor
			ImgElement(const ImgElement& ie);

			//assignment operator
			ImgElement& operator=(const ImgElement& src);

			//getter methods

			//returns the width of the image
			int getWidth() const;

			//returns the height of the image
			int getHeight() const;

			//returns the image element's surface
			const Surface& getSurface() const;

			//returns the path to the element's image
			const std::string& getPath() const;

			//other methods
		
			//sets the color key for the image
			void setColorKey(const Color& col, bool enable);
			
		//private fields and methods
		private:
			//friendship declaration
			friend class Surface;

			//fields
			Surface surf; //the surface that holds the image
			std::string path; //the path to the image
	};
}

//end of header
