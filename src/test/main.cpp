/*
 * main.cpp
 * Main code file for the test program for the cppsdl library
 * Created by Andrew Davis
 * Created on 3/7/2018
 *
 * Copyright 2018 Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License version 3
 */

//includes
#include <cstdlib>
#include <string>
#include <sstream>
#include "../lib/cppsdl.h"

//constants
const int SCREEN_WIDTH = 600; //the width of the window
const int SCREEN_HEIGHT = 400; //the height of the window

//global variables
int modData = -1; //the data to modify
CPPSDL::Mutex mutex; //regulates data access
CPPSDL::Condition canConsume; //can the data be cleared?
CPPSDL::Condition canProduce; //can the data be populated?

//function prototypes

//first thread callback - populates the global data
int producer(void* data);

//second thread callback - clears the global data
int consumer(void* data);

//actually carries out the data population
void produce();

//actual carries out the data clearing
void consume();

//main function - main entry point for the program
int main(int argc, char* argv[]) {
	//init cppsdl
	CPPSDL::Init();

	//create a window
	CPPSDL::Window win("Mutex/Condition Test", SCREEN_WIDTH,
				SCREEN_HEIGHT, true);

	//create a renderer
	CPPSDL::Renderer rend(win);

	//get the bundle path
	std::string bundlePath = CPPSDL::GetBasePath();
	bundlePath += "../src/test/assets/demo.bundle";

	//load the bundle from the path
	CPPSDL::Bundle bundle;
	bundle.deserialize(bundlePath);

	//create an image element to render
	std::string imgPath = bundle.assetForName("splash.png").getPath();
	CPPSDL::ImgElement img(imgPath);

	//create a texture from the image
	CPPSDL::Texture tex(rend, img);

	//seed the RNG
	std::srand(CPPSDL::GetTicks());

	//log the base path
	CPPSDL::LogMessage(CPPSDL::GetBasePath());

	//get two threads to modify the data with
	CPPSDL::Thread workerOne(producer, "Producer", "Producer");
	CPPSDL::Thread workerTwo(consumer, "Consumer", "Consumer");

	//start the threads with a random delay between them
	workerOne.start();
	CPPSDL::Delay(16 + std::rand() % 32);
	workerTwo.start();

	//create an event handler
	CPPSDL::EventHandler eh;

	//create a sentinel variable
	bool quit = false;

	//main loop
	while(!quit) {
		//loop and process events
		while(eh.pollEvent() != 0) {
			//handle events
			auto event = eh.getEvent();
			auto type = event.getType();

			if(type == CPPSDL::Events::Quit) { //quit 
				quit = true;	
			}
		}

		//clear the screen
		rend.setDrawColor(CPPSDL::Color(0xFF, 0xFF, 0xFF));
		rend.clear();

		//render the splash texture
		rend.renderTexture(tex, 0, 0);

		//update the screen
		rend.update();

	}

	//stop the worker threads
	workerOne.wait();
	workerTwo.wait();

	//serialize the bundle
	bundle.serialize(bundlePath);

	//and return with no errors
	return EXIT_SUCCESS;
}

//producer function - populates the global data
int producer(void* data) {
	//log that the thread started
	CPPSDL::LogMessage("Producer started...");

	//seed the RNG
	std::srand(CPPSDL::GetTicks());

	//produce 5 times
	for(int i = 0; i < 5; i++) {
		//wait randomly
		CPPSDL::Delay(std::rand() % 1000);

		//populate the data
		produce();
		
	}

	//log that the thread finished
	CPPSDL::LogMessage("Producer finished!");

	//and return 0
	return 0;
}

//consumer function - clears the global data
int consumer(void* data) {
	//log that the thread started
	CPPSDL::LogMessage("Consumer started...");

	//seed the RNG
	std::srand(CPPSDL::GetTicks());

	//consume 5 times
	for(int i = 0; i < 5; i++) {
		//wait randomly
		CPPSDL::Delay(std::rand() % 1000);

		//clear the data
		consume();
	}

	//log that the thread finished
	CPPSDL::LogMessage("Consumer finished!");

	//and return 0
	return 0;
}

//produce function - actually populates the global data
void produce() {
	//lock the mutex
	mutex.lock();

	//handle already populated data
	if(modData != -1) { //if the data is populated
		//log that the data is populated
		CPPSDL::LogMessage("Data already populated!");

		//and wait for a signal
		canProduce.wait(mutex);
	}

	//populate and show the data
	modData = std::rand() % 255;
	CPPSDL::LogMessage(std::string("Data is now ") +
				std::to_string(modData));

	//unlock the mutex
	mutex.unlock();

	//and signal the consumer thread
	CPPSDL::Condition::signal(canConsume);
}

//consume function - actually clears the data
void consume() {
	//lock the mutex
	mutex.lock();

	//handle already cleared data
	if(modData == -1) { //if the data is already cleared
		//log that the data was cleared
		CPPSDL::LogMessage("Data already cleared!");

		//and wait for a signal
		canConsume.wait(mutex);
	}

	//show and clear the data
	CPPSDL::LogMessage(std::string("Cleared ") + 
				std::to_string(modData));
	modData = -1;

	//unlock the mutex
	mutex.unlock();

	//and signal the producer thread
	CPPSDL::Condition::signal(canProduce);
}

//end of program
