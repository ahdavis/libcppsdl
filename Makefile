# Makefile for cppsdl
# Compiles the code for the library
# Created on 3/7/2017
# Created by Andrew Davis
#
# Copyright (C) 2017  Andrew Davis
#
# Licensed under the Lesser GNU General Public License version 3

# define the compiler
CXX=g++

# define the compiler flags for the library
CXXFLAGS=-fPIC -c -Wall -std=c++17

# define the compiler flags for the test program
test: CXXFLAGS=-c -Wall -std=c++17 -g

# define linker flags for the library
LDFLAGS=-shared -lSDL2 -lSDL2_image -lSDL2_ttf -lSDL2_mixer -lm

# define linker flags for the test program
TFLAGS=-lm -L./lib -Wl,-rpath,./lib -lcppsdl 

# retrieve source code for the library
LEXCE=$(shell ls src/lib/except/*.cpp)
LWIN=$(shell ls src/lib/win/*.cpp)
LUTIL=$(shell ls src/lib/util/*.cpp)
LELEM=$(shell ls src/lib/element/*.cpp)
LEVNT=$(shell ls src/lib/event/*.cpp)
LINPT=$(shell ls src/lib/input/*.cpp)
LGFX=$(shell ls src/lib/gfx/*.cpp)
LCOL=$(shell ls src/lib/color/*.cpp)
LSND=$(shell ls src/lib/sound/*.cpp)
LGUI=$(shell ls src/lib/gui/*.cpp)
LTIME=$(shell ls src/lib/time/*.cpp)
LFILE=$(shell ls src/lib/file/*.cpp)
LTHRD=$(shell ls src/lib/thread/*.cpp)
LBUND=$(shell ls src/lib/bundle/*.cpp)

# list the source code for the library
LSOURCES=$(LEXCE) $(LWIN) $(LUTIL) $(LELEM) $(LEVNT) $(LINPT) $(LGFX)
LSOURCES += $(LCOL) $(LSND) $(LGUI) $(LTIME) $(LFILE) $(LTHRD) $(LBUND)

# compile the source code for the library
LOBJECTS=$(LSOURCES:.cpp=.o)

# retrieve source code for the test program
TMAIN=$(shell ls src/test/*.cpp)

# list the source code for the test program
TSOURCES=$(TMAIN)

# compile the source code for the test program
TOBJECTS=$(TSOURCES:.cpp=.o)

# define the name of the library
LIB=libcppsdl.so

# define the name of the test executable
TEST=demo

# rule for building both the library and the test program
all: library test

# master rule for compiling the library
library: $(LSOURCES) $(LIB)

# master rule for compiling the test program
test: $(TSOURCES) $(TEST)

# sub-rule for compiling the library
$(LIB): $(LOBJECTS)
	$(CXX) $(LOBJECTS) -o $@ $(LDFLAGS)
	mkdir lib
	mkdir lobj
	mv -f $(LOBJECTS) lobj/
	mv -f $@ lib/

# sub-rule for compiling the test program
$(TEST): $(TOBJECTS)
	$(CXX) $(TOBJECTS) -o $@ $(TFLAGS)
	mkdir bin
	mkdir tobj
	mv -f $(TOBJECTS) tobj/
	mv -f $@ bin/

# rule for compiling source code to object code
.cpp.o:
	$(CXX) $(CXXFLAGS) $< -o $@

# target to install the compiled library
# REQUIRES ROOT
install:
	if [ -f "/usr/lib/libcppsdl.so" ]; then \
		rm /usr/lib/libcppsdl.so; \
	fi 
	if [ -d "/usr/include/cppsdl" ]; then \
		rm -rf /usr/include/cppsdl; \
	fi 
	
	mkdir /usr/include/cppsdl
	mkdir /usr/include/cppsdl/except
	mkdir /usr/include/cppsdl/win
	mkdir /usr/include/cppsdl/util 
	mkdir /usr/include/cppsdl/element
	mkdir /usr/include/cppsdl/event
	mkdir /usr/include/cppsdl/input 
	mkdir /usr/include/cppsdl/gfx 
	mkdir /usr/include/cppsdl/color 
	mkdir /usr/include/cppsdl/sound
	mkdir /usr/include/cppsdl/gui
	mkdir /usr/include/cppsdl/time 
	mkdir /usr/include/cppsdl/file 
	mkdir /usr/include/cppsdl/thread 
	mkdir /usr/include/cppsdl/bundle
	
	cp src/lib/cppsdl.h /usr/include/cppsdl/
	cp $(shell ls src/lib/except/*.h) /usr/include/cppsdl/except/
	cp $(shell ls src/lib/win/*.h) /usr/include/cppsdl/win/
	cp $(shell ls src/lib/util/*.h) /usr/include/cppsdl/util/
	cp $(shell ls src/lib/element/*.h) /usr/include/cppsdl/element/
	cp $(shell ls src/lib/event/*.h) /usr/include/cppsdl/event/
	cp $(shell ls src/lib/input/*.h) /usr/include/cppsdl/input/
	cp $(shell ls src/lib/gfx/*.h) /usr/include/cppsdl/gfx/
	cp $(shell ls src/lib/color/*.h) /usr/include/cppsdl/color/
	cp $(shell ls src/lib/sound/*.h) /usr/include/cppsdl/sound/
	cp $(shell ls src/lib/gui/*.h) /usr/include/cppsdl/gui/
	cp $(shell ls src/lib/time/*.h) /usr/include/cppsdl/time/
	cp $(shell ls src/lib/file/*.h) /usr/include/cppsdl/file/  
	cp $(shell ls src/lib/thread/*.h) /usr/include/cppsdl/thread/
	cp $(shell ls src/lib/bundle/*.h) /usr/include/cppsdl/bundle/
	cp ./lib/$(LIB) /usr/lib/

# target to clean the workspace
clean:
	if [ -d "lobj" ]; then \
		rm -rf lobj; \
	fi
	if [ -d "lib" ]; then \
		rm -rf lib; \
	fi
	if [ -d "bin" ]; then \
		rm -rf bin; \
	fi
	if [ -d "tobj" ]; then \
		rm -rf tobj; \
	fi

# end of Makefile
